﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Security.Permissions;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using WinFavs.Classes;

namespace WinFavs.Controls
{
	[ProvideProperty("HelpText", typeof (Control)),
	 Designer(typeof (HelpLabelDesigner)),
	// ReSharper disable once RedundantNameQualifier
	System.ComponentModel.DesignerCategory("Code")]
	public class Helper : Control, IExtenderProvider
	{
		private Control activeControl;

		/// <summary>
		///     Required designer variable.
		/// </summary>
		private Container components;

		private Hashtable helpTexts;

		public Helper()
		{
			InitializeComponent();

			helpTexts = new Hashtable();
		}

		[
			Browsable(false),
			EditorBrowsable(EditorBrowsableState.Never),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
		]
		public override string Text
		{
			get { return base.Text; }
			set { base.Text = value; }
		}

		bool IExtenderProvider.CanExtend(object target)
		{
			return target is Control && !(target is Helper);
		}

		/// <summary>
		///     Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary>
		///     Required method for Designer support - do not modify
		///     the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new Container();
			TabStop = false;
		}

		[DefaultValue("")]
		public string GetHelpText(Control control)
		{
			string text = (string) helpTexts[control] ?? string.Empty;
			return text;
		}

		private void OnControlEnter(object sender, EventArgs e)
		{
			activeControl = (Control) sender;
			Invalidate();
		}

		private void OnControlLeave(object sender, EventArgs e)
		{
			if (sender != activeControl) return;
			activeControl = null;
			Invalidate();
		}

		public void SetHelpText(Control control, string value)
		{
			if (value == null)
			{
				value = string.Empty;
			}

			if (value.Length == 0)
			{
				helpTexts.Remove(control);

				control.MouseEnter -= OnControlEnter;
				control.MouseLeave -= OnControlLeave;
			}
			else
			{
				helpTexts[control] = value;

				control.MouseEnter += OnControlEnter;
				control.MouseLeave += OnControlLeave;
			}

			if (control == activeControl)
			{
				Invalidate();
			}
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			base.OnPaint(pe);

			Rectangle rect = ClientRectangle;

			if (activeControl == null) return;
			string text = (string) helpTexts[activeControl];
			if (string.IsNullOrEmpty(text)) return;
			rect.Inflate(-2, -2);
			Brush brush = new SolidBrush(ForeColor);
			pe.Graphics.DrawString(text, Font, brush, rect);
			brush.Dispose();
		}

		[DesignerCategory("Code")]
		[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
		private class HelpLabelDesigner : ControlDesigner
		{
			private bool trackSelection = true;

			/// <summary>
			///     This property is added to the control's set of properties in the method
			///     PreFilterProperties below.  Note that on designers, properties that are
			///     explictly declared by TypeDescriptor.CreateProperty can be declared as
			///     private on the designer.  This helps to keep the designer's publi
			///     object model clean.
			/// </summary>
			[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
			// ReSharper disable once UnusedMember.Local
			private bool TrackSelection
			{
				get { return trackSelection; }
				set
				{
					trackSelection = value;
					if (trackSelection)
					{
						ISelectionService ss = (ISelectionService) GetService(typeof (ISelectionService));
						if (ss != null)
						{
							UpdateHelpLabelSelection(ss);
						}
					}
					else
					{
						Helper helper = (Helper) Control;
						if (helper.activeControl == null) return;
						helper.activeControl = null;
						helper.Invalidate();
					}
				}
			}

			public override DesignerVerbCollection Verbs
			{
				get
				{
					DesignerVerb[] verbs =
					{
						new DesignerVerb("Sample Verb", OnSampleVerb)
					};
					return new DesignerVerbCollection(verbs);
				}
			}

			protected override void Dispose(bool disposing)
			{
				if (disposing)
				{
					ISelectionService ss = (ISelectionService) GetService(typeof (ISelectionService));
					if (ss != null)
					{
						ss.SelectionChanged -= OnSelectionChanged;
					}
				}

				base.Dispose(disposing);
			}

			public override void Initialize(IComponent component)
			{
				base.Initialize(component);

				ISelectionService ss = (ISelectionService) GetService(typeof (ISelectionService));
				if (ss != null)
				{
					ss.SelectionChanged += OnSelectionChanged;
				}
			}

			private void OnSampleVerb(object sender, EventArgs e)
			{
				Alert.Show("You have just invoked a sample verb.  Normally, this would do something interesting.");
			}

			private void OnSelectionChanged(object sender, EventArgs e)
			{
				if (!trackSelection) return;
				ISelectionService ss = (ISelectionService) sender;
				UpdateHelpLabelSelection(ss);
			}

			protected override void PreFilterProperties(IDictionary properties)
			{
				base.PreFilterProperties(properties);

				properties["TrackSelection"] = TypeDescriptor.CreateProperty(
					GetType(), // the type this property is defined on 
					"TrackSelection", // the name of the property 
					typeof (bool)); // attributes
			}

			/// <summary>
			///     This is a helper method that, given a selection service, will update the active control
			///     of our help label with the currently active selection.
			/// </summary>
			/// <param name="ss"></param>
			private void UpdateHelpLabelSelection(ISelectionService ss)
			{
				Control c = ss.PrimarySelection as Control;
				Helper helper = (Helper) Control;
				if (c != null)
				{
					helper.activeControl = c;
					helper.Invalidate();
				}
				else
				{
					if (helper.activeControl == null) return;
					helper.activeControl = null;
					helper.Invalidate();
				}
			}
		}
	}
}