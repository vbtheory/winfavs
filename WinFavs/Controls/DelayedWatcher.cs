﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace WinFavs.Controls
{
	// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("Code")]
	[DefaultEvent("ChangeDetected")]
    public class DelayedWatcher : FileSystemWatcher
    {

		Timer tmrRaise = new Timer();

	    private bool raiseEvent;

		public event EventHandler ChangeDetected;

		[DefaultValue(100), Description("Specifies how much time the Watcher waits before raising an event")]
	    public int Interval
	    {
		    get { return tmrRaise.Interval; }
			set { tmrRaise.Interval = value; }
	    }

		[Description("Raised when a change in the folder was detected during the previous interval")]
		protected virtual void OnChangeDetected(EventArgs e)
		{
			EventHandler handler = ChangeDetected;

			if (handler == null) return;

			raiseEvent = false;
			handler(this, e);
		}

		public DelayedWatcher()
		{
			tmrRaise.Interval = 100;
			tmrRaise.Tick += (sender, args) =>
			{
				if (raiseEvent && EnableRaisingEvents)
					OnChangeDetected(EventArgs.Empty);
			};
			tmrRaise.Start();

			Changed += (sender, args) =>
			{
				raiseEvent = true;
			};
			Renamed += (sender, args) =>
			{
				raiseEvent = true;
			};
			Deleted += (sender, args) =>
			{
				raiseEvent = true;
			};
			Created += (sender, args) =>
			{
				raiseEvent = true;
			};
		}

    }
}
