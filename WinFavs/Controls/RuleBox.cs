﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WinFavs.Classes;

namespace WinFavs.Controls
{
	// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("Code")]
	public class RuleBox : TextBox
	{
		public enum Validation
		{
			Email,
			FixedLength,
			Path,
			FileName,
			None
		}

		private string _cueText;
		private int _fixedLength;
		private bool _isValid;
		private bool _raiseChanged;
		private Timer _t = new Timer();
		public event EventHandler TextChangedDelayed;
		private Validation _validationMethod = Validation.FixedLength;

		public bool IsValid
		{
			get
			{
				Validate();
				return _isValid;
			}
			private set
			{
				_isValid = value;
				BackColor = !_isValid && Text !="" ? Color.FromArgb(255, 234, 200) : SystemColors.Window;
			}
		}

		public Validation ValidationMethod
		{
			get { return _validationMethod; }
			set
			{
				_validationMethod = value;
				Validate();
			}
		}

		public int FixedLength
		{
			get { return _fixedLength; }
			set
			{
				if (value < 0) return;

				_fixedLength = value;
				Validate();
			}
		}

		public string CueText
		{
			get { return _cueText; }
			set
			{
				_cueText = value;
				Win32.SetCueText(this, value);
			}
		}

		protected override void OnLeave(EventArgs e)
		{
			base.OnLeave(e);

			Validate();
		}

		protected override void OnTextChanged(EventArgs e)
		{
			base.OnTextChanged(e);

			_raiseChanged = true;
		}

		protected virtual void OnTextChangedDelayed(EventArgs e)
		{
			EventHandler handler = TextChangedDelayed;

			if (handler == null) return;

			_raiseChanged = false;
			handler(this, e);
		}

		public RuleBox()
		{
			_t.Interval = 250;
			_t.Tick += (sender, args) =>
			{
				if (_raiseChanged) OnTextChangedDelayed(EventArgs.Empty);
			};
			_t.Start();
		}

		public void Validate()
		{
			switch (_validationMethod)
			{
				case Validation.Path:
					IsValid = Regex.IsMatch(Text, @"^[A-z]\:((\\([^\<\>\:\""\\\/\|\*\?\[\]\=\%\$\+\,\;\r\n\t\0])+)*(\\?))?$");
					break;
				case Validation.FixedLength:
					IsValid = Text.Length > FixedLength;
					break;
				case Validation.FileName:
					IsValid = Regex.IsMatch(Text,
						@"^(?!^(PRN|AUX|CLOCK\$|NUL|CON|COM\d|LPT\d|\..*)(\..+)?$)[^\x00-\x1f\\\?\*\:\<\>\""\;\|\/]+$");
					break;
				case Validation.Email:
					IsValid = Regex.IsMatch(Text, @"^[_A-z0-9\-]+(\.[_A-z0-9-]+)*@[A-z0-9-]+(\.[A-z0-9-]+)*(\.[A-z]{2,4})$");
					break;
				case Validation.None:
					IsValid = true;
					break;
			}
		}

	}
}