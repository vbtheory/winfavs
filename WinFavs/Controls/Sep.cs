﻿namespace WinFavs.Controls
{
	[System.ComponentModel.DesignerCategory("Code")]
	public class Sep : System.Windows.Forms.ToolStripSeparator
	{
		public override bool Enabled
		{
			get
			{
				return false;
			} 
		}
	}
}