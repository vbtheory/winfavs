﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WinFavs.Classes;
using WinFavs.Properties;

namespace WinFavs
{
	public partial class FrmShortcut : Form
	{
		private bool _canExit = true;
		private string _iconPath = "";
		private int _iconIndex;

		public string IconPath
		{
			get { return _iconPath; }
			set
			{
				_iconPath = value;
				BtnClearIcon.Visible = value != "";
			}
		}

		public int IconIndex
		{
			get { return _iconIndex; }
			set
			{
				_iconIndex = value;
				TxtPath_TextChanged(TxtName, null);
			}
		}

		public FrmShortcut(string shortcutName, string shortcutPath, string iconPath, int iconIndex)
		{
			InitializeComponent();

			Lighter.SetHighlight(BtnOk, Highlight.Green);

			TxtName.Text = shortcutName;
			TxtPath.Text = shortcutPath;
			IconPath = iconPath;
			IconIndex = iconIndex;

			if (Text.EndsWith("*")) Text = Text.Substring(0, Text.Length - 1);

			if (TxtName.Text != "") return;

			ChbAuto.Checked = true;
			TxtPath.Select();
			if (Text.EndsWith("*")) Text = Text.Substring(0, Text.Length - 1);
		}

		private void TxtPath_TextChanged(object sender, EventArgs e)
		{
			if (!Text.EndsWith("*")) Text += "*";

			BtnOk.Enabled = false;
			BtnUp.Enabled = false;
		}

		private void BtnBrowse_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog f = new FolderBrowserDialog {Description = "Add folder to profile", ShowNewFolderButton = false};

			if (f.ShowDialog() != DialogResult.OK) return;

			TxtName.Text = f.SelectedPath.Split('\\')[f.SelectedPath.Split('\\').Count() - 1];
			TxtPath.Text = f.SelectedPath;
		}

		private void BtnOk_Click(object sender, EventArgs e)
		{
			if (Directory.Exists(TxtPath.Text)) return;

			Alert a = new Alert("The folder you selected does not exist. Are you sure you would like to <b>create this shortcut</b>?",
				    "Folder does not exist", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Never ask again");

			a.Btn2Text = "&Create anyway";
			a.Btn3Text = "C&ancel";

			if (FrmMain.C.ChbTargetNotExist.Checked && a.Show() == DialogResult.No)
				_canExit = false;

			if (a.CheckBoxState) FrmMain.C.ChbTargetNotExist.Checked = false;
		}

		private void FrmEditShort_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_canExit)
			{
				if (!Text.EndsWith("*") || DialogResult == DialogResult.OK) return;

				if (BtnOk.Enabled)
				{
					Alert a = new Alert("You have some unsaved changes. Would you like to <b>save it</b>", "Saving confirmation",
						MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);

					a.Btn1Text = "&Save";
					a.Btn2Text = "&Don't save";
					a.Btn3Text = "&Cancel";
					switch (a.Show())
					{
						case DialogResult.Yes:
							DialogResult = DialogResult.OK;
							break;
						case DialogResult.No:
							DialogResult = DialogResult.Cancel;
							break;
						case DialogResult.Cancel:
							e.Cancel = true;
							break;
					}
				}
				else
				{
					Alert a = new Alert("You have unsaved changes, would you like to <b>continue editing</b>?", "Closing confirmation",
						MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

					a.Btn2Text = "&Keep editing";
					a.Btn3Text = "&Discard";
					switch (a.Show())
					{
						case DialogResult.Yes:
							e.Cancel = true;
							break;
						case DialogResult.No:
							DialogResult = DialogResult.Cancel;
							break;
					}
				}
				return;
			}

			_canExit = true;
			e.Cancel = true;
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			TxtName.ReadOnly = ChbAuto.Checked;
			TxtName.TabStop = !ChbAuto.Checked;

			if (ChbAuto.Checked) TxtPath_TextChanged(TxtPath, null);
		}

		private void BtnUp_Click(object sender, EventArgs e)
		{
			try
			{
				TxtPath.Text = new DirectoryInfo(TxtPath.Text).Parent.FullName;
			}
			catch
			{
			}
		}

		private void FrmShortcut_Shown(object sender, EventArgs e)
		{
			if (!FrmMain.C.ChbHints.Checked || !FrmMain.Frm.ShortcutHint) return;

			FrmMain.Frm.ShortcutHint = false;

			Alert.Show("Welcome to the shortcuts dialog. Here you create a shortcut for a specified folder.\n" +
			            "You should insert the path of the folder into the path field. If the path doesn't already exist, the icon will be a \"?\"\n" +
			            "You can also browse to the folder using the \"...\" button or go to the parent folder using the button next to it or even browse through all subfolders of the current path.\n" +
			            "You are able to either specify a name for shortcut or choose the app's auto-name feature which will automatically assign a name for your shortcut.",
				"How to use the shortcuts dialog");
		}

		private void BtnBrowsIcon_Click(object sender, EventArgs e)
		{
			OpenIconDialog o = new OpenIconDialog(IconPath ==""?Environment.GetFolderPath(Environment.SpecialFolder.System) + "\\shell32.dll": IconPath, _iconIndex);
			
			if(o.ShowDialog()!=DialogResult.OK) return;

			IconPath = o.FileName;
			IconIndex = o.Index;
		}

		private void BtnClearIcon_Click(object sender, EventArgs e)
		{
			IconPath = "";
			IconIndex = 0;
		}

		private void FrmShortcut_DragDrop(object sender, DragEventArgs e)
		{
			string[] folders = (string[])e.Data.GetData(DataFormats.FileDrop);

			foreach (string folder in folders.Where(x => Directory.Exists(x) || (x.EndsWith(".lnk") && File.Exists(x))))
			{
				TxtName.Text = folder.EndsWith(".lnk")
					? Path.GetFileNameWithoutExtension(folder)
					: folder.Split('\\')[folder.Split('\\').Count() - 1];
				TxtPath.Text = folder.EndsWith(".lnk") ? FrmMain.GetLnkTarget(folder) : folder;
				break;
			}
		}

		private void FrmShortcut_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Link;
		}

		private void TxtPath_TextChangedDelayed(object sender, EventArgs e)
		{
			if (ChbAuto.Checked && ((Control)sender).Name != "TxtName")
				try
				{
					if (Regex.IsMatch(TxtPath.Text, @"^[A-z]\:(\\)?$"))
						TxtName.Text = TxtPath.Text.Substring(0, 1).ToUpper() + " drive";
					else TxtName.Text = new DirectoryInfo(TxtPath.Text).Name;

					if (TxtName.Text == "" || !TxtName.IsValid)
						throw new Exception();
				}
				catch
				{
					TxtName.Text = "Shortcut";
				}

			BtnOk.Enabled = TxtName.IsValid && TxtPath.IsValid;

			try
			{
				PnlDrop.BackgroundImage = TxtPath.IsValid || IconPath != "" ? IconManager.GetIcon(TxtPath.Text, _iconPath, _iconIndex, IconSize.Large) : null;
			}
			catch
			{
				PnlDrop.BackgroundImage = null;
			}

			if (TxtPath.Text.ToLower() == "taha") PnlDrop.BackgroundImage = Resources.WinFavs;

			try
			{
				if (!TxtPath.IsValid || !Directory.Exists(TxtPath.Text)) return;

				BtnUp.Enabled = new DirectoryInfo(TxtPath.Text).Parent != null && new DirectoryInfo(TxtPath.Text).Parent.Exists;
			}
			catch
			{
			}
		}

	}
}