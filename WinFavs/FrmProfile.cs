﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WinFavs.Classes;
using Shortcut = WinFavs.Classes.Shortcut;

namespace WinFavs
{
	public partial class FrmProfile : Form
	{
		#region Declarations

		private Profile CurrentProfile;

		private bool _canClose = true;
		private bool _edit;

		private List<Shortcut> _shortcuts = new List<Shortcut>();

		#endregion

		#region GUI

		/// <summary>
		///     Creates a new instance of the Editor form
		/// </summary>
		/// <param name="p">Profile to be edited</param>
		/// <param name="edit">If true, the profile will only be edited</param>
		/// <param name="view">The view mode for the list view</param>
		public FrmProfile(Profile p, bool edit, View view)
		{
			_edit = edit;
			CurrentProfile = p;

			InitializeComponent();

			Lighter.SetHighlight(BtnSave, Highlight.Green);

			Win32.SetWindowTheme(LsvProfile.Handle, "explorer", null);
			Win32.SendMessage(LsvProfile.Handle, 0x1000 + 54, 0x00010000, "0x00010000");

			if (CurrentProfile.Entries == null) CurrentProfile.Entries = new List<Shortcut>();

			TxtName.Text = string.IsNullOrEmpty(CurrentProfile.Name) ? "" : CurrentProfile.Name;
			_shortcuts = CurrentProfile.Entries;

			RefreshShortcuts(false);

			if (Text.EndsWith("*")) Text = Text.Substring(0, Text.Length - 1);

			LsvProfile.View = view;
			if (FrmMain.C.ChbConservative.Checked) LsvProfile.Items.Clear();
			LsvProfile.VirtualMode = FrmMain.C.ChbConservative.Checked;
			RefreshShortcuts(false);

			HdrName.Width = -1;
			HdrPath.Width = -2;

			CmsClipboard.Renderer = new WinRenderer();
			CmsSplit.Renderer = new WinRenderer();
		}

		private void FrmEdit_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!_canClose) _canClose = e.Cancel = true;
			else
			{
				if (!Text.EndsWith("*")) return;

				if (BtnSave.Enabled)
				{
					Alert a = new Alert("You have unsaved changes, would you like to <b>save before closing</b>?", "Saving confirmation",
								MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
					a.Btn1Text = "&Save";
					a.Btn2Text = "&Don't Save";
					a.Btn3Text = "&Cancel";

					switch (a.Show())
					{
						case DialogResult.Yes:
							BtnSave_Click(null, null);
							break;
						case DialogResult.No:
							DialogResult = DialogResult.Cancel;
							break;
						case DialogResult.Cancel:
							e.Cancel = true;
							break;
					}
				}
				else
				{
					Alert a = new Alert("You have unsaved changes, would you like to <b>continue editing</b>?", "Closing confirmation",
						MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
					a.Btn2Text = "&Keep editing";
					a.Btn3Text = "&Discard";

					switch (a.Show())
					{
						case DialogResult.Yes:
							e.Cancel = true;
							break;
						case DialogResult.No:
							DialogResult = DialogResult.Cancel;
							break;
					}
				}
			}
		}

		private void BtnAdd_Click(object sender, EventArgs e)
		{
			FrmShortcut f = new FrmShortcut("", "", "", 0);

			if (f.ShowDialog() != DialogResult.OK) return;

			FixNames(f, _shortcuts.Count);
			RefreshShortcuts(true);
			f.Dispose();
		}

		private void BtnSave_Click(object sender, EventArgs e)
		{
			Alert a = new Alert("Are you sure you would like to create a profile with no shortcuts?", "Empty profile confirmation",
				    MessageBoxButtons.YesNo, "Never ask again");

			a.Btn2Text = "&Create";
			a.Btn3Text = "C&ancel";

			if (FrmMain.C.ChbNoShortcuts.Checked && _shortcuts.Count == 0 && a.Show() == DialogResult.No)
			{
				_canClose = false;
				if (a.CheckBoxState) FrmMain.C.ChbNoShortcuts.Checked = false;
				return;
			}
			if (a.CheckBoxState) FrmMain.C.ChbNoShortcuts.Checked = false;

			if (!_edit && File.Exists(FrmMain.ProfilesPath + @"\" + TxtName.Text + ".wfp"))
			{
				Alert a2 = new Alert("File already exits, Would you like to <b>overwrite it</b>?", "Overwrite confirmation",
					MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

				a2.Btn2Text = "&Overwrite";
				a2.Btn3Text = "&Cancel";
				if (a2.Show() == DialogResult.No)
				{
					_canClose = false;
					return;
				}
			}

			if (!_edit) CurrentProfile.FileName = FrmMain.ProfilesPath + "\\" + TxtName.Text + ".wfp";
			CurrentProfile.Name = TxtName.Text;
			CurrentProfile.Entries = _shortcuts;

			FrmMain.SaveProfile(CurrentProfile);

			if (Text.EndsWith("*"))
				Text = Text.Substring(0, Text.Length - 2);
		}

		private void BtnEdit_Click(object sender, EventArgs e)
		{
			List<int> orderedIndices = LsvProfile.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int index in orderedIndices)
			{
				FrmShortcut f = new FrmShortcut(_shortcuts[index].Name, _shortcuts[index].Path, _shortcuts[index].Icon, _shortcuts[index].IconIndex);

				if (f.ShowDialog() != DialogResult.OK)
				{	
					if (orderedIndices.Count > 1 && index != orderedIndices[orderedIndices.Count - 1])
					{
						Alert a = new Alert("Do you want to <b>keep editing shortcuts</b>?", "", MessageBoxButtons.YesNo);
						a.Btn2Text = "&Keep editing";
						a.Btn3Text = "&Cancel";

						if (a.Show() == DialogResult.Yes) continue;
						break;
					}
				}

				_shortcuts.RemoveAt(index);

				FixNames(f, index);
				ToggleModified();
			}

			RefreshShortcuts(false);
		}

		private void FixNames(FrmShortcut f, int index)
		{
			if (_shortcuts.Any(shortcut => shortcut.Name == f.TxtName.Text))
			{
				for (int i = 1; ; i++)
				{
					if (_shortcuts.Any(shortcut => shortcut.Name == f.TxtName.Text + " (" + i + ")")) continue;

					f.TxtName.Text += @" (" + i + @")";
					break;
				}
			}

			Shortcut s = new Shortcut(f.TxtName.Text, f.TxtPath.Text, f.IconPath, f.IconIndex);
			_shortcuts.Insert(index, s);
		}

		private void TmrCheckSelect_Tick(object sender, EventArgs e)
		{
			BtnRemove.Enabled = BtnEdit.Enabled = LsvProfile.SelectedIndices.Count != 0;
			BtnMoveUp.Enabled = LsvProfile.SelectedIndices.Count != 0 && !LsvProfile.SelectedIndices.Contains(0);
			BtnMoveDown.Enabled = LsvProfile.SelectedIndices.Count != 0 &&
			                      !LsvProfile.SelectedIndices.Contains(LsvProfile.Items.Count - 1);
			BtnClear.Enabled = LsvProfile.Items.Count != 0;
			CsiFromCopy.Enabled = LsvProfile.Items.Count != 0;
		}

		private void BtnClear_Click(object sender, EventArgs e)
		{
			Alert a = new Alert("Are you sure you would like to clear <b>all</b> shortcuts?", "Clear confirmation"
				, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

			a.Btn2Text = "&Clear";
			a.Btn3Text = "C&ancel";
			a.Btn2Highlight = Highlight.Red;

			if (a.Show() != DialogResult.Yes) return;

			_shortcuts.Clear();
			RefreshShortcuts(true);
		}

		private void BtnRemove_Click(object sender, EventArgs e)
		{
			Alert a = new Alert(string.Format("Are you sure you would like to delete <b>{0}</b>?",
					LsvProfile.SelectedIndices.Count.IsPlural("shortcut")), "Delete confirmation"
				, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

			a.Btn2Text = "&Delete";
			a.Btn3Text = "&Cancel";
			a.Btn2Highlight = Highlight.Red;

			if (a.Show() != DialogResult.Yes) return;

			List<int> orderedIndices = LsvProfile.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int i in orderedIndices)
			{
				_shortcuts.RemoveAt(i);
			}

			RefreshShortcuts(true);
		}

		private void LsvMain_DragDrop(object sender, DragEventArgs e)
		{
			string[] folders = (string[]) e.Data.GetData(DataFormats.FileDrop);

			foreach (string folder in folders.Where(x => ((x.EndsWith(".lnk") && File.Exists(x)) || (Directory.Exists(x)))))
			{
				string name = folder.EndsWith(".lnk")
					? Path.GetFileNameWithoutExtension(folder)
					: folder.Split('\\')[folder.Split('\\').Count() - 1];
				string path = folder.EndsWith(".lnk") ? FrmMain.GetLnkTarget(folder) : folder;

				if (_shortcuts.Any(shortcut => shortcut.Name == name))
				{
					for (int i = 1;; i++)
					{
						if (_shortcuts.Any(shortcut => shortcut.Name == name + " (" + i + ")")) continue;

						name += @" (" + i + @")";
						break;
					}
				}

				Shortcut s = new Shortcut(name, path, "", 0);
				_shortcuts.Add(s);
			}

			RefreshShortcuts(true);
		}

		private void LsvMain_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Link;
		}

		#endregion

		#region Methods

		/// <summary>
		///     Refreshes the list of shortcuts in the list view
		/// </summary>
		private void RefreshShortcuts(bool toggleModified)
		{
			if (toggleModified) ToggleModified();

			Cursor = Cursors.AppStarting;
			LsvProfile.BeginUpdate();

			LsvProfile.Items.Clear();
			ImgLarge.Images.Clear();
			ImgSmall.Images.Clear();

			if(FrmMain.C.ChbConservative.Checked) RefreshConservative();
			else RefreshNormal(toggleModified);

			HdrName.Width = -1;
			HdrPath.Width = -2;

			LsvProfile.EndUpdate();
			Cursor = Cursors.Default;
		}

		private void RefreshNormal(bool toggleModified)
		{
			List<ListViewItem> l = new List<ListViewItem>();
			List<string> loadedPaths = new List<string>();

			Stopwatch s = new Stopwatch();
			s.Start();

			foreach (Shortcut shortcut in _shortcuts)
			{
				int index;

				if (loadedPaths.Contains(shortcut.Path))
					index = loadedPaths.IndexOf(shortcut.Path);
				else
				{
					ImgLarge.Images.Add(IconManager.GetIcon(shortcut.Path, shortcut.Icon, shortcut.IconIndex, IconSize.ExtraLarge));
					ImgSmall.Images.Add(IconManager.GetIcon(shortcut.Path, shortcut.Icon, shortcut.IconIndex, IconSize.Large));

					loadedPaths.Add(shortcut.Path);
					index = ImgLarge.Images.Count - 1;
				}

				ListViewItem f = new ListViewItem(shortcut.Name, index, LsvProfile.Groups[Directory.Exists(shortcut.Path) ? 0 : 1]);
				f.SubItems.Add(shortcut.Path);
				f.ToolTipText = shortcut.Name + " (" + shortcut.Path + ")";

				l.Add(f);
			}

			LsvProfile.Items.AddRange(l.ToArray());
			l.Clear();

			s.Stop();
			if (s.ElapsedMilliseconds <= 2500) return;

			Notifier.Show("These shortcuts took about " + s.ElapsedMilliseconds.FormatTime() + " to load", "Click here to enable conservative mode",
				() =>
				{
					FrmMain.C.ChbConservative.Checked = true;
					RefreshShortcuts(toggleModified);
				});
		}

		private void RefreshConservative()
		{
			List<Bitmap> images = new List<Bitmap>();

			images.Add(IconManager.GetFolderIcon(IconSize.Large, FolderType.Open));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Desktop), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Downloads), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Environment.GetEnvironmentVariable("SystemDrive"), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Pictures), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Music), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Environment.GetEnvironmentVariable("USERPROFILE"), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Videos), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Documents), "", 0, IconSize.Large));
			images.Add(WinFavs.Properties.Resources.Warning);

			foreach (Bitmap image in images)
			{
				ImgLarge.Images.Add(image);
				ImgSmall.Images.Add(image);
			}

			LsvProfile.VirtualListSize = _shortcuts.Count;
		}

		private void LsvProfile_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
		{
			Shortcut shortcut = _shortcuts[e.ItemIndex];

			int index = 0;
			string temp = shortcut.Path.ToLower();

			if (temp == Paths.GetDefaultPath(KnownFolder.Desktop))
				index = 1;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Downloads))
				index = 2;
			else if ((temp.Length == 2 && temp.EndsWith(":")) || (temp.Length == 3 && temp.EndsWith(":\\")))
				index = 3;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Pictures))
				index = 4;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Music))
				index = 5;
			else if (temp == Environment.GetEnvironmentVariable("USERPROFILE").ToLower())
				index = 6;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Videos))
				index = 7;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Documents))
				index = 8;
			else if (!Directory.Exists(temp))
				index = 9;

			ListViewItem f = new ListViewItem(shortcut.Name, index, LsvProfile.Groups[Directory.Exists(shortcut.Path) ? 0 : 1]);
			f.SubItems.Add(shortcut.Path);
			if (!FrmMain.C.ChbConservative.Checked) f.ToolTipText = shortcut.Name + " (" + shortcut.Path + ")";

			e.Item = f;
		}

		/// <summary>
		///     Show an asterisk on the title bar to show that there are some unsaved changes
		/// </summary>
		private void ToggleModified()
		{
			if (!Text.EndsWith("*"))
				Text += "*";
		}

		#endregion

		private void FrmProfile_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Alt && e.KeyCode == Keys.Delete && BtnRemove.Enabled)
				BtnRemove_Click(null, null);
			if (e.Alt && e.KeyCode == Keys.Up && BtnMoveUp.Enabled)
				BtnMoveUp_Click(null, null);
			if (e.Alt && e.KeyCode == Keys.Down && BtnMoveDown.Enabled)
				BtnMoveDown_Click(null, null);
		}

		private void FrmProfile_Shown(object sender, EventArgs e)
		{
			if (!FrmMain.C.ChbHints.Checked || !FrmMain.Frm.ProfileHint) return;

			FrmMain.Frm.ProfileHint = false;

			Alert.Show("Welcome to the profiles dialog. Here you can do the following:\n" +
			            "<bullet>- Add new shortcut to profile\n" +
			            "- Select one or many shortcuts to edit them\n" +
			            "- Remove shortcuts from profile</bullet>\n" +
			            "Once you're done with your profile, choose a <b>significant</b> name for it and save it for later",
				"How to use the profiles dialog");
		}

		private void BtnMoveUp_Click(object sender, EventArgs e)
		{
			if (LsvProfile.SelectedIndices.Contains(0)) return;

			Point p = LsvProfile.AutoScrollOffset;

			List<int> orderedIndices = LsvProfile.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderBy(x => x).ToList();

			foreach (int index in orderedIndices.Where(index => index != 0))
			{
				Swap(index, index - 1);
			}

			RefreshShortcuts(true);

			foreach (int t in orderedIndices)
			{
				LsvProfile.SelectedIndices.Add(t - 1 <= 0 ? 0 : t - 1);
			}

			if (LsvProfile.Scrollable) LsvProfile.AutoScrollOffset = p;
		}

		private void Swap(int indexA, int indexB)
		{
			Shortcut tmp = _shortcuts[indexA];
			_shortcuts[indexA] = _shortcuts[indexB];
			_shortcuts[indexB] = tmp;
		}

		private void BtnMoveDown_Click(object sender, EventArgs e)
		{
			if (LsvProfile.SelectedIndices.Contains(LsvProfile.Items.Count - 1)) return;

			Point p = LsvProfile.AutoScrollOffset;

			List<int> orderedIndices = LsvProfile.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int index in orderedIndices.Where(index => index != LsvProfile.Items.Count - 1))
			{
				Swap(index, index + 1);
			}

			RefreshShortcuts(true);

			foreach (int t in orderedIndices)
			{
				LsvProfile.SelectedIndices.Add(t + 1 >= LsvProfile.Items.Count ? LsvProfile.Items.Count - 1 : t + 1);
			}

			if (LsvProfile.Scrollable) LsvProfile.AutoScrollOffset = p;
		}

		private void LsvMain_AfterLabelEdit(object sender, LabelEditEventArgs e)
		{
			if (
				!Regex.IsMatch(e.Label ?? "",
					@"^(?!^(PRN|AUX|CLOCK\$|NUL|CON|COM\d|LPT\d|\..*)(\..+)?$)[^\x00-\x1f\\\?\*\:\<\>\""\;\|\/]+$"))
			{
				e.CancelEdit = true;
				Notifier.Show("The name assigned is not a valid name");
				return;
			}

			Shortcut s = _shortcuts[e.Item];
			s.Name = e.Label;
			_shortcuts[e.Item] = s;

			LsvProfile.Items[e.Item].ToolTipText = s.Name + "\nPath:" + s.Path;

			ToggleModified();
		}

		private void LsvMain_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button != MouseButtons.Right) return;

			CsiPaste.Enabled = FrmMain.Clipboard != null;

			ListViewItem i = LsvProfile.GetItemAt(e.X, e.Y);

			CsiCopy.Enabled = CsiCut.Enabled = i != null || LsvProfile.SelectedIndices.Count != 0;

			if(CsiCopy.Enabled || CsiCut.Enabled || CsiPaste.Enabled)CmsClipboard.Show(LsvProfile.PointToScreen(e.Location));
		}

		private void CsiCopy_Click(object sender, EventArgs e)
		{
			FrmMain.Clipboard = new List<Shortcut>();
			foreach (int index in LsvProfile.SelectedIndices)
				FrmMain.Clipboard.Add(_shortcuts[index]);
		}

		private void CsiCut_Click(object sender, EventArgs e)
		{
			FrmMain.Clipboard = new List<Shortcut>(); 
			
			List<int> orderedIndices = LsvProfile.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();
			foreach (int index in orderedIndices)
			{
				FrmMain.Clipboard.Add(_shortcuts[index]);
				_shortcuts.RemoveAt(index);
			}
			RefreshShortcuts(true);
		}

		private void CsiPaste_Click(object sender, EventArgs e)
		{
			if(FrmMain.Clipboard == null) return;

			foreach (Shortcut s in FrmMain.Clipboard)
			{
				string result = s.Name;
				if (_shortcuts.Any(shortcut => shortcut.Name == s.Name))
				{
					for (int i = 1; ; i++)
					{
						if (_shortcuts.Any(shortcut => shortcut.Name == s.Name + " (" + i + ")")) continue;

						result += @" (" + i + @")";
						break;
					}
				}
				_shortcuts.Add(new Shortcut(result, s.Path, s.Icon, s.IconIndex));
			}

			RefreshShortcuts(true);
		}

		private void LsvMain_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.Control && e.KeyCode == Keys.C && LsvProfile.SelectedIndices.Count !=0)
				CsiCopy_Click(null, null);
			else if(e.Control && e.KeyCode == Keys.X && LsvProfile.SelectedIndices.Count != 0)
				CsiCut_Click(null, null);
			else if (e.Control && e.KeyCode == Keys.V && FrmMain.Clipboard != null)
				CsiPaste_Click(null, null);
			else if (e.Control && e.KeyCode == Keys.A)
				for (int i = 0; i < LsvProfile.Items.Count; i++)
					if(!LsvProfile.SelectedIndices.Contains(i)) LsvProfile.SelectedIndices.Add(i);
		}

		private void FrmProfile_FormClosed(object sender, FormClosedEventArgs e)
		{
			Dispose(true);
		}

		private void TxtName_TextChangedDelayed(object sender, EventArgs e)
		{
			BtnSave.Enabled = TxtName.IsValid;
		}

		private void TxtName_TextChanged(object sender, EventArgs e)
		{
			ToggleModified();
			BtnSave.Enabled = false;
		}

		private void CsiFromCopy_Click(object sender, EventArgs e)
		{
			List<int> orderedIndices = LsvProfile.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int index in orderedIndices)
			{
				FrmShortcut f = new FrmShortcut(_shortcuts[index].Name, _shortcuts[index].Path, _shortcuts[index].Icon, _shortcuts[index].IconIndex);

				if (f.ShowDialog() != DialogResult.OK)
				{
					if (orderedIndices.Count > 1 && index != orderedIndices[orderedIndices.Count - 1])
					{
						Alert a = new Alert("Do you want to <b>keep creating shortcuts</b>?", "", MessageBoxButtons.YesNo);
						a.Btn2Text = "&Keep creating";
						a.Btn3Text = "&Cancel";

						if (a.Show() == DialogResult.Yes) continue;
						break;
					}
				}

				FixNames(f, _shortcuts.Count);
				ToggleModified();
			}

			RefreshShortcuts(false);
		}

	}
}