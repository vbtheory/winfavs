﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WinFavs.Classes;
using Microsoft.VisualBasic.ApplicationServices;

namespace WinFavs
{
	internal static class Program
	{

		/// <summary>
		///     The main entry point for the application.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.SetUnhandledExceptionMode(UnhandledExceptionMode.ThrowException);
			Application.SetCompatibleTextRenderingDefault(false);
			Application.EnableVisualStyles();

			SingleInstanceController controller = new SingleInstanceController();

			if (!Debugger.IsAttached)
			{
				controller.UnhandledException += ThreadExceptionFunction;
				AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
				Application.ThreadException += ThreadExceptionFunction;
			}

			if (!File.Exists(FrmMain.ConfigPath) && !Reggy.IsUserAdministrator())
			{
				Process p = new Process();
				p.StartInfo.FileName = Application.ExecutablePath;
				p.StartInfo.Verb = "runas";
				p.Start();
				Process.GetCurrentProcess().Kill();
			}

			Application.Run(new FrmMain(args));
		}

		private static void ThreadExceptionFunction(object sender, ThreadExceptionEventArgs e)
		{
			ShowException(e.Exception, true);
		}

		private static void CurrentDomain_UnhandledException(object sender, System.UnhandledExceptionEventArgs e)
		{
			ShowException((Exception) e.ExceptionObject, e.IsTerminating);
		}

		private static void ShowException(Exception e, bool fatal)
		{
			try
			{
				Logger.Log(e.TargetSite.Name + " caused the following exception: " + e.Message + ": " + Zip(e.StackTrace), fatal?LogType.Fatal:LogType.Error);
				FrmMain.Frm.NtiMain.Visible = false;
			}
			catch { }

			if (Debugger.IsAttached) throw e;

			bool restart = false;

			try
			{
				Alert a = new Alert(string.Format("An unhandled exception occurred:<b> {0}</b>{1}" + (fatal ? " \nDo you want to restart WinFavs." : ""),
						e.Message, e.Message.EndsWith(".") ? "" : "."), "Unhandled exception", fatal ? MessageBoxButtons.YesNo : MessageBoxButtons.OK,
					fatal ? MessageBoxIcon.Error : MessageBoxIcon.Warning);

				a.Btn2Text = "&Restart";
				a.Btn3Text = "&Close";
				if (a.Show() == DialogResult.Yes)
					restart = true;
			}
			catch {}

			if (!fatal) return;

			ProcessStartInfo info = new ProcessStartInfo
			{
				Arguments = "/C choice /C Y /N /D Y /T 3 & \"" +
				            Application.ExecutablePath + "\"",
				WindowStyle = ProcessWindowStyle.Hidden,
				CreateNoWindow = true,
				FileName = "cmd.exe"
			};

			Logger.WaitForLog();

			if(restart) Process.Start(info);

			Process.GetCurrentProcess().Kill();
		}

		public static string Zip(string text)
		{
			byte[] buffer = Encoding.UTF8.GetBytes(text);
			var memoryStream = new MemoryStream();
			using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
			{
				gZipStream.Write(buffer, 0, buffer.Length);
			}

			memoryStream.Position = 0;

			var compressedData = new byte[memoryStream.Length];
			memoryStream.Read(compressedData, 0, compressedData.Length);

			var gZipBuffer = new byte[compressedData.Length + 4];
			Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
			Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
			return Convert.ToBase64String(gZipBuffer);
		}

		// ReSharper disable once UnusedMember.Global
		public static string DecompressString(string compressedText)
		{
			byte[] gZipBuffer = Convert.FromBase64String(compressedText);
			using (var memoryStream = new MemoryStream())
			{
				int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
				memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

				var buffer = new byte[dataLength];

				memoryStream.Position = 0;
				using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
				{
					gZipStream.Read(buffer, 0, buffer.Length);
					return Encoding.UTF8.GetString(buffer);
				}
			}
		}
	}

	public class SingleInstanceController : WindowsFormsApplicationBase
	{
		public SingleInstanceController()
		{
			IsSingleInstance = true;

			StartupNextInstance += this_StartupNextInstance;
		}

		void this_StartupNextInstance(object sender, StartupNextInstanceEventArgs e)
		{
			FrmMain.Frm.OpenFiles(e.CommandLine);
			FrmMain.Frm.showToolStripMenuItem_Click(null, null);
		}
		
		protected override void OnCreateMainForm()
		{
			MainForm = new FrmMain(CommandLineArgs.ToList());
		}

	}
}