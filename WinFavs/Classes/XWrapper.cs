﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;

namespace WinFavs.Classes
{
	/// <summary>
	///     A simple XML writer wrapper made by VBTheory
	/// </summary>
	internal class XWriter : IDisposable
	{

		#region Declarations

		private string _filename;

		private StreamWriter _sw;

		private int _tabs;

		private Stack<string> _unclosed = new Stack<string>();

		#region Public Properties

		/// <summary>
		///     Specifies if the writer is currently open and can write
		/// </summary>
		private bool IsOpen { get; set; }

		/// <summary>
		///     Specifies if the will contain indents
		/// </summary>
		private bool Indent { get; set; }

		#endregion

		#region Exception strings

		private const string FILEEXISTS = "File already exists";
		private const string WRITERNOTOPEN = "Writer is either never opened or was closed";
		private const string ALREADYOPEN = "Writer is already open";
		private const string NAMECANTBENULL = "Name of the element can't be null or empty";

		#endregion

		#endregion

		#region Constructor

		public XWriter()
		{
			Indent = true;
		}

		#endregion

		#region Private methods

		private string GetTab()
		{
			if (!Indent) return "";

			string temp = "";

			for (int i = 0; i < _tabs; i++)
			{
				temp += "\t";
			}

			return temp;
		}

		private string Encode(string s)
		{
			return SecurityElement.Escape(s);
		}

		#endregion

		#region Public methods

		/// <summary>
		///     Opens a file to write in
		/// </summary>
		/// <param name="filename">Name of the file</param>
		/// <param name="overwrite">Whether or not to overwrite the file if it exists</param>
		public void Open(string filename, bool overwrite = true)
		{
			if (!overwrite && File.Exists(_filename))
				throw new Exception(FILEEXISTS);
			if (IsOpen)
				throw new Exception(ALREADYOPEN);

			_tabs = 0;

			_unclosed = new Stack<string>();

			_filename = Path.GetFullPath(filename);

			_sw = new StreamWriter(_filename, false, new UTF8Encoding(true, false));

			WriteString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", false);

			IsOpen = true;
		}

		/// <summary>
		///     Writes a comment in the file.
		/// </summary>
		/// <param name="comment">String of the comment</param>
		public void WriteComment(string comment)
		{
			if (!IsOpen) throw new Exception(WRITERNOTOPEN);

			comment = Encode(comment);

			_sw.WriteLine(GetTab() + string.Format("<!--{0}-->", comment));
		}

		/// <summary>
		///     Writes an element
		/// </summary>
		/// <param name="name">Name of the element</param>
		/// <param name="attributes">Name & values of all attributes of this element</param>
		public void WriteElement(string name, params Attribute[] attributes)
		{
			WriteElement(name, false, attributes);
		}

		/// <summary>
		///     Writes an element
		/// </summary>
		/// <param name="name">Name of the element</param>
		/// <param name="selfEnclosed">If true, the element will be self-enclosed</param>
		/// <param name="attributes">Name & values of all attributes of this element</param>
		public void WriteElement(string name, bool selfEnclosed, params Attribute[] attributes)
		{
			if (!IsOpen) throw new Exception(WRITERNOTOPEN);

			if (string.IsNullOrEmpty(name))
				throw new ArgumentException(NAMECANTBENULL);

			string par = attributes.Aggregate("", (current, p) => current + "" + p.ToString());

			name = Encode(name);

			par = string.IsNullOrEmpty(par) ? "" : par;

			if (selfEnclosed)
				_sw.WriteLine(GetTab() + string.Format("<{0}{1} />", name, par));
			else
			{
				_sw.WriteLine(GetTab() + string.Format("<{0}{1}>", name, par));
				_unclosed.Push(name);
				_tabs++;
			}
		}

		/// <summary>
		///     Writes a string of text
		/// </summary>
		/// <param name="s">String to be written</param>
		/// <param name="encodeToHTML">Whether or not to format string to HTML</param>
		private void WriteString(string s, bool encodeToHTML = true)
		{
			s = encodeToHTML ? Encode(s) : s;
			_sw.WriteLine(GetTab() + s);
		}

		/// <summary>
		///     Closes elements until the selected one
		/// </summary>
		/// <param name="name">Name of the element to stop at</param>
		public void CloseElement(string name)
		{
			if (!IsOpen) throw new Exception(WRITERNOTOPEN);

			name = Encode(name);

			for (int i = 0; i < _unclosed.Count; i++)
			{
				string unclosedElement = _unclosed.Pop();

				_tabs--;
				_sw.WriteLine(GetTab() + string.Format("</{0}>", unclosedElement));
				if (name != "" && name == unclosedElement)
					break;
			}
		}

		/// <summary>
		///     Closes one element
		/// </summary>
		public void CloseElement()
		{
			if (!IsOpen) throw new Exception(WRITERNOTOPEN);

			CloseElement(1);
		}

		/// <summary>
		///     Closes a number of elements
		/// </summary>
		/// <param name="count"></param>
		private void CloseElement(int count)
		{
			if (!IsOpen) throw new Exception(WRITERNOTOPEN);


			for (int i = 0; i < count; i++)
			{
				string unclosedElement = _unclosed.Pop();

				_tabs--;
				_sw.WriteLine(GetTab() + string.Format("</{0}>", unclosedElement));
				if (count == i + 1)
					break;
			}
		}

		/// <summary>
		///     Closes all elements
		/// </summary>
		private void CloseElements()
		{
			CloseElement(_unclosed.Count);
		}

		/// <summary>
		///     Closes the writer and flushes resources
		/// </summary>
		public void Dispose()
		{
			CloseElements();
			IsOpen = false;
			_sw.Flush();
			_sw.Close();
		}

		#endregion

	}

	internal class XReader : IDisposable
	{

		#region Declarations

		private string _filename;

		private StreamReader _sr;

		private Stack<string> _opened = new Stack<string>();

		#region Public Properties

		/// <summary>
		///     Specifies if the writer is currently open and can write
		/// </summary>
		private bool IsOpen { get; set; }

		/// <summary>
		///     Specifies if the will contain indents
		/// </summary>
		private bool Indent { get; set; }

		#endregion

		#region Exception strings

		private const string READERNOTOPEN = "Writer is either never opened or was closed";
		private const string ALREADYOPEN = "Writer is already open";
		private const string NAMECANTBENULL = "Name of the element can't be null or empty";

		#endregion

		#endregion

		/// <summary>
		///     Opens a file to write in
		/// </summary>
		/// <param name="filename">Name of the file</param>
		public void Open(string filename)
		{
			if (IsOpen)
				throw new Exception(ALREADYOPEN);

			_opened = new Stack<string>();

			_filename = Path.GetFullPath(filename);

			_sr = new StreamReader(_filename, new UTF8Encoding(true, false));

			IsOpen = true;
		}
		


		public void Dispose()
		{
			throw new NotImplementedException();
		}
	}

	/// <summary>
	///     Represents an attribute for a XML element
	/// </summary>
	public class Attribute
	{
		private readonly string _value = "";
		private string _name = "";

		/// <summary>
		///     Creates an attribute with the specified name and value
		/// </summary>
		/// <param name="n">Attribute name</param>
		/// <param name="v">Attribute value</param>
		public Attribute(string n, string v)
		{
			_name = ExceptChars(n);
			_value = v;
		}

		/// <summary>
		///     Converts the attribute to a writable tag
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			if (string.IsNullOrEmpty(_name) && string.IsNullOrEmpty(_value)) return "";

			if (string.IsNullOrEmpty(_name)) return "";

			return string.Format(" {0}=\"{1}\"", _name, _value);
		}

		private string ExceptChars(string str)
		{
			IEnumerable<char> toExclude = new HashSet<char>(new[] {' ', '\t', '\n', '\r'});

			StringBuilder sb = new StringBuilder(str.Length);
			foreach (char c in str)
			{
				if (!toExclude.Contains(c))
					sb.Append(c);
			}
			return sb.ToString();
		}
	}
}