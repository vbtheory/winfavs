﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Media;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Transitions;
using WinFavs.Properties;

namespace WinFavs.Classes
{
	// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("Code")]
	public static class Notifier
	{

		public delegate void ClickEvent();
		public static bool IsShown;
		internal static Stack<Notification> _queue = new Stack<Notification>();

		#region Show Overloads

		public static void Show(string message)
		{
			_queue.Push(new Notification("WinFavs", message, null, "", null));

			if (_queue.Count > 0) ShowNotification();
		}

		public static void Show(string message, string title)
		{
			_queue.Push(new Notification(title, message, null, "", null));

			if (_queue.Count > 0) ShowNotification();
		}

		public static void Show(string message, Bitmap b)
		{
			_queue.Push(new Notification("WinFavs", message, b, "", null));

			if (_queue.Count > 0) ShowNotification();
		}

		public static void Show(string message, string title, Bitmap b)
		{
			_queue.Push(new Notification(title, message, b, "", null));

			if (_queue.Count > 0) ShowNotification();
		}

		public static void Show(string message, string eventName, ClickEvent c)
		{
			_queue.Push(new Notification("WinFavs", message, null, eventName, c));

			if (_queue.Count > 0) ShowNotification();
		}

		public static void Show(string message, Bitmap b, string eventName, ClickEvent c)
		{
			_queue.Push(new Notification("WinFavs", message, b, eventName, c));

			if (_queue.Count > 0) ShowNotification();
		}

		public static void Show(string message, string title, Bitmap b, string eventName, ClickEvent c)
		{
			_queue.Push(new Notification(title, message, b, eventName, c));

			if (_queue.Count > 0) ShowNotification();
		}

		public static void Show(string message, string title, string eventName, ClickEvent c)
		{
			_queue.Push(new Notification(title, message, null, eventName, c));

			if (_queue.Count > 0) ShowNotification();
		}

		#endregion

		public static void ShowNotification()
		{
			if (_queue.Count == 0 || IsShown) return;

			IsShown = true;
			Notification nt = _queue.Pop();

			Notiwindow n = new Notiwindow(nt.Message, nt.Title, nt.Icon, nt.EventName, nt.Click);
			SetWindowPos(n.Handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE + SWP_SHOWWINDOW + SWP_NOSIZE + SWP_NOMOVE);
			n.Visible = true;
		}

		[DllImport("user32.dll", EntryPoint = "SetWindowPos")]
		public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

		const short SWP_NOMOVE = 0X2;
		const short SWP_NOACTIVATE = 0x0010;
		const short SWP_NOSIZE = 1;
		const int SWP_SHOWWINDOW = 0x0040;
		private const int HWND_TOPMOST = -1;
	}

	internal class Notiwindow : Form
	{

		#region Declarations

		private bool _canGrow = true;
		private bool forceClose;
		private bool _fading;
		private Label _lblMessage = new Label();
		private Label _lblTitle = new Label();
		private PictureBox _pbxIcon = new PictureBox();
		private PictureBox _pbxClose = new PictureBox();
		private PictureBox _pbxClick = new PictureBox();

		#endregion

		public Notiwindow(string message, string title, Bitmap b, string en, Notifier.ClickEvent c)
		{
			ControlBox = false;
			ShowIcon = ShowInTaskbar = false;
			FormBorderStyle = FormBorderStyle.None;
			BackColor = Color.WhiteSmoke;
			ForeColor = Color.FromArgb(45, 45, 45);
			MinimizeBox = MaximizeBox = false;
			MinimumSize = Size = new Size(350, 90);
			TabStop = false;
			TopMost = true;

			_lblTitle.AutoSize = true;
			_lblMessage.Text = message;
			_lblTitle.Text = title;
			_lblMessage.AutoSize = true;
			_lblTitle.Font = new Font("Segoe UI", 14);
			_lblMessage.Font = new Font("Segoe UI Semilight", 10.75F);
			_lblMessage.SizeChanged += _lblMessage_SizeChanged;
			_lblMessage.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
			_lblMessage.MaximumSize = new Size(350, Int32.MaxValue);

			_pbxClose.Click += (sender, args) =>
			{
				forceClose = true;
				FadeOut();
			};

			Controls.Add(_lblTitle);
			Controls.Add(_pbxIcon);
			Controls.Add(_lblMessage);
			Controls.Add(_pbxClose);

			_pbxIcon.SizeMode = _pbxClose.SizeMode = PictureBoxSizeMode.Zoom;
			_pbxClose.Image = Resources.Close;
			_pbxClose.BackColor = Color.Transparent;
			_pbxClose.Size = new Size(16, 16);
			_pbxClose.Location = new Point(Width - _pbxClose.Width - 5, 5);
			_pbxClose.Anchor = AnchorStyles.Right | AnchorStyles.Top;

			_pbxIcon.Image = DateTime.Now.Month == 5 && DateTime.Now.Day == 20 ? Resources.WFV : (b ?? Resources.WinFavs);
			_pbxIcon.BackColor = Color.FromArgb(235, 235, 235);
			_pbxIcon.Location = new Point(0, 0);
			_pbxIcon.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
			_pbxIcon.Paint +=
				(sender, args) => args.Graphics.DrawLine(Pens.Silver, _pbxIcon.Width-1, 0, _pbxIcon.Width - 1, _pbxIcon.Height);
			_pbxIcon.Size = new Size((Height*3)/4, Height);

			_lblMessage.Location = new Point((Height*3)/4 + 5, _lblTitle.Bottom + 10);
			_lblTitle.Location = new Point((Height*3)/4 + 5, 5);
			Size =
				new Size((_lblMessage.Width > _lblTitle.Width ? _lblMessage.Width + 20 : _lblTitle.Width + 50) + _pbxIcon.Width,
					_lblMessage.Height + _lblTitle.Height + 25 + (c!=null?35:0));

			MouseLeave += Notiwindow_MouseLeave;
			MouseEnter += Notiwindow_MouseEnter;
			Shown += (sender, args) =>
			{
				for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
				{
					if (Application.OpenForms[i] is Notiwindow) continue;

					Application.OpenForms[i].Activate();
					return;
				}
			};

			if (c != null)
			{
				_pbxClick.Click += (sender, args) =>
				{
					forceClose = true;
					FadeOut();
					c();
				};
				_pbxClick.Cursor = Cursors.Hand;
				_pbxClick.BackColor = Color.FromArgb(225, 225, 225);
				_pbxClick.Size = new Size(Width - _pbxIcon.Width, 35);
				_pbxClick.Location = new Point(_pbxIcon.Right, Height - _pbxClick.Height);
				_pbxClick.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
				_pbxClick.Paint +=
					(sender, e) =>
					{
						e.Graphics.DrawLine(Pens.Silver,0, 0, _pbxClick.Width, 0);
						e.Graphics.DrawString(en, new Font("Segoe UI Semilight", 10.75F), new SolidBrush(Color.FromArgb(45, 45, 45)), 2, 6);
					};
				_pbxClick.MouseEnter += (sender, args) => _pbxClick.BackColor = Color.FromArgb(210, 210, 210);
				_pbxClick.MouseDown += (sender, args) => _pbxClick.BackColor = Color.FromArgb(190, 190, 190);
				_pbxClick.MouseLeave += (sender, args) => _pbxClick.BackColor = Color.FromArgb(225, 225, 225);
				_pbxClick.MouseUp += (sender, args) => _pbxClick.BackColor = Color.FromArgb(225, 225, 225);
				
				Controls.Add(_pbxClick);
			}

			StartPosition = FormStartPosition.Manual;
			Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - 10 - Width + 50, Screen.PrimaryScreen.WorkingArea.Height - 10 - Height);
			Opacity = 0.01;

			Transition t = new Transition(new TransitionType_CriticalDamping(500));
			t.add(this, "Left", Left -50);
			t.add(this, "Opacity", 0.99);
			t.TransitionCompletedEvent += (sender, args) =>
			{
				Thread thread = new Thread(x =>
				{
					Thread.Sleep(4000);
					if (Notifier._queue.Count > 0) FadeOut();
					else Notiwindow_MouseLeave(null, null);
				}) {IsBackground = true};

				thread.Start();
			};
			t.run();

			if (FrmMain.C.ChbPop.Checked)
			{
				SoundPlayer s = new SoundPlayer(Resources.Blop);
				s.Play();
			}

			Thread fo = new Thread(x =>
			{
				Thread.Sleep(6000);
				FadeOut();
			}) {IsBackground = true};
			fo.Start();
		}

		private const int WM_MOUSEACTIVATE = 0x0021, MA_NOACTIVATE = 0x0003;

		#region Events

		private void Notiwindow_MouseLeave(object sender, EventArgs e)
		{
			if (IsDisposed || _fading) return;
			if (ClientRectangle.Contains(PointToClient(MousePosition))) return;

			Transition t = new Transition(new TransitionType_Acceleration(250));
			t.add(this, "Opacity", 0.6);

			t.run();
		}

		private void Notiwindow_MouseEnter(object sender, EventArgs e)
		{
			if (IsDisposed || _fading) return;

			Transition t = new Transition(new TransitionType_Deceleration(250));
			t.add(this, "Opacity", 0.99);

			t.run();
		}

		private void _lblMessage_SizeChanged(object sender, EventArgs e)
		{
			if (!_canGrow) return;

			Size = new Size((_lblMessage.Width > _lblTitle.Width ? _lblMessage.Width + 20 : _lblTitle.Width + 50),
				_lblMessage.Height + _lblTitle.Height + 30);
			_canGrow = false;
		}

		#endregion

		#region Methods

		private void FadeOut()
		{
			if(_fading) return;
			_fading = true;

			if (IsDisposed)	return;

			while (!forceClose && ClientRectangle.Contains(PointToClient(MousePosition))) { }

			Transition t2 = new Transition(new TransitionType_Deceleration(300));

			if (forceClose) t2.add(this, "Left", Left + 50);
			else if (Notifier._queue.Count != 0) t2.add(this, "Left", Left - 50);

			t2.add(this, "Opacity", 0.01);
			t2.TransitionCompletedEvent += (sender, args) =>
			{
				Notifier.IsShown = false;
				Notifier.ShowNotification();
				Controls.Clear();
				Close();
				Dispose();
				GC.Collect();
			};
			t2.run();
		}

		#endregion

		protected override void WndProc(ref Message m)
		{
			switch (m.Msg)
			{
				case 0x0085: //WM_NCPAINT for the shadow
					if (Environment.OSVersion.Version.Major >= 6)
					{
						int enabled = 0;
						WIN32APIMethods.DwmIsCompositionEnabled(ref enabled);
						if(enabled != 1) return;
					}

					var var = 2;
					WIN32APIMethods.DwmSetWindowAttribute(Handle, 2, ref var, 4);
					MARGINS margins = new MARGINS
					{
						bottomHeight = 1,
						leftWidth = 1,
						rightWidth = 1,
						topHeight = 1
					};
					WIN32APIMethods.DwmExtendFrameIntoClientArea(Handle, ref margins);
					break;
				case WM_MOUSEACTIVATE:
					m.Result = (IntPtr) MA_NOACTIVATE;
					return;
			}
			base.WndProc(ref m);
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams pm = base.CreateParams;
				pm.ExStyle |= 0x80;
				return pm;
			}
		}
	}

	public struct Notification
	{
		public string Message;
		public string Title;
		public Bitmap Icon;
		public string EventName;
		public Notifier.ClickEvent Click;

		public Notification(string t, string m, Bitmap b, string e, Notifier.ClickEvent c)
		{
			Title = t;
			Message = m;
			Icon = b;
			Click = c;
			EventName = e;
		}
	}

	public static class WIN32APIMethods
	{
		[DllImport("dwmapi.dll")]
		public static extern int DwmExtendFrameIntoClientArea(IntPtr hWnd, ref MARGINS pMarInset);

		[DllImport("dwmapi.dll")]
		public static extern int DwmSetWindowAttribute(IntPtr hwnd, int attr, ref int attrValue, int attrSize);

		[DllImport("dwmapi.dll")]
		public static extern int DwmIsCompositionEnabled(ref int pfEnabled);
	}

	public struct MARGINS
	{
		public int leftWidth;
		public int rightWidth;
		public int topHeight;
		public int bottomHeight;
	}

}