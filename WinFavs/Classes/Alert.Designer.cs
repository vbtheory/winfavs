﻿namespace WinFavs.Classes
{
	partial class Alert
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PnlTop = new System.Windows.Forms.Panel();
			this.RtbMessage = new System.Windows.Forms.RichTextBox();
			this.PbxIcon = new System.Windows.Forms.PictureBox();
			this.Btn3 = new System.Windows.Forms.Button();
			this.Btn2 = new System.Windows.Forms.Button();
			this.Btn1 = new System.Windows.Forms.Button();
			this.ChbNeverAgain = new System.Windows.Forms.CheckBox();
			this.PnlTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PbxIcon)).BeginInit();
			this.SuspendLayout();
			// 
			// PnlTop
			// 
			this.PnlTop.BackColor = System.Drawing.SystemColors.Window;
			this.PnlTop.Controls.Add(this.RtbMessage);
			this.PnlTop.Controls.Add(this.PbxIcon);
			this.PnlTop.Location = new System.Drawing.Point(0, 0);
			this.PnlTop.MinimumSize = new System.Drawing.Size(278, 67);
			this.PnlTop.Name = "PnlTop";
			this.PnlTop.Size = new System.Drawing.Size(278, 67);
			this.PnlTop.TabIndex = 4;
			this.PnlTop.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlTop_Paint);
			// 
			// RtbMessage
			// 
			this.RtbMessage.BackColor = System.Drawing.SystemColors.Window;
			this.RtbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.RtbMessage.Location = new System.Drawing.Point(50, 12);
			this.RtbMessage.MinimumSize = new System.Drawing.Size(200, 50);
			this.RtbMessage.Name = "RtbMessage";
			this.RtbMessage.ReadOnly = true;
			this.RtbMessage.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
			this.RtbMessage.Size = new System.Drawing.Size(216, 50);
			this.RtbMessage.TabIndex = 0;
			this.RtbMessage.TabStop = false;
			this.RtbMessage.Text = "";
			this.RtbMessage.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.RtbMessage_LinkClicked);
			// 
			// PbxIcon
			// 
			this.PbxIcon.Location = new System.Drawing.Point(12, 12);
			this.PbxIcon.Name = "PbxIcon";
			this.PbxIcon.Size = new System.Drawing.Size(32, 32);
			this.PbxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.PbxIcon.TabIndex = 0;
			this.PbxIcon.TabStop = false;
			// 
			// Btn3
			// 
			this.Btn3.Location = new System.Drawing.Point(191, 96);
			this.Btn3.Name = "Btn3";
			this.Btn3.Size = new System.Drawing.Size(75, 23);
			this.Btn3.TabIndex = 2;
			this.Btn3.Text = "button1";
			this.Btn3.UseVisualStyleBackColor = true;
			this.Btn3.Click += new System.EventHandler(this.Btn1_Click);
			// 
			// Btn2
			// 
			this.Btn2.Location = new System.Drawing.Point(110, 96);
			this.Btn2.Name = "Btn2";
			this.Btn2.Size = new System.Drawing.Size(75, 23);
			this.Btn2.TabIndex = 1;
			this.Btn2.Text = "button1";
			this.Btn2.UseVisualStyleBackColor = true;
			this.Btn2.Click += new System.EventHandler(this.Btn1_Click);
			// 
			// Btn1
			// 
			this.Btn1.Location = new System.Drawing.Point(29, 96);
			this.Btn1.Name = "Btn1";
			this.Btn1.Size = new System.Drawing.Size(75, 23);
			this.Btn1.TabIndex = 0;
			this.Btn1.Text = "button1";
			this.Btn1.UseVisualStyleBackColor = true;
			this.Btn1.Click += new System.EventHandler(this.Btn1_Click);
			// 
			// ChbNeverAgain
			// 
			this.ChbNeverAgain.AutoSize = true;
			this.ChbNeverAgain.Location = new System.Drawing.Point(12, 73);
			this.ChbNeverAgain.Name = "ChbNeverAgain";
			this.ChbNeverAgain.Size = new System.Drawing.Size(80, 17);
			this.ChbNeverAgain.TabIndex = 3;
			this.ChbNeverAgain.Text = "checkBox1";
			this.ChbNeverAgain.UseVisualStyleBackColor = true;
			// 
			// Alert
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(274, 131);
			this.ControlBox = false;
			this.Controls.Add(this.ChbNeverAgain);
			this.Controls.Add(this.Btn1);
			this.Controls.Add(this.Btn2);
			this.Controls.Add(this.Btn3);
			this.Controls.Add(this.PnlTop);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(280, 160);
			this.Name = "Alert";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Alert";
			this.Shown += new System.EventHandler(this.Alert_Shown);
			this.PnlTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.PbxIcon)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox PbxIcon;
		private System.Windows.Forms.Panel PnlTop;
		private System.Windows.Forms.RichTextBox RtbMessage;
		private System.Windows.Forms.Button Btn3;
		private System.Windows.Forms.Button Btn2;
		private System.Windows.Forms.Button Btn1;
		private System.Windows.Forms.CheckBox ChbNeverAgain;
	}
}