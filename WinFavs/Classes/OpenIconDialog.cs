﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WinFavs.Classes
{
	public partial class OpenIconDialog : Form
	{
		private string _fileName;

		public OpenIconDialog(string filename, int index)
		{
			InitializeComponent();
			
			FileName = filename;
			Lighter.SetHighlight(BtnOk, Highlight.Green);

			if (LsvIcons.Items.Count <= index) return;

			LsvIcons.Items[index].Selected = true;
			LsvIcons.Items[index].Focused = true;
		}


		public string FileName
		{
			get { return _fileName; }
			set
			{
				_fileName = value;
				TxtPath.Text = value;

				GetIcons();
			}
		}

		public int Index { get; set; }

		private void GetIcons()
		{
			ImgMain.Images.Clear();
			LsvIcons.Items.Clear();

			if(FileName == "") return;

			try
			{
				foreach (Icon i in IconsFromFile(FileName, IconSize.Large))
				{
					try
					{
						ImgMain.Images.Add(i);
					}
					catch
					{
						ImgMain.Images.Add(new Bitmap(32, 32));
					}
					LsvIcons.Items.Add("", ImgMain.Images.Count - 1);
				}
			}
			catch (Exception ex)
			{
				Alert.Show("Failed to load icon from file: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Logger.Log("Error loading icons from file: " + ex.Message, LogType.Error);
			}
		}

		public static IEnumerable<Icon> IconsFromFile(string filename, IconSize size)
		{
			int iconCount = ExtractIconEx(filename, -1, null, null, 0);
			//checks how many icons.
			IntPtr[] iconPtr = new IntPtr[iconCount];

			//extracts the icons by the size that was selected.
			if (size == IconSize.Small)
				ExtractIconEx(filename, 0, null, iconPtr, iconCount);
			else
				ExtractIconEx(filename, 0, iconPtr, null, iconCount);

			Icon[] iconList = new Icon[iconCount];

			//gets the icons in a list.
			for (int i = 0; i < iconCount; i++)
			{
				Icon tempIcon = Icon.FromHandle(iconPtr[i]);
				iconList[i] = GetManagedIcon(ref tempIcon);
			}

			return iconList;
		}

		public static Icon GetManagedIcon(ref Icon unmanagedIcon)
		{
			Icon managedIcon = (Icon)unmanagedIcon.Clone();

			DestroyIcon(unmanagedIcon.Handle);

			return managedIcon;
		}

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		extern static bool DestroyIcon(IntPtr handle);

		[DllImport("Shell32", CharSet = CharSet.Auto)]
		internal extern static int ExtractIconEx(
			[MarshalAs(UnmanagedType.LPTStr)] 
            string lpszFile,                //size of the icon
			int nIconIndex,
			//index of the icon (in case we have more 
			//than 1 icon in the file
			IntPtr[] phIconLarge,           //32x32 icon
			IntPtr[] phIconSmall,           //16x16 icon
			int nIcons);

		private void BtnBrowse_Click(object sender, EventArgs e)
		{
			OpenFileDialog o = new OpenFileDialog();
			o.Filter = "All supported formats (*.ico, *.dll, *.exe)|*.ico;*.dll;*.exe";

			if(o.ShowDialog() != DialogResult.OK) return;
			if (ExtractIconEx(o.FileName, -1, null, null, 0) == 0)
			{
				Alert.Show("The file you selected does not contain any icons", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			FileName = o.FileName;
			Index = 0;
		}

		private void LsvIcons_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(LsvIcons.SelectedIndices.Count>0) Index = LsvIcons.SelectedIndices[0];
		}

		private void LsvIcons_DoubleClick(object sender, EventArgs e)
		{
			if(LsvIcons.SelectedIndices.Count == 0) return;

			DialogResult=DialogResult.OK;
			Close();
		}

		private void LsvIcons_DrawItem(object sender, DrawListViewItemEventArgs e)
		{
			if(e.Item.Selected) e.Graphics.FillRectangle(Brushes.DeepSkyBlue, e.Item.Bounds);
			e.Graphics.DrawImage(ImgMain.Images[e.Item.ImageIndex], e.Item.Bounds.X + (e.Item.Bounds.Width - 32) / 2, e.Item.Bounds.Y + (e.Item.Bounds.Height - 32) / 2, 32, 32);
			e.DrawText();
			e.DrawFocusRectangle();
		}

		private void LsvIcons_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button != MouseButtons.Left) return;

			ListViewItem i = LsvIcons.GetItemAt(e.X, e.Y);

			if(i == null) return;

			i.Selected = true;
			i.Focused = true;
		}     
	}

	public class DoubleView : ListView
	{
		public DoubleView()
		{
			DoubleBuffered = true;
		}
	}
}
