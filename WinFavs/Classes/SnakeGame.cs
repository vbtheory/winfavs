﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace WinFavs.Classes
{
	class SnakeGame : IDisposable
	{

		#region Declarations

		private Control Host;
		private Control Score;
		private Control Parent;
		private Timer TmrSnake = new Timer();
		private Timer TmrSuperFruit = new Timer();

		private const int _gridSize = 25;
		private List<Point> _body = new List<Point>();
		private Direction _direction = Direction.Left;
		private int _eaten;
		private ListQueue<Direction> _queue = new ListQueue<Direction>();
		private int _score;
		private int _highscore;
		private Point fruit;
		private Point superFruit = new Point(-2, -2);
		private bool _showSuperFruit = true;

		private enum Direction
		{
			Left,
			Right,
			Up,
			Down
		}

		private class ListQueue<T> : List<T>
		{
			public void Enqueue(T item)
			{
				Add(item);
			}

			public T Dequeue()
			{
				var t = base[0];
				RemoveAt(0);
				return t;
			}

			public T Peek()
			{
				return base[0];
			}
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Initializes an instance of a snake game
		/// </summary>
		/// <param name="host">Control that will be painted (Must be double buffered)</param>
		/// <param name="parent">Control that contains the host</param>
		/// <param name="score">Control that will hold the scores</param>
		public SnakeGame(Control host, Control parent, Control score)
		{
			Host = host;
			Parent = parent;
			Score = score;

			TmrSnake.Interval = 200;
			TmrSnake.Tick += TmrSnake_Tick;
			TmrSuperFruit.Interval = 3000;
			TmrSuperFruit.Tick += (sender, args) =>
			{
				TmrSuperFruit.Stop();
				_showSuperFruit = true;
				superFruit = new Point(-2, -2);
			};

			EndGame();
		}

		/// <summary>
		/// Refreshes the host control
		/// </summary>
		/// <param name="g">Graphics object that will be used to paint the host</param>
		public void Paint(Graphics g)
		{
			if (_body.Count < 3) return;

			g.SmoothingMode = SmoothingMode.AntiAlias;

			g.FillRectangle(Brushes.DarkGreen, fruit.X*_gridSize, fruit.Y*_gridSize, _gridSize + 1, _gridSize + 1);
			if(_showSuperFruit) g.FillRectangle(Brushes.DarkBlue, superFruit.X*_gridSize, superFruit.Y*_gridSize, _gridSize + 1,
				_gridSize + 1);

			for (int i = 1; i < _body.Count; i++)
			{
				Point point = _body[i];
				int opacity = (255 - (255*i*5)/100 <= 30 ? 30 : 255 - (255*i*5)/100);

				g.FillRectangle(new SolidBrush(Color.FromArgb(opacity, Color.DarkRed)), point.X*_gridSize,
					point.Y*_gridSize, _gridSize + 1, _gridSize + 1);
			}

			Point p = _body[0];
			Rectangle r = new Rectangle(p.X*_gridSize, p.Y*_gridSize, _gridSize + 1, _gridSize + 1);
			Point[] ps = new Point[5];

			switch (_direction)
			{
				case Direction.Up:
					ps = new[]
					{
						new Point(r.X, r.Bottom), new Point(r.X, r.Y + r.Height/2), new Point(r.X + r.Width/2, r.Top),
						new Point(r.Right, r.Top + r.Height/2), new Point(r.Right, r.Bottom)
					};
					break;
				case Direction.Down:
					ps = new[]
					{
						new Point(r.X, r.Y), new Point(r.X, r.Y + r.Height/2), new Point(r.X + r.Width/2, r.Bottom),
						new Point(r.Right, r.Y + r.Height/2), new Point(r.Right, r.Top)
					};
					break;
				case Direction.Left:
					ps = new[]
					{
						new Point(r.Right, r.Y), new Point(r.X + r.Width/2, r.Y), new Point(r.Left, r.Y + r.Height/2),
						new Point(r.X + r.Width/2, r.Bottom), new Point(r.Right, r.Bottom)
					};
					break;
				case Direction.Right:
					ps = new[]
					{
						new Point(r.X, r.Y), new Point(r.X + r.Width/2, r.Y), new Point(r.Right, r.Y + r.Height/2),
						new Point(r.X + r.Width/2, r.Bottom), new Point(r.X, r.Bottom)
					};
					break;
			}

			g.FillPolygon(Brushes.DarkRed, ps);
			ControlPaint.DrawBorder(g, Host.ClientRectangle, Color.Gray, ButtonBorderStyle.Solid);
		}

		/// <summary>
		/// Sends a key that will change the direction of the snake (hooked in the key down event of the form)
		/// </summary>
		/// <param name="k">Key code of the key</param>
		public void SendKey(Keys k)
		{
			if (_queue.Count == 0)
			{
				switch (k)
				{
					case Keys.Up:
						_queue.Enqueue(Direction.Up);
						break;
					case Keys.Down:
						_queue.Enqueue(Direction.Down);
						break;
					case Keys.Left:
						_queue.Enqueue(Direction.Left);
						break;
					case Keys.Right:
						_queue.Enqueue(Direction.Right);
						break;
				}
				return;
			}

			switch (k)
			{
				case Keys.Up:
					if (_queue.Peek() == Direction.Up) return;
					if (_queue.Peek() == Direction.Down) _queue[_queue.Count - 1] = Direction.Up;
					else _queue.Enqueue(Direction.Up);
					break;
				case Keys.Down:
					if (_queue.Peek() == Direction.Down) return;
					if (_queue.Peek() == Direction.Up) _queue[_queue.Count - 1] = Direction.Down;
					else _queue.Enqueue(Direction.Down);
					break;
				case Keys.Left:
					if (_queue.Peek() == Direction.Left) return;
					if (_queue.Peek() == Direction.Right) _queue[_queue.Count - 1] = Direction.Left;
					else _queue.Enqueue(Direction.Left);
					break;
				case Keys.Right:
					if (_queue.Peek() == Direction.Right) return;
					if (_queue.Peek() == Direction.Left) _queue[_queue.Count - 1] = Direction.Right;
					else _queue.Enqueue(Direction.Right);
					break;
			}
		}

		/// <summary>
		/// Pauses the game
		/// </summary>
		public void PauseGame()
		{
			if (!TmrSnake.Enabled) return;
			_showSuperFruit = true;
			TmrSnake.Stop();
			Button b = new Button();
			b.Click += (sender, args) =>
			{
				b.Visible = false;
				Parent.Controls.Remove(b);
				b.Dispose();
				Host.Focus();
				TmrSnake.Start();
			};
			b.Size = new Size(100, 30);
			b.Text = "Continue";
			b.Location = new Point(Host.Left + (Host.Width - b.Width) / 2, Host.Top + (Host.Height - b.Height) / 2);
			Parent.Controls.Add(b);
			b.Focus();
			b.BringToFront();
		}

		public void Dispose()
		{
			TmrSnake.Dispose();
			TmrSuperFruit.Dispose();
		}

		#endregion

		#region Private methods

		private void StartGame()
		{
			_score = 0;
			TmrSnake.Interval = 200;
			_eaten = 0;
			superFruit = new Point(-2, -2);
			_body = new List<Point>();
			_direction = Direction.Right;
			_body.Add(new Point(5, 5));
			_body.Add(new Point(4, 5));
			_body.Add(new Point(3, 5));
			GenerateFruit();
			Host.Invalidate();
			TmrSnake.Stop();
			TmrSnake.Start();
			Score.Text = string.Format("Score: {0} - Highscore: {1}", _score, _highscore);
			_queue = new ListQueue<Direction>();
			Host.Focus();
		}

		private void GenerateFruit()
		{
			Random r = new Random();

			List<Point> availablePoints = new List<Point>();

			for (int i = 0; i < 250/_gridSize; i++)
			{
				for (int j = 0; j < 250/_gridSize; j++)
				{
					if (!_body.Contains(new Point(i, j)) && new Point(i, j) != superFruit) availablePoints.Add(new Point(i, j));
				}
			}

			fruit = availablePoints[r.Next(availablePoints.Count - 1)];

			if (_eaten >= 5 && superFruit == new Point(-2, -2))
			{
				TmrSuperFruit.Start();
				_eaten = 0;
				availablePoints.Remove(fruit);
				superFruit = availablePoints[r.Next(availablePoints.Count - 1)];
				_showSuperFruit = true;
			}

			Host.Invalidate();
		}

		private void TmrSnake_Tick(object sender, EventArgs e)
		{
			_showSuperFruit = !_showSuperFruit;
			while (_queue.Count > 0 && ((_queue.Peek() == Direction.Left && _direction == Direction.Right) ||
			                            (_queue.Peek() == Direction.Right && _direction == Direction.Left) ||
			                            (_queue.Peek() == Direction.Down && _direction == Direction.Up) ||
			                            (_queue.Peek() == Direction.Up && _direction == Direction.Down)))
				_queue.Dequeue();

			bool gotFruit = false;

			if (_body[0] == fruit)
			{
				gotFruit = true;
				_score++;
				if (_score > _highscore) _highscore = _score;
				Score.Text = string.Format("Score: {0} - Highscore: {1}", _score, _highscore);
				GenerateFruit();
				_eaten++;
			}
			if (_body[0] == superFruit)
			{
				gotFruit = true;
				_score += 6;
				if (_score > _highscore) _highscore = _score;
				_eaten = 0;
				Score.Text = string.Format("Score: {0} - Highscore: {1}", _score, _highscore);
				superFruit = new Point(-2, -2);
			}
			int temp = Host.Width/_gridSize;
			if (_body[0].X == temp || _body[0].X == -1 ||
			    _body[0].Y == temp || _body[0].Y == -1 ||
			    _body.FindAll(x => x == _body[0]).Count > 1)
			{
				EndGame();
				return;
			}

			_direction = _queue.Count > 0 ? _queue.Dequeue() : _direction;

			switch (_direction)
			{
				case Direction.Left:
					if (!gotFruit) _body.RemoveAt(_body.Count - 1);
					_body.Insert(0, new Point(_body[0].X - 1, _body[0].Y));
					break;
				case Direction.Right:
					if (!gotFruit) _body.RemoveAt(_body.Count - 1);
					_body.Insert(0, new Point(_body[0].X + 1, _body[0].Y));
					break;
				case Direction.Down:
					if (!gotFruit) _body.RemoveAt(_body.Count - 1);
					_body.Insert(0, new Point(_body[0].X, _body[0].Y + 1));
					break;
				case Direction.Up:
					if (!gotFruit) _body.RemoveAt(_body.Count - 1);
					_body.Insert(0, new Point(_body[0].X, _body[0].Y - 1));
					break;
			}

			if (200 - _score > 30)
				TmrSnake.Interval = 200 - _score;

			Host.Invalidate();
		}

		private void EndGame()
		{
			Host.Controls.Clear();
			TmrSuperFruit.Stop();
			TmrSnake.Stop();
			Button b = new Button();
			b.Click += (sender, args) =>
			{
				b.Visible = false;
				Parent.Controls.Remove(b);
				b.Dispose();
				Host.Focus();
				StartGame();
			};
			b.Size = new Size(50, 50);
			b.Text = "New game";
			b.Location = new Point(Host.Left + (Host.Width - b.Width) / 2, Host.Top + (Host.Height - b.Height) / 2);
			Parent.Controls.Add(b);
			b.Focus();
			b.BringToFront();
		}

		#endregion

	}
}