﻿namespace WinFavs.Classes
{
	partial class OpenIconDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.LblPath = new System.Windows.Forms.Label();
			this.TxtPath = new System.Windows.Forms.TextBox();
			this.BtnBrowse = new System.Windows.Forms.Button();
			this.LsvIcons = new WinFavs.Classes.DoubleView();
			this.ImgMain = new System.Windows.Forms.ImageList(this.components);
			this.BtnCancel = new System.Windows.Forms.Button();
			this.BtnOk = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// LblPath
			// 
			this.LblPath.AutoSize = true;
			this.LblPath.Location = new System.Drawing.Point(9, 9);
			this.LblPath.Name = "LblPath";
			this.LblPath.Size = new System.Drawing.Size(123, 13);
			this.LblPath.TabIndex = 0;
			this.LblPath.Text = "Look for icons in this file:";
			// 
			// TxtPath
			// 
			this.TxtPath.BackColor = System.Drawing.SystemColors.InactiveBorder;
			this.TxtPath.Location = new System.Drawing.Point(12, 28);
			this.TxtPath.Name = "TxtPath";
			this.TxtPath.ReadOnly = true;
			this.TxtPath.Size = new System.Drawing.Size(228, 20);
			this.TxtPath.TabIndex = 0;
			// 
			// BtnBrowse
			// 
			this.BtnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnBrowse.Location = new System.Drawing.Point(246, 26);
			this.BtnBrowse.Name = "BtnBrowse";
			this.BtnBrowse.Size = new System.Drawing.Size(75, 23);
			this.BtnBrowse.TabIndex = 1;
			this.BtnBrowse.Text = "Browse...";
			this.BtnBrowse.UseVisualStyleBackColor = true;
			this.BtnBrowse.Click += new System.EventHandler(this.BtnBrowse_Click);
			// 
			// LsvIcons
			// 
			this.LsvIcons.Alignment = System.Windows.Forms.ListViewAlignment.Left;
			this.LsvIcons.AutoArrange = false;
			this.LsvIcons.FullRowSelect = true;
			this.LsvIcons.HideSelection = false;
			this.LsvIcons.LargeImageList = this.ImgMain;
			this.LsvIcons.Location = new System.Drawing.Point(12, 73);
			this.LsvIcons.MultiSelect = false;
			this.LsvIcons.Name = "LsvIcons";
			this.LsvIcons.OwnerDraw = true;
			this.LsvIcons.ShowGroups = false;
			this.LsvIcons.Size = new System.Drawing.Size(309, 253);
			this.LsvIcons.SmallImageList = this.ImgMain;
			this.LsvIcons.TabIndex = 2;
			this.LsvIcons.TileSize = new System.Drawing.Size(36, 36);
			this.LsvIcons.UseCompatibleStateImageBehavior = false;
			this.LsvIcons.View = System.Windows.Forms.View.Tile;
			this.LsvIcons.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.LsvIcons_DrawItem);
			this.LsvIcons.SelectedIndexChanged += new System.EventHandler(this.LsvIcons_SelectedIndexChanged);
			this.LsvIcons.DoubleClick += new System.EventHandler(this.LsvIcons_DoubleClick);
			this.LsvIcons.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LsvIcons_MouseMove);
			// 
			// ImgMain
			// 
			this.ImgMain.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ImgMain.ImageSize = new System.Drawing.Size(32, 32);
			this.ImgMain.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// BtnCancel
			// 
			this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnCancel.Location = new System.Drawing.Point(246, 332);
			this.BtnCancel.Name = "BtnCancel";
			this.BtnCancel.Size = new System.Drawing.Size(75, 23);
			this.BtnCancel.TabIndex = 4;
			this.BtnCancel.Text = "Cancel";
			this.BtnCancel.UseVisualStyleBackColor = true;
			// 
			// BtnOk
			// 
			this.BtnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.BtnOk.Location = new System.Drawing.Point(126, 332);
			this.BtnOk.Name = "BtnOk";
			this.BtnOk.Size = new System.Drawing.Size(114, 23);
			this.BtnOk.TabIndex = 3;
			this.BtnOk.Text = "OK";
			this.BtnOk.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(9, 57);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(165, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Select an icon from the list below:";
			// 
			// OpenIconDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(333, 360);
			this.Controls.Add(this.LsvIcons);
			this.Controls.Add(this.BtnOk);
			this.Controls.Add(this.BtnCancel);
			this.Controls.Add(this.BtnBrowse);
			this.Controls.Add(this.TxtPath);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.LblPath);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "OpenIconDialog";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Choose an icon";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LblPath;
		private System.Windows.Forms.TextBox TxtPath;
		private System.Windows.Forms.Button BtnBrowse;
		private DoubleView LsvIcons;
		private System.Windows.Forms.Button BtnCancel;
		private System.Windows.Forms.Button BtnOk;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ImageList ImgMain;
	}
}