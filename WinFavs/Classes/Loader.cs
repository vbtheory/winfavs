﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using Transitions;

namespace WinFavs.Classes
{
	// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("Code")]
	static class Loader
	{

		static Dictionary<Form, xForm> _loaders = new Dictionary<Form, xForm>();

		public static void ShowLoader(this Form c, string message)
		{
			if(_loaders.ContainsKey(c)) return;

			c.Enabled = false;
			xForm f = new xForm(c, message);
			f.Show();

			_loaders.Add(c, f);
		}

		public static void RemoveLoader(this Control c)
		{
			for (int i = 0; i < _loaders.Count; i++)
			{
				KeyValuePair<Form, xForm> pair = _loaders.ToList()[i];

				if (pair.Key != c) continue;

				c.Controls.Remove(pair.Value);
				pair.Value.Close();

				_loaders.Remove(pair.Key);
			}
			c.Enabled = true;
		}

	}

	internal class xForm : Form
	{
		private PathGradientBrush b;
		private Form _parent;
		private string _message;

		public xForm(Form parent, string message)
		{
			_parent = parent;
			_message = message;

			SetStyle(ControlStyles.DoubleBuffer | ControlStyles.ResizeRedraw, true);
			ControlBox = false;
			ShowIcon = false;
			ShowInTaskbar = false;
			Opacity = 0;
			FormBorderStyle = FormBorderStyle.None;
			StartPosition = FormStartPosition.Manual;
			Location = parent.PointToScreen(parent.ClientRectangle.Location);
			Size = parent.ClientSize;
			WindowState = parent.WindowState;
			Visible = _parent.Visible;
			BringToFront();
			Activate();

			parent.Activated += ParentOnActivated;
			parent.VisibleChanged += ParentOnVisibleChanged;
			Activated += (sender, args) =>
			{
				parent.Activate();
				BringToFront();
			};
			parent.LocationChanged += ParentOnLocationChanged;
			parent.Resize += ParentOnResize;

			GraphicsPath gp = new GraphicsPath();
			gp.AddEllipse(new Rectangle(ClientRectangle.X - Width/2, ClientRectangle.Y - Height/2, Width * 2, Height * 2));

			b = new PathGradientBrush(gp);
			b.CenterPoint = new PointF(ClientRectangle.Width / 2F, ClientRectangle.Height / 2F);
			b.CenterColor = Color.FromArgb(30, 30, 30);
			b.SurroundColors = new[] { Color.Black };

			Invalidate();

			Transition t = new Transition(new TransitionType_Acceleration(250));
			t.add(this, "Opacity", .9);
			t.run();
		}

		private void ParentOnVisibleChanged(object sender, EventArgs eventArgs)
		{
			if(!_parent.IsDisposed) Visible = _parent.Visible;
		}

		private void ParentOnActivated(object sender, EventArgs args)
		{
			BringToFront();
		}

		private void ParentOnResize(object sender, EventArgs args)
		{
			Size = _parent.ClientSize;
			WindowState = _parent.WindowState;
		}

		private void ParentOnLocationChanged(object sender, EventArgs args)
		{
			Location = _parent.PointToScreen(_parent.ClientRectangle.Location);
		}
		
		protected override void OnPaint(PaintEventArgs e)
		{
			e.Graphics.FillRectangle(b, ClientRectangle);
			e.Graphics.DrawString(_message, new Font(_parent.Font.FontFamily, 36F), Brushes.White, ClientRectangle, new StringFormat{LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center});
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams pm = base.CreateParams;
				pm.ExStyle |= 0x80;
				return pm;
			}
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);

			if (Opacity == 0.01)
			{
				Dispose();
				b.Dispose();
				return;
			}

			_parent.Activated -= ParentOnActivated;
			_parent.LocationChanged -= ParentOnLocationChanged;
			_parent.Resize -= ParentOnResize;

			e.Cancel = true;
			Transition t = new Transition(new TransitionType_Acceleration(200));
			t.add(this, "Opacity", 0.01);
			t.TransitionCompletedEvent += (sender, args) => Close();
			t.run();
		}
	}
}
