﻿using System;
using System.Drawing;
using System.Media;
using System.Windows.Forms;

namespace WinFavs.Classes
{
	class TxtBox : Control
	{

		#region Fields

		private bool _drawCaret;
		private bool _selectionMode;

		private int _caretPosition;
		private int _selectionStart = -1;
		private Color _caretColor = Color.Black;
		private Color _borderColor = Color.Black;
		private BorderStyle _borderStyle = BorderStyle.FixedSingle;
		private ButtonBorderStyle _fixedSingleStyle = ButtonBorderStyle.Solid;

		#endregion

		public TxtBox()
		{
			if(DesignMode) return;

			SetStyle(ControlStyles.DoubleBuffer | ControlStyles.ResizeRedraw | ControlStyles.FixedHeight, true);
			Size = new Size(150, (int)(CreateGraphics().MeasureString(" ", Font).Height * 1.5));
			Cursor = Cursors.IBeam;
			Padding = new Padding(3, 0, 0, 0);

			Timer t = new Timer();
			t.Interval = SystemInformation.CaretBlinkTime;
			t.Tick += (sender, args) =>
			{
				if (Focused) _drawCaret = !_drawCaret;
				else _drawCaret = false;
				Invalidate();
			};
			t.Start();

			Invalidate();
		}

		#region Properties

		public Color BorderColor
		{
			get { return _borderColor; }
			set
			{
				_borderColor = value;
				Invalidate();
			}
		}

		public BorderStyle BorderStyle
		{
			get { return _borderStyle; }
			set
			{
				_borderStyle = value;
				Invalidate();
			}
		}

		public ButtonBorderStyle FixedSingleStyle
		{
			get { return _fixedSingleStyle; }
			set
			{
				_fixedSingleStyle = value;
				Invalidate();
			}
		}

		public int CaretPosition
		{
			get { return _caretPosition; }
			set
			{
				if (Text.Length < value || value < 0)
				{
					SystemSounds.Beep.Play();
					return;
				}
				SelectionStart = -1;
				_caretPosition = value;

				if (Focused) _drawCaret = true;
				Invalidate();
			}
		}

		public Color CaretColor
		{
			get { return _caretColor; }
			set
			{
				_caretColor = value;
				Invalidate();
			}
		}

		public int SelectionStart
		{
			get { return _selectionStart; }
			set
			{
				if (Text.Length < value || (value < 0 && value != -1)) return;

				_selectionStart = value;
				Invalidate();
			}
		}

		#endregion

		#region Event overrides

		protected override void OnKeyDown(KeyEventArgs e)
		{
			int selectionStart = Math.Min(SelectionStart, CaretPosition);
			int selectionLength = Math.Max(SelectionStart, CaretPosition) - selectionStart;

			switch (e.KeyCode)
			{
				case Keys.F3://todo: fix right key
					if (e.Control) CaretPosition = Text.IndexOfAny(new[] { ' ', ',', '.', '!' }, CaretPosition) + 1; //todo: fix ctrl+right
					else if (e.Shift) SelectionStart++;
					else CaretPosition++;
					break;
				case Keys.F2://todo: fix left key
					if (e.Control) CaretPosition = Text.IndexOfAny(new[] { ' ', ',', '.', '!' }, 0, CaretPosition) - 1; //todo: fix ctrl+left
					else if (e.Shift) SelectionStart--;
					else CaretPosition--;
					break;
				case Keys.Delete:
					if (CaretPosition == Text.Length - 1) break;
					Text = Text.Remove(CaretPosition, 1);

					Invalidate();
					break;
				case Keys.Back:
					if (CaretPosition == 0) break;
					Text = Text.Remove(CaretPosition-1, 1);
					CaretPosition--;
					Invalidate();
					break;
				case Keys.X:
					if (!e.Control || SelectionStart == CaretPosition || SelectionStart == -1)
						break;

					CaretPosition = selectionStart;
					SelectionStart = -1;

					Clipboard.SetText(Text.Substring(selectionStart, selectionLength));
					Text = Text.Remove(selectionStart, selectionLength);
					break;
				case Keys.V:
					if (!e.Control || !Clipboard.ContainsText())
						break;
					if (e.Control && SelectionStart != CaretPosition && SelectionStart != -1)
					{
						CaretPosition = selectionStart;
						SelectionStart = -1;

						Text = Text.Remove(selectionStart, selectionLength);
					}
					Text = Text.Insert(CaretPosition, Clipboard.GetText());
					CaretPosition += Clipboard.GetText().Length;
					break;
				case Keys.C:
					if (e.Control && SelectionStart != CaretPosition && SelectionStart != -1)
						Clipboard.SetText(Text.Substring(selectionStart, selectionLength));
					break;
			}
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			if (ModifierKeys == Keys.Control || ModifierKeys == Keys.Shift || ModifierKeys == Keys.Alt) return;

			if (SelectionStart != CaretPosition && SelectionStart != -1)
			{
				int start = Math.Min(SelectionStart, CaretPosition);
				int length = Math.Max(SelectionStart, CaretPosition) - start;
				CaretPosition = start;
				SelectionStart = -1;

				Text = Text.Remove(start, length);
			}

			Text = Text.Insert(CaretPosition, e.KeyChar.ToString());
			CaretPosition++;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			float textHeight = e.Graphics.MeasureString(Text, Font).Height;
			float realPos = MeasureDisplayStringWidth(e.Graphics, Text.Substring(0, CaretPosition), Font);
			float textY = (Height - textHeight)/2;

			e.Graphics.Clear(BackColor);
			if (BorderStyle == BorderStyle.Fixed3D) ControlPaint.DrawBorder3D(e.Graphics, ClientRectangle);
			else if( BorderStyle == BorderStyle.FixedSingle) ControlPaint.DrawBorder(e.Graphics, ClientRectangle, BorderColor, FixedSingleStyle);

			if (SelectionStart != -1 && SelectionStart != CaretPosition)
			{
				int start = Math.Min(SelectionStart, CaretPosition);
				int length = Math.Max(SelectionStart, CaretPosition) - start;

				e.Graphics.FillRectangle(new SolidBrush(SystemColors.Highlight), Padding.Left + MeasureDisplayStringWidth(e.Graphics, Text.Substring(0, start), Font), textY -2 , MeasureDisplayStringWidth(e.Graphics, Text.Substring(start, length), Font), textHeight + 4);
			}
			e.Graphics.DrawString(Text, Font, new SolidBrush(ForeColor), Padding.Left, textY);

			if (_drawCaret) //todo: fix caret position
				e.Graphics.DrawLine(new Pen(CaretColor, SystemInformation.CaretWidth), realPos + Padding.Left, textY,
					realPos + Padding.Left, (Height + textHeight)/2);

			base.OnPaint(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);
			if (!_selectionMode) return;

			int selectionInt =
				new RichTextBox {Text = Text, Font = Font, Size = Size}.GetCharIndexFromPosition(new Point(e.X, e.Y));
			if (selectionInt < Text.Length - 1) selectionInt++;
			if (selectionInt != CaretPosition) SelectionStart = selectionInt;
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);

			if (e.Button != MouseButtons.Left) return;

			Focus();
			Point clickPoint = new Point(e.X, e.Y);
			CaretPosition = new RichTextBox { Text = Text, Font = Font, Size = Size }.GetCharIndexFromPosition(clickPoint);
			_selectionMode = true;
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);

			if (e.Button == MouseButtons.Left) _selectionMode = false;
		}

		#endregion

		#region Methods
		
		int MeasureDisplayStringWidth(Graphics graphics, string text, Font font)
		{
			if (text == "") return 0;
			StringFormat format = new StringFormat();
			RectangleF rect = new RectangleF(0, 0, 1000, 1000);
			CharacterRange[] ranges = { new CharacterRange(0, text.Length) };
			Region[] regions = new Region[1];

			format.SetMeasurableCharacterRanges(ranges);
			regions = graphics.MeasureCharacterRanges(text, font, rect, format);
			rect = regions[0].GetBounds(graphics);

			return (int)(rect.Right + 1.0f);
		}

		#endregion
	}
}
