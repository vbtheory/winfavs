﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Microsoft.VisualBasic.Devices;
using Microsoft.Win32;

namespace WinFavs.Classes
{
	internal static class Logger
	{

		#region Declarations

		private static readonly Stopwatch[] Watches = new Stopwatch[20];
		private static bool _isClosed = true;

		public static string _logPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
		                                @"\WinFavs\WinFavs.log";

		public static string _oldLog = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
		                               @"\WinFavs\WinFavs.old.log";

		private static DateTime _start;
		public static readonly Queue<LogEvent> Q = new Queue<LogEvent>();
		public static bool IsLogging;

		#endregion

		#region Public Methods

		public static void OpenLog()
		{
			try
			{
				if (File.Exists(_logPath) && new FileInfo(_logPath).Length/1024 >= 32 && !Debugger.IsAttached)
				{
					try
					{
						File.Delete(_oldLog);
					}
					catch
					{
					}
					File.Move(_logPath, _oldLog);
				}
				_isClosed = false;
				if (File.Exists(_logPath)) Log("", LogType.Info, false);
				Log("Started: " + DateTime.Now.ToString("G"), LogType.Info, false);

				Log(String.Format("Ver={3} Winver={0} CPU={7} Cores={1} RAM={5}GB Is64Bit={2} IsAdmin={4} HasCmdLine={6}{8}"
					, Environment.OSVersion.Version, Environment.ProcessorCount, Win32.Is64BitOperatingSystem.ToYesNo(),
					Application.ProductVersion, Reggy.IsUserAdministrator().ToYesNo(),
					(new ComputerInfo().TotalPhysicalMemory/1024F/1024F/1024F).ToString("####.##"),
					(Environment.GetCommandLineArgs().Count() > 1).ToYesNo(),
					GetClock(),
					Debugger.IsAttached ? " Debug=Yes" : ""),
					LogType.Info, false);
				_start = DateTime.Now;
			}
			catch
			{
			}
		}

		public static void Log()
		{
			if (Q.Count == 0) return;
			IsLogging = true;
			LogEvent l = Q.Dequeue();
			switch (l.Index)
			{
				case -1:
					LogEvent(l);
					break;
				case -2:
					LogEvent(l);
					_isClosed = true;
					break;
				default:
					LogTimed(l);
					break;
			}
			IsLogging = false;
		}

		public static void Log(string logContent, LogType lt = LogType.Info, bool attachTime = true)
		{
			StackFrame s = new StackFrame(1);
			Q.Enqueue(new LogEvent(DateTime.Now, Thread.CurrentThread.ManagedThreadId,
				s.GetMethod().Name, s.GetMethod().DeclaringType.Name, logContent, lt, attachTime));
			if (!IsLogging) Log();
		}

		public static int Log(string logContent, Mode m, int index, LogType lt = LogType.Info, bool attachTime = true)
		{
			if (m == Mode.Start && index != -2)
			{
				for (int i = 0; i < 19; i++)
				{
					if (Watches[i] != null) continue;

					index = i;
					break;
				}
			}

			if (index != -2)
				if (m == Mode.Start)
				{
					Watches[index] = new Stopwatch();
					Watches[index].Start();
				}
				else
					Watches[index].Stop();

			Q.Enqueue(new LogEvent(DateTime.Now, Thread.CurrentThread.ManagedThreadId,
				new StackFrame(1).GetMethod().Name, new StackFrame(1).GetMethod().DeclaringType.Name, logContent, m, index, lt, attachTime));
			if (!IsLogging) Log();
			return index;
		}

		public static void CloseLog()
		{
			try
			{
				Log("Closed: " + DateTime.Now.ToString("G") + " (" + Convert.ToInt64((DateTime.Now - _start).TotalMilliseconds).FormatTime() + ")",
					Mode.Stop, -2, LogType.Info, false);
			}
			catch {}
		}

		/// <summary>
		/// Freezes the app while there is still log to write
		/// </summary>
		public static void WaitForLog()
		{
			while (Q.Count > 0)
			{
				if (!IsLogging) Log();
			}
		}

		#endregion

		#region Private Methods

		private static void LogEvent(LogEvent l)
		{
			try
			{
				if (_isClosed) return;

				StreamWriter fs = new StreamWriter(_logPath, true);
				if (l.AttachTime)
					fs.Write("{0} [{1}] {2} {4}.{3} : ", l.Time.ToString("hh:mm:ss.fff"), l.Type, l.ThreadId, l.CallerName.Trim('.'), l.Class);
				fs.WriteLine(l.LogContent);
				fs.Flush();
				fs.Close();
			}
			catch
			{
			}
		}

		private static void LogTimed(LogEvent l)
		{
			try
			{
				if (_isClosed) return;

				StreamWriter fs = new StreamWriter(_logPath, true);
				if (l.AttachTime)
					fs.Write("{0} [{1}] {2} {4}.{3} : ", l.Time.ToString("hh:mm:ss.fff"), l.Type, l.ThreadId, l.CallerName.Trim('.'), l.Class);
				switch (l.Mode)
				{
					case Mode.Start:
						fs.WriteLine(l.LogContent);
						break;
					case Mode.Stop:
						fs.WriteLine(l.LogContent, Watches[l.Index].ElapsedMilliseconds + "ms");
						Watches[l.Index] = null;
						break;
				}
				fs.Flush();
				fs.Close();
			}
			catch
			{
			}
		}

		#endregion

		#region Additional Methods

		private static string GetClock()
		{
			try
			{
				RegistryKey r = Registry.LocalMachine.OpenSubKey("HARDWARE\\DESCRIPTION\\SYSTEM\\CENTRALPROCESSOR\\0");
				return (Single.Parse(r.GetValue("~MHz").ToString()) / 1000F).ToString("##.00") + "GHz";
			}
			catch
			{
				return "N/A";
			}
		}

		public static string ToYesNo(this bool b)
		{
			return b ? "Yes" : "No";
		}

		public static string IsPlural(this int number, string word)
		{
			return number + " " + word + (number == 1 ? "" : "s");
		}

		public static string IsPlural(this long number, string word)
		{
			return number + " " + word + (number == 1 ? "" : "s");
		}

		public static string IsPlural(this float number, string word, string format)
		{
			return number.ToString(format) + " " + word + (number == 1 ? "" : "s");
		}

		public static int ToInt(this string number)
		{
			return Int32.Parse(number);
		}

		public static string FormatTime(this long ms)
		{
			if (ms < 1000) return ms.IsPlural("millisecond");
			if (ms >= 1000 && ms < 60000) return (ms/1000F).IsPlural("second","##.#");
			return ((ms / 1000F) / 60F).IsPlural("minute", "##.#");
		}

		#endregion
	}

	public struct LogEvent
	{
		public bool AttachTime;
		public string CallerName;
		public string Class;
		public int Index;
		public string LogContent;
		public Mode Mode;
		public int ThreadId;
		public DateTime Time;
		public LogType Type;

		public LogEvent(DateTime time, int threadId, string callerName, string claas, string logContent, Mode m, int index,
			LogType lt = LogType.Info, bool attachTime = true)
		{
			LogContent = logContent;
			Mode = m;
			Index = index;
			Type = lt;
			AttachTime = attachTime;
			CallerName = callerName;
			ThreadId = threadId;
			Time = time;
			Class = claas;
		}

		public LogEvent(DateTime time, int threadId, string callerName, string claas, string logContent, LogType lt = LogType.Info,
			bool attachTime = true)
		{
			LogContent = logContent;
			Mode = new Mode();
			Index = -1;
			Type = lt;
			AttachTime = attachTime;
			CallerName = callerName;
			ThreadId = threadId;
			Time = time;
			Class = claas;
		}
	}

	public enum Mode
	{
		Start,
		Stop
	}

	public enum LogType
	{
		Info,
		Warning,
		Error,
		Fatal
	}
}