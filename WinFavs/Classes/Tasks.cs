﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace WinFavs.Classes
{
	static class Tasks
	{

		/// <summary>
		///     Scans tasks folder for any valid ones
		/// </summary>
		public static void ScanForTasks(ref List<Task> _loadedTasks, bool fromFrmMain, bool cancelIfAutoRefreshEnabled, ListView lsvMain = null)
		{
			if(cancelIfAutoRefreshEnabled && FrmMain.C.ChbRefresh.Checked) return;

			bool result = false;
			int index = Logger.Log("Scanning for tasks...", Mode.Start, 0);
			if(lsvMain!=null) lsvMain.Items.Clear();
			_loadedTasks.Clear();

			if (!Directory.Exists(FrmMain.TasksPath))
			{
				Directory.CreateDirectory(FrmMain.TasksPath);
				return;
			}

			int tasks = 0, enabled = 0, today = 0;
			DateTime start = DateTime.Now;

			foreach (string task in Directory.GetFiles(FrmMain.TasksPath).Where(f => f.EndsWith(".tsk")))
			{
				Task s = ReadTask(task, true);

				if (s.IsOld && !s.HasFailedLoading) result = true;
				if (s.HasFailedLoading) continue;

				_loadedTasks.Add(s);

				tasks++;
				if (s.Enabled) enabled++;
				if (((s.Mon && start.DayOfWeek == DayOfWeek.Monday) ||
					 (s.Tue && start.DayOfWeek == DayOfWeek.Tuesday) ||
					 (s.Wed && start.DayOfWeek == DayOfWeek.Wednesday) ||
					 (s.Thu && start.DayOfWeek == DayOfWeek.Thursday) ||
					 (s.Fri && start.DayOfWeek == DayOfWeek.Friday) ||
					 (s.Sat && start.DayOfWeek == DayOfWeek.Saturday) ||
					 (s.Sun && start.DayOfWeek == DayOfWeek.Sunday))
					&& start.Hour <= s.Hour && start.Minute <= s.Minute) today++;

				if (lsvMain == null) continue;
				string days = "";

				if (s.Mon && s.Tue && s.Thu && s.Wed && s.Fri && s.Sat && s.Sun) days = "Everyday";
				else if (!s.Mon && !s.Tue && !s.Thu && !s.Wed && !s.Fri && !s.Sat && !s.Sun) days = "Never";
				else
				{
					if (s.Mon) days = "Monday";
					if (s.Tue) days += (days == "" ? "" : ",") + "Tuesday";
					if (s.Wed) days += (days == "" ? "" : ",") + "Wednesday";
					if (s.Thu) days += (days == "" ? "" : ",") + "Thursday";
					if (s.Fri) days += (days == "" ? "" : ",") + "Friday";
					if (s.Sat) days += (days == "" ? "" : ",") + "Saturday";
					if (s.Sun) days += (days == "" ? "" : ",") + "Sunday";
				}

				ListViewItem l = new ListViewItem(s.Profile);
				l.SubItems.Add(s.Hour.ToString("00") + ":" + s.Minute.ToString("00"));
				l.SubItems.Add(days);
				l.SubItems.Add(s.Enabled.ToString());

				lsvMain.Items.Add(l);
			}

			if (lsvMain != null && !lsvMain.IsDisposed)
			{
				lsvMain.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
				lsvMain.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
			}

			if (fromFrmMain)
			{
				FrmMain.Frm.TmrTasks.Interval = 1;
			}

			Logger.Log(string.Format("Found {0} ({1}, {2} enabled, {3} today)", tasks.IsPlural("task"), "{0}", enabled, today), Mode.Stop,
				index);

			if (result) FrmMain.Frm.MigratePresets();
		}

		/// <summary>
		///     Creates a task from the a file
		/// </summary>
		/// <param name="path">Path of the file to be read</param>
		/// <param name="checkExists">If true and the profile doesn't exist, it will be marked as failed</param>
		/// <param name="isOld">If the preset we're loading is being migrated</param>
		/// <returns></returns>
		public static Task ReadTask(string path, bool checkExists, bool isOld = false)
		{
			Task s = new Task();

			try
			{
				XDocument file = XDocument.Load(path);
				s.Path = path;

				XElement x = file.Root;

				string[] days = x.Attribute("Days").Value.Split(',');

				if (x.Attributes().Any(a => a.Name == "Preset")) s.IsOld = true;

				s.Mon = days.Contains("Mon");
				s.Tue = days.Contains("Tue");
				s.Wed = days.Contains("Wed");
				s.Thu = days.Contains("Thu");
				s.Fri = days.Contains("Fri");
				s.Sat = days.Contains("Sat");
				s.Sun = days.Contains("Sun");

				s.Hour = int.Parse(x.Attribute("Hour").Value);
				s.Minute = int.Parse(x.Attribute("Minute").Value);

				s.Profile = x.Attribute(isOld ||s.IsOld?"Preset" : "Profile").Value;

				s.Enabled = bool.Parse(x.Attribute("Enabled").Value);
				bool b = checkExists && !File.Exists(FrmMain.ProfilesPath + "\\" + s.Profile + ".wfp");

				s.HasFailedLoading = b || s.Hour > 23 || s.Minute > 59 || s.Hour < 0 || s.Minute < 0;
			}
			catch (Exception ex)
			{
				Logger.Log(string.Format("Exception while reading task: {0} ({1}, {2})", ex.Message, path, ex.TargetSite.Name),
					LogType.Error);

				s.HasFailedLoading = true;
			}
			
			return s;
		}

		public static void WriteTask(string path, string enabled, string days, string hour, string minute, string profile)
		{
			XWriter x = new XWriter();
			x.Open(path);

			x.WriteElement("Task", true, new Attribute("Version", Application.ProductVersion),
				new Attribute("Enabled", enabled),
				new Attribute("Days", days),
				new Attribute("Hour", hour),
				new Attribute("Minute", minute),
				new Attribute("Profile", profile));

			x.Dispose();

		}
	}
}
