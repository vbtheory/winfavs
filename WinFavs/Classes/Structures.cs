﻿using System.Collections.Generic;

namespace WinFavs.Classes
{
	public struct Profile
	{
		public List<Shortcut> Entries;
		public string FileName;

		public bool HasFailedLoading;
		public string Name;

		public Profile(string n, string f)
		{
			Name = n;
			FileName = f;
			Entries = new List<Shortcut>();
			HasFailedLoading = false;
		}
	}

	public struct Shortcut
	{
		public string Name, Path, Icon;
		public int IconIndex;

		public Shortcut(string n, string p, string i, int index	)
		{
			Name = n;
			Path = p;
			Icon = i;
			IconIndex = index;
		}
	}

	public struct Task
	{
		public bool Enabled;
		public bool Fri;
		public bool IsOld;

		public bool HasFailedLoading;

		public int Hour, Minute;
		public bool Mon;
		public string Path, Profile;
		public bool Sat, Sun;
		public bool Thu;
		public bool Tue, Wed;
	}

}