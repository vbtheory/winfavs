﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using WinFavs.Properties;

namespace WinFavs.Classes
{
	internal static class IconManager
	{
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		public struct SHFILEINFO
		{
			public IntPtr hIcon;
			public int iIcon;
			public uint dwAttributes;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)] public string szDisplayName;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)] public string szTypeName;
		};

		[DllImport("shell32.dll", CharSet = CharSet.Auto)]
		private static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, out SHFILEINFO psfi, uint cbFileInfo,
			uint uFlags);

		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool DestroyIcon(IntPtr hIcon);

		public const uint SHGFI_ICON = 0x000000100;
		public const uint SHGFI_USEFILEATTRIBUTES = 0x000000010;
		public const uint SHGFI_OPENICON = 0x000000002;
		public const uint SHGFI_SMALLICON = 0x000000001;
		public const uint SHGFI_LARGEICON = 0x000000000; 
		private const int SHIL_JUMBO = 0x4;
		private const int SHIL_EXTRALARGE = 0x2;
		public const uint FILE_ATTRIBUTE_DIRECTORY = 0x00000010;

		public static Bitmap GetFolderIcon(IconSize size, FolderType folderType)
		{
			// Need to add size check, although errors generated at present!    
			uint flags = SHGFI_ICON | SHGFI_USEFILEATTRIBUTES;

			if (FolderType.Open == folderType) flags += SHGFI_OPENICON;

			switch (size)
			{
				case IconSize.Small:
					flags += SHGFI_SMALLICON;
					break;
				case IconSize.ExtraLarge:
					flags += SHIL_EXTRALARGE;
					break;
				case IconSize.Jumbo:
					flags += SHIL_JUMBO;
					break;
				default:
					flags += SHGFI_LARGEICON;
					break;
			}

			// Get the folder icon    
			var shfi = new SHFILEINFO();

			var res = SHGetFileInfo(@"C:\Windows",
				FILE_ATTRIBUTE_DIRECTORY,
				out shfi,
				(uint) Marshal.SizeOf(shfi),
				flags);

			if (res == IntPtr.Zero)
				throw Marshal.GetExceptionForHR(Marshal.GetHRForLastWin32Error());

			// Load the icon from an HICON handle  
			Icon.FromHandle(shfi.hIcon);

			// Now clone the icon, so that it can be successfully stored in an ImageList
			var icon = (Icon) Icon.FromHandle(shfi.hIcon).Clone();

			DestroyIcon(shfi.hIcon); // Cleanup    

			return icon.ToBitmap();
		}

		/// <summary>
		///     Gets the associated icon for a folder
		/// </summary>
		/// <param name="folderPath">Path of the folder</param>
		/// <param name="iconPath">Path of custom icon</param>
		/// <param name="index">Index of the icon in resources</param>
		/// <param name="i"></param>
		public static Bitmap GetIcon(string folderPath, string iconPath, int index, IconSize i)
		{
			if (FrmMain.Icons.ContainsKey(folderPath) && iconPath == "")
				return new Bitmap(FrmMain.Icons[folderPath]);

			if (iconPath != "")
			{
				try
				{
					if (File.Exists(iconPath)) return IconFromFile(iconPath, i, index).ToBitmap();
				}
				catch{}
			}

			Bitmap result;
			bool failed = false;

			try
			{
				Win32.SHFILEINFO shinfo = new Win32.SHFILEINFO();
				
				uint icon;

				switch (i)
				{
					case IconSize.Small:
						icon = SHGFI_SMALLICON;
						break;
					default:
						icon = SHGFI_LARGEICON;
						break;
					case IconSize.ExtraLarge:
						icon = SHIL_EXTRALARGE;
						break;
					case IconSize.Jumbo:
						icon = SHIL_JUMBO;
						break;
				}

				//Call function with the path to the folder you want the icon for
				Win32.SHGetFileInfo(
					folderPath,
					0, ref shinfo, (uint)Marshal.SizeOf(shinfo),
					Win32.SHGFI_ICON | icon);

				result = Icon.FromHandle(shinfo.hIcon).ToBitmap();
			}
			catch
			{
				failed = true;
				result = Resources.Warning;
			}

			if (!failed && iconPath == "") FrmMain.Icons.Add(folderPath, result);
			return result;
		}

		public static Icon IconFromFile(string filename, IconSize size, int index)
		{
			int iconCount = OpenIconDialog.ExtractIconEx(filename, -1, null, null, 0);
			//checks how many icons.
			if (iconCount <= 0 || index >= iconCount) return null;
			// no icons were found.

			IntPtr[] IconPtr = new IntPtr[1];

			//extracts the icon that we want in the selected size.
			if (size == IconSize.Small)
				OpenIconDialog.ExtractIconEx(filename, index, null, IconPtr, 1);
			else
				OpenIconDialog.ExtractIconEx(filename, index, IconPtr, null, 1);

			Icon tempIcon = Icon.FromHandle(IconPtr[0]);

			return OpenIconDialog.GetManagedIcon(ref tempIcon);
		}

	}

	public enum FolderType
	{
		Closed,
		Open
	}

	public enum IconSize
	{
		Jumbo,
		ExtraLarge,
		Large,
		Small
	}

}