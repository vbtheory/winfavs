﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Windows.Forms;
using Microsoft.Win32;

namespace WinFavs.Classes
{
	static class Reggy
	{
		static List<RegEntry> _addedEntries = new List<RegEntry>();
 
		#region Properties

		/// <summary>
		/// Extension .ext
		/// </summary>
		public static string Extension
		{
			get;
			set;
		}
		/// <summary>
		/// Keyname extfile
		/// </summary>
		public static string KeyName
		{
			get;
			set;
		}
		/// <summary>
		/// Icon path
		/// </summary>
		public static string Icon
		{
			get;
			set;
		}
		/// <summary>
		/// File description
		/// </summary>
		public static string Description
		{
			get;
			set;
		}
		/// <summary>
		/// App name for start entry
		/// </summary>
		public static string AppName
		{
			get;
			set;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates the basic association
		/// </summary>
		public static bool CreateRegistry(bool isSilent = false)
		{
			try
			{
				if (!IsUserAdministrator()) throw new Exception("You are not running the app as admin, please run it as an admin and try again");

				RegistryKey ext = Registry.ClassesRoot.CreateSubKey(Extension);
				ext.SetValue("", KeyName, RegistryValueKind.ExpandString);

				RegistryKey key = Registry.ClassesRoot.CreateSubKey(KeyName);
				key.SetValue("", Description, RegistryValueKind.ExpandString);
				key.CreateSubKey("DefaultIcon").SetValue("", Icon, RegistryValueKind.ExpandString);

				Register("open", "Open", "\"" + Application.ExecutablePath + "\"" + " \"%1\"");

				ext.Close();
				key.Close();
				return true;
			}
			catch (Exception ex)
			{
				if(!isSilent) Alert.Show("Failed to create registry: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Logger.Log("Failed to create registry: " + ex.Message, LogType.Error);
				return false;
			}
		}
		/// <summary>
		/// Adds shell entry
		/// </summary>
		/// <param name="keyName">Entry name</param>
		/// <param name="menuText">Entry display name</param>
		/// <param name="menuCommand">Entry command</param>
		public static bool Register(string keyName, string menuText, string menuCommand)
		{
			try
			{
				string regPath = string.Format(@"{0}\shell\{1}", KeyName, keyName);

				using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(regPath))
					key.SetValue(null, menuText);

				using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(string.Format(@"{0}\command", regPath)))
					key.SetValue(null, menuCommand);

				_addedEntries.Add(new RegEntry(keyName, menuText, menuCommand));

				return true;
			}
			catch (Exception ex)
			{
				Alert.Show("Failed to register entry: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Logger.Log("Failed to register entry: " + ex.Message, LogType.Error);
				return false;
			}
		}
		/// <summary>
		/// Removes all association
		/// </summary>
		public static bool Unregister()
		{
			try
			{
				if (!IsUserAdministrator()) throw new Exception("You are not running the app as admin, please run it as an admin and try again");

				if (Registry.ClassesRoot.OpenSubKey(Extension) != null) Registry.ClassesRoot.DeleteSubKeyTree(Extension);
				if (Registry.ClassesRoot.OpenSubKey(KeyName) != null) Registry.ClassesRoot.DeleteSubKeyTree(KeyName);
				return true;
			}
			catch (Exception ex)
			{
				Alert.Show("Failed to remove registry: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Logger.Log("Failed to remove registry: " + ex.Message, LogType.Error);
				return false;
			}
		}
		/// <summary>
		/// Adds app to start
		/// </summary>
		public static bool AddToStart(bool isSilent = false)
		{
			try
			{
				RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
				rkApp.SetValue(AppName, "\"" + Application.ExecutablePath + "\" -s");
				return true;
			}
			catch (Exception ex)
			{
				if (!isSilent) Alert.Show("Failed to add to start: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Logger.Log("Failed to add to start: " + ex.Message, LogType.Error);
				return false;
			}
		}
		/// <summary>
		/// Removes app startup entry
		/// </summary>
		public static bool RemoveFromStart()
		{
			try
			{
				RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
				if (rkApp != null) rkApp.DeleteValue(AppName, false);
				return true;
			}
			catch (Exception ex)
			{
				Alert.Show("Failed to remove from start: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Logger.Log("Failed to remove from start: " + ex.Message, LogType.Error);
				return false;
			}
		}
		/// <summary>
		///     Returns if the app is running in admin mode
		/// </summary>
		public static bool IsUserAdministrator()
		{
			try
			{
				WindowsIdentity user = WindowsIdentity.GetCurrent();
				WindowsPrincipal principal = new WindowsPrincipal(user);
				return principal.IsInRole(WindowsBuiltInRole.Administrator);
			}
			catch
			{
				return false;
			}
		}

		public static RegState CheckStart()
		{
			try
			{
				using(RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
				return ((string)rkApp.GetValue(AppName) == "\"" + Application.ExecutablePath + "\" -s"
										 ? RegState.Available
										 : RegState.NonAvailable);
			}
			catch
			{
				return RegState.Unknown;
			}
		}

		public static RegState CheckAssociation()
		{
			try
			{
				int count = 0;

				RegistryKey ext = Registry.ClassesRoot.CreateSubKey(Extension);
				if ((string) ext.GetValue("") == KeyName) count++;

				RegistryKey k = Registry.ClassesRoot.CreateSubKey(KeyName);
				if ((string)k.GetValue("") == Description) count++;
				if ((string)k.OpenSubKey("DefaultIcon").GetValue("") == Icon) count++;

				foreach (RegEntry e in _addedEntries)
				{
					string regPath = string.Format(@"{0}\shell\{1}", KeyName, e.KeyName);

					using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(regPath))
						if((string) key.GetValue(null) == e.MenuText) count++;

					using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(string.Format(@"{0}\command", regPath)))
						if((string) key.GetValue(null) == e.MenuCommand) count++;
				}

				ext.Close();
				k.Close();

				if(count == 0) return RegState.NonAvailable;
				return count < 3+_addedEntries.Count*2 ? RegState.Incomplete : RegState.Available;
			}
			catch
			{
				return RegState.Unknown;
			}
		}

		#endregion

	}

	public enum RegState
	{
		Available,
		NonAvailable,
		Unknown,
		Incomplete
	}

	public struct RegEntry
	{
		public string KeyName, MenuText, MenuCommand;

		public RegEntry(string keyName, string menuText, string menuCommand)
		{
			KeyName = keyName;
			MenuText = menuText;
			MenuCommand = menuCommand;
		}
	}

}
