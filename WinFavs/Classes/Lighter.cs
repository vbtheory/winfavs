﻿using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using WinFavs.Controls;

namespace WinFavs.Classes
{ 
	public static class Lighter
	{
		private static Hashtable _highlights = new Hashtable();

		public static Highlight GetHighlight(Button p)
		{
			return (Highlight)(_highlights[p] ?? Highlight.None);
		}

		public static void SetHighlight(Button p, Highlight value)
		{
			if (!_highlights.ContainsKey(p))
			{
				p.Paint += MyControlOnPaint;
				p.EnabledChanged += (sender, args) => p.Invalidate();
				p.MouseEnter += (sender, args) => p.AccessibleName = "In";
				p.MouseDown += (sender, args) => p.AccessibleName = "Down";
				p.MouseLeave += (sender, args) => p.AccessibleName = "None";
				p.MouseUp += (sender, args) =>
				{
					p.AccessibleName = "None";
					p.Invalidate();
				};

				p.AccessibleName = "None";
				p.Move += (sender, args) => p.Invalidate();
			}

			_highlights[p] = value;

			p.Invalidate();
			p.FlatStyle = value != Highlight.None || p is SplitButton || p.BackgroundImage != null || p.Image != null
				? FlatStyle.Standard
				: FlatStyle.System;
		}

		private static void MyControlOnPaint(object sender, PaintEventArgs e)
		{
			Color c;
			Button control = ((Button) sender);
			int dec = control.Enabled ? 0 : -10;

			if (control.AccessibleName == "In" && control.Enabled) dec += 50;
			else if (control.AccessibleName == "Down" && control.Enabled) dec += 100;

			switch ((Highlight) (_highlights[sender] ?? Highlight.None))
			{
				case Highlight.Blue:
					c = Color.FromArgb(75 + dec, Color.FromArgb(0x20B2FF));
					break;
				case Highlight.Green:
					c = Color.FromArgb(75 + dec, Color.FromArgb(0xAAFF21));
					break;
				case Highlight.Orange:
					c = Color.FromArgb(75 + dec, Color.FromArgb(0xE8AF39));
					break;
				case Highlight.Red:
					c = Color.FromArgb(55 + dec, Color.FromArgb(0xFF3D2C));
					break;
				case Highlight.Black:
					c = Color.FromArgb(55 + dec, Color.Black);
					break;
				default:
					return;
			}

			e.Graphics.Clear(control.Parent == null ? control.BackColor : control.Parent.BackColor);

			Bitmap b = new Bitmap(e.ClipRectangle.Width, e.ClipRectangle.Height);
			Graphics g = Graphics.FromImage(b);
			
			ButtonRenderer.DrawButton(g, e.ClipRectangle, false, control.Enabled?PushButtonState.Normal:PushButtonState.Disabled);
			GraphicsPath gp = new GraphicsPath();
			gp.AddEllipse(e.ClipRectangle);
			PathGradientBrush pgb = new PathGradientBrush(gp);

			for (int x = 0; x < b.Width; x++)
				for (int y = 0; y < b.Height; y++)
					if (b.GetPixel(x, y).A != 0) g.FillRectangle(new SolidBrush(c), x, y, 1, 1);
			
			pgb.CenterPoint = new PointF(e.ClipRectangle.Width / 2F, e.ClipRectangle.Height / 2F);
			pgb.CenterColor = Color.FromArgb(129, Color.White);
			pgb.SurroundColors = new [] { Color.Transparent };
			g.FillPath(pgb, gp);

			if (control.Focused) ControlPaint.DrawFocusRectangle(g, new Rectangle(e.ClipRectangle.X + 3, e.ClipRectangle.Y + 3, e.ClipRectangle.Width - 6, e.ClipRectangle.Height-6));
			g.DrawString(control.Text.Trim('&'), control.Font, new SolidBrush(control.Enabled?control.ForeColor:SystemColors.GrayText), e.ClipRectangle,
				new StringFormat {LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center});
			if (control.Image != null) g.DrawImage(control.Image, (e.ClipRectangle.Width - control.Image.Width) / 2, (e.ClipRectangle.Height - control.Image.Height) / 2, control.Image.Width, control.Image.Height);

			e.Graphics.DrawImage(b, e.ClipRectangle);

			b.Dispose();
			g.Dispose();
			pgb.Dispose();
			gp.Dispose();

		}

	}

	public enum Highlight
	{
		Blue,
		Red,
		Orange,
		Green,
		Black,
		None
	}
}