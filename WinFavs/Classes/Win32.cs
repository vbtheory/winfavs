﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WinFavs.Classes
{
	internal static class Win32
	{
		private const int EM_SETCUEBANNER = 0x1501;

		//Constants flags for SHGetFileInfo 
		public const uint SHGFI_ICON = 0x100;
		public const uint SHGFI_LARGEICON = 0x0; // 'Large icon

		//Import SHGetFileInfo function

		public static bool Is64BitProcess
		{
			get { return IntPtr.Size == 8; }
		}

		public static bool Is64BitOperatingSystem
		{
			get
			{
				// Clearly if this is a 64-bit process we must be on a 64-bit OS.
				if (Is64BitProcess)
					return true;
				// Ok, so we are a 32-bit process, but is the OS 64-bit?
				// If we are running under Wow64 than the OS is 64-bit.
				bool isWow64;
				return ModuleContainsFunction("kernel32.dll", "IsWow64Process") && IsWow64Process(GetCurrentProcess(), out isWow64) &&
				       isWow64;
			}
		}

		[DllImport("uxtheme", CharSet = CharSet.Unicode)]
		public static extern Int32 SetWindowTheme
			(IntPtr hWnd, String textSubAppName, String textSubIdList);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern Int32
			SendMessage(
			IntPtr hWnd,
			int msg,
			int wParam,
			[MarshalAs(UnmanagedType.LPWStr)] string lParam
			);

		public static void SetCueText(TextBox t, string cue)
		{
			SendMessage(t.Handle, EM_SETCUEBANNER, 0, cue);
		}

		[DllImport("shell32.dll")]
		public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi,
			uint cbSizeFileInfo, uint uFlags);

		private static bool ModuleContainsFunction(string moduleName, string methodName)
		{
			IntPtr hModule = GetModuleHandle(moduleName);
			if (hModule != IntPtr.Zero)
				return GetProcAddress(hModule, methodName) != IntPtr.Zero;
			return false;
		}

		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool IsWow64Process(IntPtr hProcess, [MarshalAs(UnmanagedType.Bool)] out bool isWow64);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetCurrentProcess();

		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		private static extern IntPtr GetModuleHandle(string moduleName);

		[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
		private static extern IntPtr GetProcAddress(IntPtr hModule, string methodName);

		[StructLayout(LayoutKind.Sequential)]
		public struct SHFILEINFO
		{
			public IntPtr hIcon;
			public int iIcon;
			public uint dwAttributes;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)] public string szDisplayName;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)] public string szTypeName;
		};

		[DllImport("user32")]
		public static extern UInt32 SendMessage
			(IntPtr hWnd, UInt32 msg, UInt32 wParam, UInt32 lParam);

		internal const int BCM_FIRST = 0x1600; //Normal button
		internal const int BCM_SETSHIELD = (BCM_FIRST + 0x000C); //Elevated button[System.Runtime.InteropServices.DllImport("dwmapi.dll")]
		
		[DllImport("dwmapi.dll")]
		public static extern int DwmIsCompositionEnabled(out bool enabled);

		[DllImport("dwmapi.dll")]
		public extern static int DwmExtendFrameIntoClientArea(IntPtr hwnd, ref MARGINS margin ) ;
	}
}