﻿using WinFavs.Controls;

namespace WinFavs
{
	partial class FrmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
			System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Available", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Non available", System.Windows.Forms.HorizontalAlignment.Left);
			this.ImgLarge = new System.Windows.Forms.ImageList(this.components);
			this.ImgSmall = new System.Windows.Forms.ImageList(this.components);
			this.CbxProfiles = new System.Windows.Forms.ComboBox();
			this.MspMain = new System.Windows.Forms.MenuStrip();
			this.MsiWinFavs = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiShowProfiles = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiShowTasks = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiSettings = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiExit = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiProfile = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiNew = new System.Windows.Forms.ToolStripMenuItem();
			this.CmsSplit = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.CmiNew = new System.Windows.Forms.ToolStripMenuItem();
			this.CsiFromProfile = new System.Windows.Forms.ToolStripMenuItem();
			this.CmiFromProfile = new System.Windows.Forms.ToolStripMenuItem();
			this.CsiFromDefault = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiEdit = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiDelete = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiApply = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiRefresh = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiTasks = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiTask = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiOpenTask = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiRefreshTasks = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiMore = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiAbout = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiDonate = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiCheckForUpdates = new System.Windows.Forms.ToolStripMenuItem();
			this.MsiFeedback = new System.Windows.Forms.ToolStripMenuItem();
			this.experimentalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openLogReaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.showLoaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ownerDrawTooltipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tryALERTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.TmrCheckEnable = new System.Windows.Forms.Timer(this.components);
			this.NtiMain = new System.Windows.Forms.NotifyIcon(this.components);
			this.CmsNotify = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.CmiProfiles = new System.Windows.Forms.ToolStripMenuItem();
			this.CmiRefresh = new System.Windows.Forms.ToolStripMenuItem();
			this.CmiShow = new System.Windows.Forms.ToolStripMenuItem();
			this.CmiExit = new System.Windows.Forms.ToolStripMenuItem();
			this.LblProfiles = new System.Windows.Forms.Label();
			this.TxtSearch = new System.Windows.Forms.TextBox();
			this.TmrTasks = new System.Windows.Forms.Timer(this.components);
			this.TtpMain = new System.Windows.Forms.ToolTip(this.components);
			this.BtnEdit = new System.Windows.Forms.Button();
			this.BtnApply = new System.Windows.Forms.Button();
			this.BtnRefresh = new System.Windows.Forms.Button();
			this.BtnOpen = new System.Windows.Forms.Button();
			this.BtnTask = new System.Windows.Forms.Button();
			this.BtnDelete = new System.Windows.Forms.Button();
			this.PbxSearch = new System.Windows.Forms.PictureBox();
			this.TmrEgg = new System.Windows.Forms.Timer(this.components);
			this.LsvProfile = new System.Windows.Forms.ListView();
			this.HdrName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.HdrPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.txtBox1 = new WinFavs.Classes.TxtBox();
			this.BtnNew = new WinFavs.Controls.SplitButton();
			this.sep1 = new WinFavs.Controls.Sep();
			this.sep12 = new WinFavs.Controls.Sep();
			this.sep2 = new WinFavs.Controls.Sep();
			this.sep3 = new WinFavs.Controls.Sep();
			this.sep4 = new WinFavs.Controls.Sep();
			this.sep7 = new WinFavs.Controls.Sep();
			this.sep5 = new WinFavs.Controls.Sep();
			this.sep10 = new WinFavs.Controls.Sep();
			this.FswProfiles = new WinFavs.Controls.DelayedWatcher();
			this.FswTasks = new WinFavs.Controls.DelayedWatcher();
			this.MspMain.SuspendLayout();
			this.CmsSplit.SuspendLayout();
			this.CmsNotify.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PbxSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FswProfiles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FswTasks)).BeginInit();
			this.SuspendLayout();
			// 
			// ImgLarge
			// 
			this.ImgLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ImgLarge.ImageSize = new System.Drawing.Size(32, 32);
			this.ImgLarge.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// ImgSmall
			// 
			this.ImgSmall.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ImgSmall.ImageSize = new System.Drawing.Size(24, 24);
			this.ImgSmall.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// CbxProfiles
			// 
			this.CbxProfiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CbxProfiles.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.CbxProfiles.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.CbxProfiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbxProfiles.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.CbxProfiles.FormattingEnabled = true;
			this.CbxProfiles.Location = new System.Drawing.Point(63, 44);
			this.CbxProfiles.Name = "CbxProfiles";
			this.CbxProfiles.Size = new System.Drawing.Size(490, 21);
			this.CbxProfiles.TabIndex = 0;
			this.TtpMain.SetToolTip(this.CbxProfiles, "Use this list to browse through all loaded profiles");
			this.CbxProfiles.SelectedIndexChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// MspMain
			// 
			this.MspMain.AutoSize = false;
			this.MspMain.Dock = System.Windows.Forms.DockStyle.None;
			this.MspMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MsiWinFavs,
            this.MsiProfile,
            this.MsiTasks,
            this.MsiMore});
			this.MspMain.Location = new System.Drawing.Point(0, 2);
			this.MspMain.Name = "MspMain";
			this.MspMain.Size = new System.Drawing.Size(281, 29);
			this.MspMain.TabIndex = 10;
			this.MspMain.Text = "WinFavs";
			// 
			// MsiWinFavs
			// 
			this.MsiWinFavs.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MsiShowProfiles,
            this.MsiShowTasks,
            this.sep1,
            this.MsiSettings,
            this.sep12,
            this.MsiExit});
			this.MsiWinFavs.Image = global::WinFavs.Properties.Resources.OpenMenu;
			this.MsiWinFavs.Name = "MsiWinFavs";
			this.MsiWinFavs.Size = new System.Drawing.Size(79, 25);
			this.MsiWinFavs.Text = "WinFavs";
			// 
			// MsiShowProfiles
			// 
			this.MsiShowProfiles.Name = "MsiShowProfiles";
			this.MsiShowProfiles.Size = new System.Drawing.Size(218, 22);
			this.MsiShowProfiles.Text = "Show profiles folder";
			this.MsiShowProfiles.ToolTipText = "Opens up the folder where all profiles are stored";
			this.MsiShowProfiles.Click += new System.EventHandler(this.MsiShowProfiles_Click);
			// 
			// MsiShowTasks
			// 
			this.MsiShowTasks.Name = "MsiShowTasks";
			this.MsiShowTasks.Size = new System.Drawing.Size(218, 22);
			this.MsiShowTasks.Text = "Show tasks folder";
			this.MsiShowTasks.ToolTipText = "Opens up the folder where all tasks are stored";
			this.MsiShowTasks.Click += new System.EventHandler(this.MsiShowTasks_Click_1);
			// 
			// MsiSettings
			// 
			this.MsiSettings.Image = global::WinFavs.Properties.Resources.Settings;
			this.MsiSettings.Name = "MsiSettings";
			this.MsiSettings.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
			this.MsiSettings.Size = new System.Drawing.Size(218, 22);
			this.MsiSettings.Text = "Settings                      ";
			this.MsiSettings.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
			// 
			// MsiExit
			// 
			this.MsiExit.Image = global::WinFavs.Properties.Resources.Delete;
			this.MsiExit.Name = "MsiExit";
			this.MsiExit.ShortcutKeyDisplayString = "Alt+F4";
			this.MsiExit.Size = new System.Drawing.Size(218, 22);
			this.MsiExit.Text = "Exit";
			this.MsiExit.ToolTipText = "Exit WinFavs";
			this.MsiExit.Click += new System.EventHandler(this.MsiExit_Click);
			// 
			// MsiProfile
			// 
			this.MsiProfile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MsiOpen,
            this.MsiNew,
            this.sep2,
            this.MsiEdit,
            this.MsiDelete,
            this.MsiApply,
            this.sep3,
            this.MsiRefresh});
			this.MsiProfile.Image = global::WinFavs.Properties.Resources.ProfileMenu;
			this.MsiProfile.Name = "MsiProfile";
			this.MsiProfile.Size = new System.Drawing.Size(69, 25);
			this.MsiProfile.Text = "Profile";
			// 
			// MsiOpen
			// 
			this.MsiOpen.Image = global::WinFavs.Properties.Resources.Open;
			this.MsiOpen.Name = "MsiOpen";
			this.MsiOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.O)));
			this.MsiOpen.Size = new System.Drawing.Size(204, 22);
			this.MsiOpen.Text = "Open";
			this.MsiOpen.ToolTipText = "Open up a .wfp file ";
			this.MsiOpen.Click += new System.EventHandler(this.MsiOpen_Click);
			// 
			// MsiNew
			// 
			this.MsiNew.DropDown = this.CmsSplit;
			this.MsiNew.Image = global::WinFavs.Properties.Resources.New;
			this.MsiNew.Name = "MsiNew";
			this.MsiNew.ShortcutKeyDisplayString = "";
			this.MsiNew.Size = new System.Drawing.Size(204, 22);
			this.MsiNew.Text = "New";
			// 
			// CmsSplit
			// 
			this.CmsSplit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiNew,
            this.CsiFromProfile,
            this.CmiFromProfile,
            this.CsiFromDefault});
			this.CmsSplit.Name = "CmsSplit";
			this.CmsSplit.OwnerItem = this.MsiNew;
			this.CmsSplit.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.CmsSplit.Size = new System.Drawing.Size(231, 92);
			// 
			// CmiNew
			// 
			this.CmiNew.Image = global::WinFavs.Properties.Resources.Profile;
			this.CmiNew.Name = "CmiNew";
			this.CmiNew.ShortcutKeyDisplayString = "";
			this.CmiNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.N)));
			this.CmiNew.Size = new System.Drawing.Size(230, 22);
			this.CmiNew.Text = "Profile";
			this.CmiNew.ToolTipText = "Creates a new  profile";
			this.CmiNew.Click += new System.EventHandler(this.MsiNewProfile_Click);
			// 
			// CsiFromProfile
			// 
			this.CsiFromProfile.Name = "CsiFromProfile";
			this.CsiFromProfile.ShortcutKeyDisplayString = "";
			this.CsiFromProfile.Size = new System.Drawing.Size(230, 22);
			this.CsiFromProfile.Text = "From this profile";
			this.CsiFromProfile.ToolTipText = "Creates a new profile the profile you are currently viewing";
			this.CsiFromProfile.Click += new System.EventHandler(this.MsiFromProfile_Click);
			// 
			// CmiFromProfile
			// 
			this.CmiFromProfile.Name = "CmiFromProfile";
			this.CmiFromProfile.ShortcutKeyDisplayString = "";
			this.CmiFromProfile.Size = new System.Drawing.Size(230, 22);
			this.CmiFromProfile.Text = "From current favorites             ";
			this.CmiFromProfile.ToolTipText = "Creates a new profile from the shortcuts in Favorites";
			this.CmiFromProfile.Click += new System.EventHandler(this.MsiFromSetup_Click);
			// 
			// CsiFromDefault
			// 
			this.CsiFromDefault.Name = "CsiFromDefault";
			this.CsiFromDefault.ShortcutKeyDisplayString = "";
			this.CsiFromDefault.Size = new System.Drawing.Size(230, 22);
			this.CsiFromDefault.Text = "From default favorites";
			this.CsiFromDefault.ToolTipText = "Creates a new profile from the default shortcuts";
			this.CsiFromDefault.Click += new System.EventHandler(this.MsiFromDefault_Click);
			// 
			// MsiEdit
			// 
			this.MsiEdit.Image = global::WinFavs.Properties.Resources.Edit;
			this.MsiEdit.Name = "MsiEdit";
			this.MsiEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
			this.MsiEdit.Size = new System.Drawing.Size(204, 22);
			this.MsiEdit.Text = "Edit";
			this.MsiEdit.ToolTipText = "Opens a new window where you can edit the shortcuts of this profile";
			this.MsiEdit.Click += new System.EventHandler(this.MsiEdit_Click);
			// 
			// MsiDelete
			// 
			this.MsiDelete.Image = global::WinFavs.Properties.Resources.Delete1;
			this.MsiDelete.Name = "MsiDelete";
			this.MsiDelete.ShortcutKeyDisplayString = "";
			this.MsiDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Delete)));
			this.MsiDelete.Size = new System.Drawing.Size(204, 22);
			this.MsiDelete.Text = "Remove";
			this.MsiDelete.ToolTipText = "Delete this profile";
			this.MsiDelete.Click += new System.EventHandler(this.MsiDelete_Click);
			// 
			// MsiApply
			// 
			this.MsiApply.Name = "MsiApply";
			this.MsiApply.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
			this.MsiApply.Size = new System.Drawing.Size(204, 22);
			this.MsiApply.Text = "Apply profile        ";
			this.MsiApply.ToolTipText = "Replaces the currently installed shortcuts with the ones in this profile";
			this.MsiApply.Click += new System.EventHandler(this.MsiApply_Click);
			// 
			// MsiRefresh
			// 
			this.MsiRefresh.Image = global::WinFavs.Properties.Resources.Refresh;
			this.MsiRefresh.Name = "MsiRefresh";
			this.MsiRefresh.ShortcutKeys = System.Windows.Forms.Keys.F5;
			this.MsiRefresh.Size = new System.Drawing.Size(204, 22);
			this.MsiRefresh.Text = "Refresh profiles";
			this.MsiRefresh.ToolTipText = "Scans the profiles folder for any new profiles";
			this.MsiRefresh.Click += new System.EventHandler(this.MsiRefresh_Click);
			// 
			// MsiTasks
			// 
			this.MsiTasks.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MsiTask,
            this.MsiOpenTask,
            this.sep4,
            this.MsiRefreshTasks});
			this.MsiTasks.Image = global::WinFavs.Properties.Resources.TasksMenu;
			this.MsiTasks.Name = "MsiTasks";
			this.MsiTasks.Size = new System.Drawing.Size(64, 25);
			this.MsiTasks.Text = "Tasks";
			// 
			// MsiTask
			// 
			this.MsiTask.Image = global::WinFavs.Properties.Resources.Schedule;
			this.MsiTask.Name = "MsiTask";
			this.MsiTask.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
			this.MsiTask.Size = new System.Drawing.Size(235, 22);
			this.MsiTask.Text = "Schedule this profile      ";
			this.MsiTask.ToolTipText = "Create a task based on this profile.";
			this.MsiTask.Click += new System.EventHandler(this.MsiTask_Click);
			// 
			// MsiOpenTask
			// 
			this.MsiOpenTask.Image = global::WinFavs.Properties.Resources.Tasks;
			this.MsiOpenTask.Name = "MsiOpenTask";
			this.MsiOpenTask.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.T)));
			this.MsiOpenTask.Size = new System.Drawing.Size(235, 22);
			this.MsiOpenTask.Text = "Show tasks";
			this.MsiOpenTask.ToolTipText = "Shows all the tasks currently available";
			this.MsiOpenTask.Click += new System.EventHandler(this.MsiShowTasks_Click);
			// 
			// MsiRefreshTasks
			// 
			this.MsiRefreshTasks.Image = global::WinFavs.Properties.Resources.Refresh;
			this.MsiRefreshTasks.Name = "MsiRefreshTasks";
			this.MsiRefreshTasks.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F5)));
			this.MsiRefreshTasks.Size = new System.Drawing.Size(235, 22);
			this.MsiRefreshTasks.Text = "Refresh tasks";
			this.MsiRefreshTasks.ToolTipText = "Scans  the tasks folder for any new  tasks";
			this.MsiRefreshTasks.Click += new System.EventHandler(this.MsiRefreshTasks_Click);
			// 
			// MsiMore
			// 
			this.MsiMore.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MsiAbout,
            this.MsiDonate,
            this.sep7,
            this.MsiCheckForUpdates,
            this.sep5,
            this.MsiFeedback,
            this.experimentalToolStripMenuItem});
			this.MsiMore.Image = global::WinFavs.Properties.Resources.NewMenu;
			this.MsiMore.Name = "MsiMore";
			this.MsiMore.ShortcutKeyDisplayString = "";
			this.MsiMore.Size = new System.Drawing.Size(63, 25);
			this.MsiMore.Text = "More";
			// 
			// MsiAbout
			// 
			this.MsiAbout.Image = global::WinFavs.Properties.Resources.About;
			this.MsiAbout.Name = "MsiAbout";
			this.MsiAbout.ShortcutKeys = System.Windows.Forms.Keys.F1;
			this.MsiAbout.Size = new System.Drawing.Size(228, 22);
			this.MsiAbout.Text = "About";
			this.MsiAbout.ToolTipText = "General information about the app and its maker";
			this.MsiAbout.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// MsiDonate
			// 
			this.MsiDonate.Name = "MsiDonate";
			this.MsiDonate.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D)));
			this.MsiDonate.Size = new System.Drawing.Size(228, 22);
			this.MsiDonate.Text = "Support this project    ";
			this.MsiDonate.ToolTipText = "Click here to donate to the developer to keep this project running";
			this.MsiDonate.Click += new System.EventHandler(this.donateToolStripMenuItem_Click);
			// 
			// MsiCheckForUpdates
			// 
			this.MsiCheckForUpdates.Image = global::WinFavs.Properties.Resources.Update;
			this.MsiCheckForUpdates.Name = "MsiCheckForUpdates";
			this.MsiCheckForUpdates.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.U)));
			this.MsiCheckForUpdates.Size = new System.Drawing.Size(228, 22);
			this.MsiCheckForUpdates.Text = "Check for updates";
			this.MsiCheckForUpdates.Click += new System.EventHandler(this.MsiCheckForUpdates_Click);
			// 
			// MsiFeedback
			// 
			this.MsiFeedback.Image = global::WinFavs.Properties.Resources.Feedback;
			this.MsiFeedback.Name = "MsiFeedback";
			this.MsiFeedback.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
			this.MsiFeedback.Size = new System.Drawing.Size(228, 22);
			this.MsiFeedback.Text = "Send feedback";
			this.MsiFeedback.ToolTipText = "Send your thoughts about the app in order to improve further releases";
			this.MsiFeedback.Click += new System.EventHandler(this.MsiFeedback_Click);
			// 
			// experimentalToolStripMenuItem
			// 
			this.experimentalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openLogReaderToolStripMenuItem,
            this.showLoaderToolStripMenuItem,
            this.ownerDrawTooltipToolStripMenuItem,
            this.tryALERTToolStripMenuItem});
			this.experimentalToolStripMenuItem.Name = "experimentalToolStripMenuItem";
			this.experimentalToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
			this.experimentalToolStripMenuItem.Text = "Experimental";
			// 
			// openLogReaderToolStripMenuItem
			// 
			this.openLogReaderToolStripMenuItem.Name = "openLogReaderToolStripMenuItem";
			this.openLogReaderToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.openLogReaderToolStripMenuItem.Text = "Open log reader";
			this.openLogReaderToolStripMenuItem.Click += new System.EventHandler(this.openLogReaderToolStripMenuItem_Click);
			// 
			// showLoaderToolStripMenuItem
			// 
			this.showLoaderToolStripMenuItem.Name = "showLoaderToolStripMenuItem";
			this.showLoaderToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.showLoaderToolStripMenuItem.Text = "Show loader";
			this.showLoaderToolStripMenuItem.Click += new System.EventHandler(this.showLoaderToolStripMenuItem_Click);
			// 
			// ownerDrawTooltipToolStripMenuItem
			// 
			this.ownerDrawTooltipToolStripMenuItem.Name = "ownerDrawTooltipToolStripMenuItem";
			this.ownerDrawTooltipToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.ownerDrawTooltipToolStripMenuItem.Text = "OwnerDraw tooltip";
			this.ownerDrawTooltipToolStripMenuItem.Click += new System.EventHandler(this.ownerDrawTooltipToolStripMenuItem_Click);
			// 
			// tryALERTToolStripMenuItem
			// 
			this.tryALERTToolStripMenuItem.Name = "tryALERTToolStripMenuItem";
			this.tryALERTToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.tryALERTToolStripMenuItem.Text = "Try ALERT";
			this.tryALERTToolStripMenuItem.Click += new System.EventHandler(this.tryALERTToolStripMenuItem_Click);
			// 
			// TmrCheckEnable
			// 
			this.TmrCheckEnable.Enabled = true;
			this.TmrCheckEnable.Interval = 10;
			this.TmrCheckEnable.Tick += new System.EventHandler(this.TmrCheckEnable_Tick);
			// 
			// NtiMain
			// 
			this.NtiMain.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.NtiMain.BalloonTipText = "WinFavs is now running in the background.\r\nYou can now right click the icon to qu" +
    "ickly change profiles or double click it show it back again.";
			this.NtiMain.BalloonTipTitle = "WinFavs is running in the background";
			this.NtiMain.ContextMenuStrip = this.CmsNotify;
			this.NtiMain.Icon = ((System.Drawing.Icon)(resources.GetObject("NtiMain.Icon")));
			this.NtiMain.Text = "WinFavs";
			this.NtiMain.Visible = true;
			this.NtiMain.DoubleClick += new System.EventHandler(this.showToolStripMenuItem_Click);
			// 
			// CmsNotify
			// 
			this.CmsNotify.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiProfiles,
            this.CmiRefresh,
            this.sep10,
            this.CmiShow,
            this.CmiExit});
			this.CmsNotify.Name = "CmsNotify";
			this.CmsNotify.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.CmsNotify.Size = new System.Drawing.Size(156, 98);
			// 
			// CmiProfiles
			// 
			this.CmiProfiles.Name = "CmiProfiles";
			this.CmiProfiles.Size = new System.Drawing.Size(155, 22);
			this.CmiProfiles.Text = "Apply";
			// 
			// CmiRefresh
			// 
			this.CmiRefresh.Image = ((System.Drawing.Image)(resources.GetObject("CmiRefresh.Image")));
			this.CmiRefresh.Name = "CmiRefresh";
			this.CmiRefresh.Size = new System.Drawing.Size(155, 22);
			this.CmiRefresh.Text = "Refresh profiles";
			this.CmiRefresh.Click += new System.EventHandler(this.MsiRefresh_Click);
			// 
			// CmiShow
			// 
			this.CmiShow.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
			this.CmiShow.Image = global::WinFavs.Properties.Resources.parent;
			this.CmiShow.Name = "CmiShow";
			this.CmiShow.Size = new System.Drawing.Size(155, 22);
			this.CmiShow.Text = "Show WinFavs";
			this.CmiShow.Visible = false;
			this.CmiShow.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
			// 
			// CmiExit
			// 
			this.CmiExit.Image = ((System.Drawing.Image)(resources.GetObject("CmiExit.Image")));
			this.CmiExit.Name = "CmiExit";
			this.CmiExit.Size = new System.Drawing.Size(155, 22);
			this.CmiExit.Text = "Exit";
			this.CmiExit.Click += new System.EventHandler(this.MsiExit_Click);
			// 
			// LblProfiles
			// 
			this.LblProfiles.AutoSize = true;
			this.LblProfiles.BackColor = System.Drawing.Color.Transparent;
			this.LblProfiles.Location = new System.Drawing.Point(12, 43);
			this.LblProfiles.Name = "LblProfiles";
			this.LblProfiles.Size = new System.Drawing.Size(44, 13);
			this.LblProfiles.TabIndex = 8;
			this.LblProfiles.Text = "Profiles:";
			// 
			// TxtSearch
			// 
			this.TxtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.TxtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.TxtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.TxtSearch.BackColor = System.Drawing.Color.White;
			this.TxtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TxtSearch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.TxtSearch.Location = new System.Drawing.Point(489, 6);
			this.TxtSearch.Name = "TxtSearch";
			this.TxtSearch.Size = new System.Drawing.Size(212, 20);
			this.TxtSearch.TabIndex = 8;
			this.TtpMain.SetToolTip(this.TxtSearch, "Search for profiles");
			this.TxtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtSearch_KeyDown);
			// 
			// TmrTasks
			// 
			this.TmrTasks.Enabled = true;
			this.TmrTasks.Interval = 1;
			this.TmrTasks.Tick += new System.EventHandler(this.tmrTasks_Tick);
			// 
			// TtpMain
			// 
			this.TtpMain.AutoPopDelay = 5000;
			this.TtpMain.InitialDelay = 250;
			this.TtpMain.ReshowDelay = 100;
			this.TtpMain.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.TtpMain_Draw);
			// 
			// BtnEdit
			// 
			this.BtnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnEdit.Enabled = false;
			this.BtnEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnEdit.Location = new System.Drawing.Point(559, 102);
			this.BtnEdit.Name = "BtnEdit";
			this.BtnEdit.Size = new System.Drawing.Size(139, 23);
			this.BtnEdit.TabIndex = 3;
			this.BtnEdit.Text = "Edit (Alt+E)";
			this.TtpMain.SetToolTip(this.BtnEdit, "Opens a new window where you can edit the shortcuts of this profile");
			this.BtnEdit.UseVisualStyleBackColor = true;
			this.BtnEdit.Click += new System.EventHandler(this.MsiEdit_Click);
			// 
			// BtnApply
			// 
			this.BtnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnApply.Enabled = false;
			this.BtnApply.Location = new System.Drawing.Point(559, 387);
			this.BtnApply.Name = "BtnApply";
			this.BtnApply.Size = new System.Drawing.Size(139, 23);
			this.BtnApply.TabIndex = 6;
			this.BtnApply.Text = "Apply profile (Alt+A)";
			this.TtpMain.SetToolTip(this.BtnApply, "Replaces the currently installed shortcuts with the ones in this profile");
			this.BtnApply.UseVisualStyleBackColor = true;
			this.BtnApply.Click += new System.EventHandler(this.MsiApply_Click);
			// 
			// BtnRefresh
			// 
			this.BtnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnRefresh.Location = new System.Drawing.Point(559, 416);
			this.BtnRefresh.Name = "BtnRefresh";
			this.BtnRefresh.Size = new System.Drawing.Size(139, 23);
			this.BtnRefresh.TabIndex = 7;
			this.BtnRefresh.Text = "Refresh profiles (F5)";
			this.TtpMain.SetToolTip(this.BtnRefresh, "Scans the profiles folder for any new profiles");
			this.BtnRefresh.UseVisualStyleBackColor = true;
			this.BtnRefresh.Click += new System.EventHandler(this.MsiRefresh_Click);
			// 
			// BtnOpen
			// 
			this.BtnOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnOpen.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnOpen.Location = new System.Drawing.Point(559, 44);
			this.BtnOpen.Name = "BtnOpen";
			this.BtnOpen.Size = new System.Drawing.Size(139, 23);
			this.BtnOpen.TabIndex = 1;
			this.BtnOpen.Text = "Open profile (Alt+O)";
			this.TtpMain.SetToolTip(this.BtnOpen, "Open up a .wfp file ");
			this.BtnOpen.UseVisualStyleBackColor = true;
			this.BtnOpen.Click += new System.EventHandler(this.MsiOpen_Click);
			// 
			// BtnTask
			// 
			this.BtnTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnTask.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnTask.Location = new System.Drawing.Point(559, 131);
			this.BtnTask.Name = "BtnTask";
			this.BtnTask.Size = new System.Drawing.Size(139, 23);
			this.BtnTask.TabIndex = 4;
			this.BtnTask.Text = "Schedule (Alt+S)";
			this.TtpMain.SetToolTip(this.BtnTask, "Create a task based on this profile.");
			this.BtnTask.UseVisualStyleBackColor = true;
			this.BtnTask.Click += new System.EventHandler(this.MsiTask_Click);
			// 
			// BtnDelete
			// 
			this.BtnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnDelete.Enabled = false;
			this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnDelete.Location = new System.Drawing.Point(559, 160);
			this.BtnDelete.Name = "BtnDelete";
			this.BtnDelete.Size = new System.Drawing.Size(139, 23);
			this.BtnDelete.TabIndex = 5;
			this.BtnDelete.Text = "Remove (Alt+Del)";
			this.TtpMain.SetToolTip(this.BtnDelete, "Delete this profile");
			this.BtnDelete.UseVisualStyleBackColor = true;
			this.BtnDelete.Click += new System.EventHandler(this.MsiDelete_Click);
			// 
			// PbxSearch
			// 
			this.PbxSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.PbxSearch.BackColor = System.Drawing.Color.White;
			this.PbxSearch.Image = global::WinFavs.Properties.Resources.Search_Find;
			this.PbxSearch.Location = new System.Drawing.Point(683, 8);
			this.PbxSearch.Name = "PbxSearch";
			this.PbxSearch.Size = new System.Drawing.Size(16, 16);
			this.PbxSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.PbxSearch.TabIndex = 12;
			this.PbxSearch.TabStop = false;
			this.TtpMain.SetToolTip(this.PbxSearch, "Search for profiles");
			this.PbxSearch.Click += new System.EventHandler(this.BtnSearch_Click);
			// 
			// TmrEgg
			// 
			this.TmrEgg.Interval = 250;
			this.TmrEgg.Tick += new System.EventHandler(this.TmrEgg_Tick);
			// 
			// LsvProfile
			// 
			this.LsvProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LsvProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.LsvProfile.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.HdrName,
            this.HdrPath});
			this.LsvProfile.ForeColor = System.Drawing.Color.Black;
			this.LsvProfile.GridLines = true;
			listViewGroup1.Header = "Available";
			listViewGroup1.Name = "GrpAvailable";
			listViewGroup2.Header = "Non available";
			listViewGroup2.Name = "GrpNonAvailable";
			this.LsvProfile.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
			this.LsvProfile.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.LsvProfile.LargeImageList = this.ImgLarge;
			this.LsvProfile.Location = new System.Drawing.Point(12, 71);
			this.LsvProfile.MultiSelect = false;
			this.LsvProfile.Name = "LsvProfile";
			this.LsvProfile.ShowItemToolTips = true;
			this.LsvProfile.Size = new System.Drawing.Size(541, 368);
			this.LsvProfile.SmallImageList = this.ImgSmall;
			this.LsvProfile.TabIndex = 2;
			this.LsvProfile.TabStop = false;
			this.LsvProfile.UseCompatibleStateImageBehavior = false;
			this.LsvProfile.View = System.Windows.Forms.View.Details;
			this.LsvProfile.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LsvList_ItemSelectionChanged);
			this.LsvProfile.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.LsvProfile_RetrieveVirtualItem);
			this.LsvProfile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LsvList_KeyDown);
			// 
			// HdrName
			// 
			this.HdrName.Text = "Name";
			// 
			// HdrPath
			// 
			this.HdrPath.Text = "Path";
			this.HdrPath.Width = 448;
			// 
			// txtBox1
			// 
			this.txtBox1.BorderColor = System.Drawing.Color.Black;
			this.txtBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtBox1.CaretColor = System.Drawing.Color.Black;
			this.txtBox1.CaretPosition = 0;
			this.txtBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtBox1.FixedSingleStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
			this.txtBox1.Location = new System.Drawing.Point(559, 239);
			this.txtBox1.Name = "txtBox1";
			this.txtBox1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
			this.txtBox1.SelectionStart = -1;
			this.txtBox1.Size = new System.Drawing.Size(150, 130);
			this.txtBox1.TabIndex = 13;
			this.txtBox1.Text = "txtBox1 arfeadc aezfzef";
			// 
			// BtnNew
			// 
			this.BtnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnNew.AutoSize = true;
			this.BtnNew.ContextMenuStrip = this.CmsSplit;
			this.BtnNew.ForeColor = System.Drawing.Color.Black;
			this.BtnNew.Location = new System.Drawing.Point(560, 73);
			this.BtnNew.Name = "BtnNew";
			this.BtnNew.Size = new System.Drawing.Size(138, 23);
			this.BtnNew.SplitMenuStrip = this.CmsSplit;
			this.BtnNew.TabIndex = 2;
			this.BtnNew.Text = "New (Alt+N)";
			this.TtpMain.SetToolTip(this.BtnNew, "Creates a new  profile");
			this.BtnNew.UseVisualStyleBackColor = true;
			this.BtnNew.Click += new System.EventHandler(this.MsiNewProfile_Click);
			// 
			// sep1
			// 
			this.sep1.Name = "sep1";
			this.sep1.Size = new System.Drawing.Size(215, 6);
			// 
			// sep12
			// 
			this.sep12.Name = "sep12";
			this.sep12.Size = new System.Drawing.Size(215, 6);
			// 
			// sep2
			// 
			this.sep2.Name = "sep2";
			this.sep2.Size = new System.Drawing.Size(201, 6);
			// 
			// sep3
			// 
			this.sep3.Name = "sep3";
			this.sep3.Size = new System.Drawing.Size(201, 6);
			// 
			// sep4
			// 
			this.sep4.Name = "sep4";
			this.sep4.Size = new System.Drawing.Size(232, 6);
			// 
			// sep7
			// 
			this.sep7.Name = "sep7";
			this.sep7.Size = new System.Drawing.Size(225, 6);
			// 
			// sep5
			// 
			this.sep5.Name = "sep5";
			this.sep5.Size = new System.Drawing.Size(225, 6);
			// 
			// sep10
			// 
			this.sep10.Name = "sep10";
			this.sep10.Size = new System.Drawing.Size(152, 6);
			// 
			// FswProfiles
			// 
			this.FswProfiles.EnableRaisingEvents = true;
			this.FswProfiles.Interval = 500;
			this.FswProfiles.NotifyFilter = ((System.IO.NotifyFilters)((((((System.IO.NotifyFilters.FileName | System.IO.NotifyFilters.Attributes) 
            | System.IO.NotifyFilters.Size) 
            | System.IO.NotifyFilters.LastWrite) 
            | System.IO.NotifyFilters.LastAccess) 
            | System.IO.NotifyFilters.CreationTime)));
			this.FswProfiles.SynchronizingObject = this;
			this.FswProfiles.ChangeDetected += new System.EventHandler(this.FswProfiles_Change);
			// 
			// FswTasks
			// 
			this.FswTasks.EnableRaisingEvents = true;
			this.FswTasks.Interval = 500;
			this.FswTasks.NotifyFilter = ((System.IO.NotifyFilters)((((((System.IO.NotifyFilters.FileName | System.IO.NotifyFilters.Attributes) 
            | System.IO.NotifyFilters.Size) 
            | System.IO.NotifyFilters.LastWrite) 
            | System.IO.NotifyFilters.LastAccess) 
            | System.IO.NotifyFilters.CreationTime)));
			this.FswTasks.SynchronizingObject = this;
			this.FswTasks.ChangeDetected += new System.EventHandler(this.FswTasks_Change);
			// 
			// FrmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.CausesValidation = false;
			this.ClientSize = new System.Drawing.Size(710, 451);
			this.Controls.Add(this.txtBox1);
			this.Controls.Add(this.PbxSearch);
			this.Controls.Add(this.BtnNew);
			this.Controls.Add(this.LblProfiles);
			this.Controls.Add(this.BtnEdit);
			this.Controls.Add(this.BtnApply);
			this.Controls.Add(this.BtnRefresh);
			this.Controls.Add(this.BtnOpen);
			this.Controls.Add(this.BtnTask);
			this.Controls.Add(this.BtnDelete);
			this.Controls.Add(this.LsvProfile);
			this.Controls.Add(this.CbxProfiles);
			this.Controls.Add(this.TxtSearch);
			this.Controls.Add(this.MspMain);
			this.ForeColor = System.Drawing.Color.Black;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.MspMain;
			this.MinimumSize = new System.Drawing.Size(574, 390);
			this.Name = "FrmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "WinFavs";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
			this.Shown += new System.EventHandler(this.FrmMain_Shown);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrmMain_Paint);
			this.MspMain.ResumeLayout(false);
			this.MspMain.PerformLayout();
			this.CmsSplit.ResumeLayout(false);
			this.CmsNotify.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.PbxSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FswProfiles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FswTasks)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ImageList ImgLarge;
		private System.Windows.Forms.MenuStrip MspMain;
		private System.Windows.Forms.ToolStripMenuItem MsiWinFavs;
		private System.Windows.Forms.ToolStripMenuItem MsiShowProfiles;
		private System.Windows.Forms.ToolStripMenuItem MsiExit;
		private System.Windows.Forms.ToolStripMenuItem MsiProfile;
		private System.Windows.Forms.ToolStripMenuItem MsiNew;
		private System.Windows.Forms.ToolStripMenuItem MsiEdit;
		private System.Windows.Forms.ToolStripMenuItem MsiDelete;
		private System.Windows.Forms.ToolStripMenuItem MsiRefresh;
		private System.Windows.Forms.ToolStripMenuItem MsiMore;
		private System.Windows.Forms.ToolStripMenuItem MsiApply;
		public System.Windows.Forms.ComboBox CbxProfiles;
		private System.Windows.Forms.Timer TmrCheckEnable;
		private System.Windows.Forms.Button BtnEdit;
		private System.Windows.Forms.Button BtnApply;
		private System.Windows.Forms.Button BtnRefresh;
		private System.Windows.Forms.Button BtnDelete;
		private System.Windows.Forms.ContextMenuStrip CmsSplit;
		private System.Windows.Forms.ToolStripMenuItem CmiNew;
		private System.Windows.Forms.ToolStripMenuItem CmiFromProfile;
		private System.Windows.Forms.ContextMenuStrip CmsNotify;
		private System.Windows.Forms.ToolStripMenuItem CmiProfiles;
		private System.Windows.Forms.ToolStripMenuItem CmiShow;
		private System.Windows.Forms.ToolStripMenuItem CmiExit;
		private System.Windows.Forms.ToolStripMenuItem MsiAbout;
		private System.Windows.Forms.ToolStripMenuItem CsiFromDefault;
		private System.Windows.Forms.ToolStripMenuItem CsiFromProfile;
		private System.Windows.Forms.ToolStripMenuItem MsiOpen;
		private System.Windows.Forms.Label LblProfiles;
		private System.Windows.Forms.ToolStripMenuItem MsiFeedback;
		private System.Windows.Forms.TextBox TxtSearch;
		private System.Windows.Forms.ToolStripMenuItem MsiTasks;
		private System.Windows.Forms.ToolStripMenuItem MsiOpenTask;
		private System.Windows.Forms.Button BtnTask;
		private System.Windows.Forms.ToolStripMenuItem MsiShowTasks;
		private System.Windows.Forms.ToolStripMenuItem MsiRefreshTasks;
		private System.Windows.Forms.Button BtnOpen;
		private System.Windows.Forms.ToolTip TtpMain;
		private System.Windows.Forms.ToolStripMenuItem CmiRefresh;
		public System.Windows.Forms.NotifyIcon NtiMain;
		private SplitButton BtnNew;
		private System.Windows.Forms.Timer TmrEgg;
		private System.Windows.Forms.ToolStripMenuItem MsiTask;
		private System.Windows.Forms.ImageList ImgSmall;
		private System.Windows.Forms.ColumnHeader HdrName;
		private System.Windows.Forms.ColumnHeader HdrPath;
		private System.Windows.Forms.ToolStripMenuItem MsiDonate;
		private Sep sep1;
		private Sep sep2;
		private Sep sep3;
		private Sep sep4;
		private Sep sep7;
		private Sep sep10;
		public System.Windows.Forms.Timer TmrTasks;
		private System.Windows.Forms.ToolStripMenuItem MsiSettings;
		private Sep sep12;
		private System.Windows.Forms.ToolStripMenuItem experimentalToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openLogReaderToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem MsiCheckForUpdates;
		private Sep sep5;
		public DelayedWatcher FswProfiles;
		public DelayedWatcher FswTasks;
		private System.Windows.Forms.ToolStripMenuItem showLoaderToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ownerDrawTooltipToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem tryALERTToolStripMenuItem;
		public System.Windows.Forms.ListView LsvProfile;
		private System.Windows.Forms.PictureBox PbxSearch;
		private Classes.TxtBox txtBox1;
	}
}

