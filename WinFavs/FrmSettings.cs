﻿using System;
using System.IO;
using System.Windows.Forms;
using Transitions;
using WinFavs.Classes;
using WinFavs.Properties;

namespace WinFavs
{
	public partial class FrmSettings : Form
	{

		public FrmSettings()
		{
			InitializeComponent();

			Win32.SendMessage(BtnAssociate.Handle, Win32.BCM_SETSHIELD, 0, 0xFFFFFFFF);
			Win32.SendMessage(BtnDissociate.Handle, Win32.BCM_SETSHIELD, 0, 0xFFFFFFFF);

			ImgMain.Images.Add(Resources.Settings);
			ImgMain.Images.Add(Resources.View);
			ImgMain.Images.Add(Resources.Profile);
			ImgMain.Images.Add(Resources.New);

			TpgGeneral.ImageIndex = 0;
			TpgView.ImageIndex = 1;
			TpgProfileTasks.ImageIndex = 2;
			TpgMisc.ImageIndex = 3;
		}

		private void BtnAddStart_Click(object sender, EventArgs e)
		{
			Reggy.AddToStart();
			FrmSettings_VisibleChanged(null, null);
		}

		private void BtnRemoveStart_Click(object sender, EventArgs e)
		{
			Reggy.RemoveFromStart();
			FrmSettings_VisibleChanged(null, null);
		}

		private void BtnAssociate_Click(object sender, EventArgs e)
		{
			Reggy.CreateRegistry();
			FrmSettings_VisibleChanged(null, null);
		}

		private void BtnDissociate_Click(object sender, EventArgs e)
		{
			Reggy.Unregister();
			FrmSettings_VisibleChanged(null, null);
		}

		private void BtnUpdate_Click(object sender, EventArgs e)
		{
			FrmMain.Frm.CheckForUpdates(false);
		}

		private void CbxProfileView_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (ChbConservative.Checked && CbxProfileView.SelectedIndex == 4)
			{
				if(FrmMain.Frm.CanShowDialogs) Alert.Show("Conservative mode does not allow tile view mode");
				CbxProfileView.SelectedIndex = 0;
				return;
			}

			FrmMain.Frm.SelectView(CbxProfileView.SelectedIndex);

			switch (CbxProfileView.SelectedIndex)
			{
				case 0:
					PbxView.Image = Resources.Large;
					break;
				case 1:
					PbxView.Image = Resources.Details;
					break;
				case 2:
					PbxView.Image = Resources.Small;
					break;
				case 3:
					PbxView.Image = Resources.List;
					break;
				case 4:
					PbxView.Image = Resources.Tile;
					break;
			}
		}

		private void ChbConservative_CheckedChanged(object sender, EventArgs e)
		{
			if (ChbConservative.Checked && CbxProfileView.SelectedIndex == 4) CbxProfileView.SelectedIndex = 0;
			FrmMain.Frm.LsvProfile.BeginUpdate();
			FrmMain.Frm.LsvProfile.Items.Clear();
			FrmMain.Frm.LsvProfile.EndUpdate();
			FrmMain.Frm.LsvProfile.VirtualMode = ChbConservative.Checked;
			FrmMain.Frm.ScanForProfiles(false);
		}

		private void BtnReset_Click(object sender, EventArgs e)
		{
			Updater.ParseVersion("0.0.0");
		}

		private void ChbRefresh_CheckedChanged(object sender, EventArgs e)
		{
			if (!Directory.Exists(FrmMain.ProfilesPath)) Directory.CreateDirectory(FrmMain.ProfilesPath);

			FrmMain.Frm.FswProfiles.EnableRaisingEvents = FrmMain.Frm.FswTasks.EnableRaisingEvents = ChbRefresh.Checked;
		}

		private void BtnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FrmSettings_VisibleChanged(object sender, EventArgs e)
		{
			if (!Visible) return;

			BtnAddStart.Enabled = BtnRemoveStart.Enabled = true;
			BtnAssociate.Enabled = BtnDissociate.Enabled = true;

			switch (Reggy.CheckStart())
			{
				case RegState.Available:
					LblStateStart.Text = "WinFavs will start with Windows";
					BtnAddStart.Enabled = false;
					break;
				case RegState.NonAvailable:
					LblStateStart.Text = "WinFavs will not start with Windows";
					BtnRemoveStart.Enabled = false;
					break;
				case RegState.Unknown:
					LblStateStart.Text = "WinFavs could not determine if the it will start with Windows";
					break;
			}

			switch (Reggy.CheckAssociation())
			{
				case RegState.Available:
					LblStatShell.Text = "WinFavs is associated with WFV files";
					BtnAddStart.Enabled = false;
					break;
				case RegState.Incomplete:
					LblStatShell.Text = "WinFavs file association may be incomplete";
					break;
				case RegState.NonAvailable:
					LblStatShell.Text = "WinFavs is not associated with WFV files";
					BtnRemoveStart.Enabled = false;
					break;
				case RegState.Unknown:
					LblStatShell.Text = "WinFavs could not determine the file association state";
					break;
			}
		}

		private void TbcMain_SelectedIndexChanged(object sender, EventArgs e)
		{
			TabPage t = TbcMain.TabPages[TbcMain.SelectedIndex];
			int lowest = -1;
			int gap = Height - TbcMain.Bottom;

			foreach (Control x in t.Controls)
				if (x.Bottom > lowest) lowest = x.Bottom;

			Transition transition = new Transition(new TransitionType_CriticalDamping(500));
			transition.add(this, "Height", lowest + 50 + gap);
			transition.add(this, "Top", Top - ((lowest + 50 + gap) - Height)  / 2);
			transition.run();
		}

	}
}
