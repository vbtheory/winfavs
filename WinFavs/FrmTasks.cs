﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WinFavs.Classes;

namespace WinFavs
{
	public partial class FrmTasks : Form
	{
		private List<Task> _loadedTasks = new List<Task>();

		public FrmTasks()
		{
			InitializeComponent();

			Tasks.ScanForTasks(ref _loadedTasks, false, false, LsvMain);

			FswMain.Path = FrmMain.TasksPath;
			FswMain.EnableRaisingEvents = FrmMain.C.ChbRefresh.Checked;
			
		}

		private void TmrCheckEnable_Tick(object sender, EventArgs e)
		{
			BtnEdit.Enabled = BtnRemove.Enabled = LsvMain.SelectedIndices.Count != 0;
			BtnClear.Enabled = LsvMain.Items.Count > 0;
		}

		private void BtnNew_Click(object sender, EventArgs e)
		{
			Logger.Log("Creating new task...");
			if (new FrmTask(new Task
			{
				Enabled = true,
				Hour = DateTime.Now.Hour,
				Minute = DateTime.Now.Minute,
				Mon = true,
				Tue = true,
				Wed = true,
				Thu = true,
				Fri = true,
				Sat = true,
				Sun = true
			}).ShowDialog() != DialogResult.OK) return;

			Tasks.ScanForTasks(ref _loadedTasks, false, true, LsvMain);
		}

		private void BtnEdit_Click(object sender, EventArgs e)
		{
			Logger.Log("Editing task...");
			List<int> orderedIndices = LsvMain.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			for (int i = 0; i < orderedIndices.Count; i++)
			{
				if (new FrmTask(_loadedTasks[orderedIndices[i]]).ShowDialog() == DialogResult.OK) continue;
				if (orderedIndices.Count <= 1 || i == orderedIndices.Count - 1) continue;

				Alert a = new Alert("Do you want to <b>keep editing shortcuts</b>?", "", MessageBoxButtons.YesNo);

				a.Btn2Text = "&Keep editing";
				a.Btn3Text = "&Cancel";

				if(a.Show() != DialogResult.Yes) break;
			}

			Tasks.ScanForTasks(ref _loadedTasks, false, true, LsvMain);
		}

		private void BtnRemove_Click(object sender, EventArgs e)
		{
			Alert a = new Alert(string.Format("Would you like to delete <b>{0}</b>?", LsvMain.SelectedIndices.Count.IsPlural("task")), "Delete confirmation", MessageBoxButtons.YesNo,
					MessageBoxIcon.Exclamation);
			a.Btn2Text = "&Delete";
			a.Btn3Text = "&Cancel";
			a.Btn2Highlight = Highlight.Red;

			if (a.Show() != DialogResult.Yes) return;

			try
			{
				List<int> orderedIndices = LsvMain.SelectedIndices.Cast<int>().ToList();
				orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

				foreach (int i in orderedIndices)
				{
					File.Delete(_loadedTasks[i].Path);
				}
			}
			catch (Exception ex)
			{
				Notifier.Show("Failed to delete task: " + ex.Message, Properties.Resources.Warning);
			}

			Tasks.ScanForTasks(ref _loadedTasks, true, false, LsvMain);
		}

		private void BtnClear_Click(object sender, EventArgs e)
		{
			Alert a = new Alert("Are you sure you would like to delete <b>all</b> tasks?", "Clearing confirmation",
					MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
			a.Btn2Text = "&Delete";
			a.Btn3Text = "&Cancel";
			a.Btn2Highlight = Highlight.Red;

			if (a.Show() != DialogResult.Yes) return;
			try
			{
				foreach (string file in Directory.GetFiles(FrmMain.TasksPath))
				{
					File.Delete(file);
				}
			}
			catch (Exception ex)
			{
				Notifier.Show("Failed to clear tasks: " + ex.Message, Properties.Resources.Warning);
			}

			Tasks.ScanForTasks(ref _loadedTasks, false, true, LsvMain);
		}

		private void BtnRefresh_Click(object sender, EventArgs e)
		{
			Tasks.ScanForTasks(ref _loadedTasks, false, true, LsvMain);
		}

		private void BtnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FrmTasks_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					BtnRemove_Click(null, null);
					break;
				case Keys.F5:
					BtnRefresh_Click(null, null);
					break;
			}
		}

		private void FrmTasks_FormClosed(object sender, FormClosedEventArgs e)
		{
			Dispose(true);
		}

		private void FswMain_ChangeDetected(object sender, EventArgs e)
		{
			Tasks.ScanForTasks(ref _loadedTasks, false, false, LsvMain);
		}
	}
}