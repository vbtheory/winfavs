﻿using WinFavs.Controls;

namespace WinFavs
{
	partial class FrmShortcut
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.LblShortcutName = new System.Windows.Forms.Label();
			this.LblShortcutPath = new System.Windows.Forms.Label();
			this.PnlDrop = new System.Windows.Forms.Panel();
			this.PnlBack = new System.Windows.Forms.Panel();
			this.ChbAuto = new System.Windows.Forms.CheckBox();
			this.TtpMain = new System.Windows.Forms.ToolTip(this.components);
			this.BtnOk = new System.Windows.Forms.Button();
			this.BtnCancel = new System.Windows.Forms.Button();
			this.BtnUp = new System.Windows.Forms.Button();
			this.BtnBrowse = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.BtnClearIcon = new System.Windows.Forms.Button();
			this.BtnBrowseIcon = new System.Windows.Forms.Button();
			this.TxtPath = new WinFavs.Controls.RuleBox();
			this.TxtName = new WinFavs.Controls.RuleBox();
			this.PnlBack.SuspendLayout();
			this.SuspendLayout();
			// 
			// LblShortcutName
			// 
			this.LblShortcutName.AutoSize = true;
			this.LblShortcutName.Location = new System.Drawing.Point(12, 14);
			this.LblShortcutName.Name = "LblShortcutName";
			this.LblShortcutName.Size = new System.Drawing.Size(79, 13);
			this.LblShortcutName.TabIndex = 0;
			this.LblShortcutName.Text = "Shortcut name:";
			// 
			// LblShortcutPath
			// 
			this.LblShortcutPath.AutoSize = true;
			this.LblShortcutPath.Location = new System.Drawing.Point(11, 43);
			this.LblShortcutPath.Name = "LblShortcutPath";
			this.LblShortcutPath.Size = new System.Drawing.Size(74, 13);
			this.LblShortcutPath.TabIndex = 0;
			this.LblShortcutPath.Text = "Shortcut path:";
			// 
			// PnlDrop
			// 
			this.PnlDrop.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PnlDrop.BackColor = System.Drawing.SystemColors.Window;
			this.PnlDrop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.PnlDrop.Location = new System.Drawing.Point(1, 1);
			this.PnlDrop.Name = "PnlDrop";
			this.PnlDrop.Size = new System.Drawing.Size(32, 32);
			this.PnlDrop.TabIndex = 4;
			this.TtpMain.SetToolTip(this.PnlDrop, "Drop folder here to create a shortcut to it");
			// 
			// PnlBack
			// 
			this.PnlBack.BackColor = System.Drawing.Color.Black;
			this.PnlBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.PnlBack.Controls.Add(this.PnlDrop);
			this.PnlBack.Location = new System.Drawing.Point(105, 71);
			this.PnlBack.Name = "PnlBack";
			this.PnlBack.Size = new System.Drawing.Size(34, 34);
			this.PnlBack.TabIndex = 5;
			// 
			// ChbAuto
			// 
			this.ChbAuto.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbAuto.Location = new System.Drawing.Point(408, 14);
			this.ChbAuto.Name = "ChbAuto";
			this.ChbAuto.Size = new System.Drawing.Size(69, 18);
			this.ChbAuto.TabIndex = 1;
			this.ChbAuto.Text = "Autoname";
			this.TtpMain.SetToolTip(this.ChbAuto, "If this is checked, the app will automatically assign a name for the shortcut");
			this.ChbAuto.UseVisualStyleBackColor = true;
			this.ChbAuto.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// TtpMain
			// 
			this.TtpMain.AutoPopDelay = 5000;
			this.TtpMain.InitialDelay = 250;
			this.TtpMain.ReshowDelay = 100;
			// 
			// BtnOk
			// 
			this.BtnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.BtnOk.Enabled = false;
			this.BtnOk.Location = new System.Drawing.Point(280, 119);
			this.BtnOk.Name = "BtnOk";
			this.BtnOk.Size = new System.Drawing.Size(116, 23);
			this.BtnOk.TabIndex = 7;
			this.BtnOk.Text = "Save shortcut";
			this.TtpMain.SetToolTip(this.BtnOk, "Save the shortcut");
			this.BtnOk.UseVisualStyleBackColor = true;
			this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
			// 
			// BtnCancel
			// 
			this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnCancel.Location = new System.Drawing.Point(402, 119);
			this.BtnCancel.Name = "BtnCancel";
			this.BtnCancel.Size = new System.Drawing.Size(75, 23);
			this.BtnCancel.TabIndex = 8;
			this.BtnCancel.Text = "Cancel";
			this.TtpMain.SetToolTip(this.BtnCancel, "Exit without saving changes");
			this.BtnCancel.UseVisualStyleBackColor = true;
			// 
			// BtnUp
			// 
			this.BtnUp.BackgroundImage = global::WinFavs.Properties.Resources.parent;
			this.BtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.BtnUp.Enabled = false;
			this.BtnUp.FlatAppearance.BorderSize = 0;
			this.BtnUp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
			this.BtnUp.Location = new System.Drawing.Point(432, 42);
			this.BtnUp.Name = "BtnUp";
			this.BtnUp.Size = new System.Drawing.Size(22, 18);
			this.BtnUp.TabIndex = 3;
			this.TtpMain.SetToolTip(this.BtnUp, "Parent folder");
			this.BtnUp.UseVisualStyleBackColor = true;
			this.BtnUp.Click += new System.EventHandler(this.BtnUp_Click);
			// 
			// BtnBrowse
			// 
			this.BtnBrowse.FlatAppearance.BorderSize = 0;
			this.BtnBrowse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
			this.BtnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtnBrowse.Location = new System.Drawing.Point(454, 42);
			this.BtnBrowse.Name = "BtnBrowse";
			this.BtnBrowse.Size = new System.Drawing.Size(22, 18);
			this.BtnBrowse.TabIndex = 4;
			this.BtnBrowse.Text = "...";
			this.TtpMain.SetToolTip(this.BtnBrowse, "Browse");
			this.BtnBrowse.UseVisualStyleBackColor = true;
			this.BtnBrowse.Click += new System.EventHandler(this.BtnBrowse_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(11, 82);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(73, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Shortcut icon:";
			// 
			// BtnClearIcon
			// 
			this.BtnClearIcon.BackgroundImage = global::WinFavs.Properties.Resources.Delete;
			this.BtnClearIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.BtnClearIcon.FlatAppearance.BorderSize = 0;
			this.BtnClearIcon.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
			this.BtnClearIcon.Location = new System.Drawing.Point(274, 77);
			this.BtnClearIcon.Name = "BtnClearIcon";
			this.BtnClearIcon.Size = new System.Drawing.Size(24, 23);
			this.BtnClearIcon.TabIndex = 6;
			this.BtnClearIcon.UseVisualStyleBackColor = true;
			this.BtnClearIcon.Visible = false;
			this.BtnClearIcon.Click += new System.EventHandler(this.BtnClearIcon_Click);
			// 
			// BtnBrowseIcon
			// 
			this.BtnBrowseIcon.FlatAppearance.BorderSize = 0;
			this.BtnBrowseIcon.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
			this.BtnBrowseIcon.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnBrowseIcon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
			this.BtnBrowseIcon.Location = new System.Drawing.Point(145, 77);
			this.BtnBrowseIcon.Name = "BtnBrowseIcon";
			this.BtnBrowseIcon.Size = new System.Drawing.Size(123, 23);
			this.BtnBrowseIcon.TabIndex = 5;
			this.BtnBrowseIcon.Text = "Choose custom icon...";
			this.BtnBrowseIcon.UseVisualStyleBackColor = true;
			this.BtnBrowseIcon.Click += new System.EventHandler(this.BtnBrowsIcon_Click);
			// 
			// TxtPath
			// 
			this.TxtPath.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.TxtPath.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories;
			this.TxtPath.BackColor = System.Drawing.SystemColors.Window;
			this.TxtPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.TxtPath.CueText = "Enter shortcut path (Ex: C:\\)";
			this.TxtPath.FixedLength = 0;
			this.TxtPath.ForeColor = System.Drawing.Color.Black;
			this.TxtPath.Location = new System.Drawing.Point(105, 41);
			this.TxtPath.Name = "TxtPath";
			this.TxtPath.Size = new System.Drawing.Size(372, 20);
			this.TxtPath.TabIndex = 2;
			this.TxtPath.ValidationMethod = WinFavs.Controls.RuleBox.Validation.Path;
			this.TxtPath.TextChangedDelayed += new System.EventHandler(this.TxtPath_TextChangedDelayed);
			this.TxtPath.TextChanged += new System.EventHandler(this.TxtPath_TextChanged);
			// 
			// TxtName
			// 
			this.TxtName.BackColor = System.Drawing.SystemColors.Window;
			this.TxtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.TxtName.CueText = "Enter shortcut name (Ex: C drive)";
			this.TxtName.FixedLength = 0;
			this.TxtName.ForeColor = System.Drawing.Color.Black;
			this.TxtName.Location = new System.Drawing.Point(105, 12);
			this.TxtName.Name = "TxtName";
			this.TxtName.Size = new System.Drawing.Size(297, 20);
			this.TxtName.TabIndex = 0;
			this.TxtName.Text = "Shortcut";
			this.TxtName.ValidationMethod = WinFavs.Controls.RuleBox.Validation.FileName;
			this.TxtName.TextChangedDelayed += new System.EventHandler(this.TxtPath_TextChangedDelayed);
			this.TxtName.TextChanged += new System.EventHandler(this.TxtPath_TextChanged);
			// 
			// FrmShortcut
			// 
			this.AcceptButton = this.BtnOk;
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.CancelButton = this.BtnCancel;
			this.ClientSize = new System.Drawing.Size(485, 148);
			this.Controls.Add(this.BtnClearIcon);
			this.Controls.Add(this.BtnUp);
			this.Controls.Add(this.ChbAuto);
			this.Controls.Add(this.BtnBrowseIcon);
			this.Controls.Add(this.BtnBrowse);
			this.Controls.Add(this.BtnOk);
			this.Controls.Add(this.BtnCancel);
			this.Controls.Add(this.TxtPath);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.LblShortcutPath);
			this.Controls.Add(this.TxtName);
			this.Controls.Add(this.LblShortcutName);
			this.Controls.Add(this.PnlBack);
			this.ForeColor = System.Drawing.Color.Black;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FrmShortcut";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Edit shortcut";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEditShort_FormClosing);
			this.Shown += new System.EventHandler(this.FrmShortcut_Shown);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FrmShortcut_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FrmShortcut_DragEnter);
			this.PnlBack.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LblShortcutName;
		private System.Windows.Forms.Label LblShortcutPath;
		private System.Windows.Forms.Button BtnCancel;
		private System.Windows.Forms.Button BtnOk;
		public RuleBox TxtName;
		public RuleBox TxtPath;
		private System.Windows.Forms.Panel PnlDrop;
		private System.Windows.Forms.Button BtnBrowse;
		private System.Windows.Forms.Panel PnlBack;
		private System.Windows.Forms.CheckBox ChbAuto;
		private System.Windows.Forms.Button BtnUp;
		private System.Windows.Forms.ToolTip TtpMain;
		private System.Windows.Forms.Button BtnBrowseIcon;
		private System.Windows.Forms.Button BtnClearIcon;
		private System.Windows.Forms.Label label1;


	}
}