﻿using IWshRuntimeLibrary;
using Microsoft.VisualBasic.Devices;
using Shell32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using WinFavs.Classes;
using WinFavs.Properties;
using Attribute = WinFavs.Classes.Attribute;
using File = System.IO.File;
using Shortcut = WinFavs.Classes.Shortcut;

namespace WinFavs
{
	public partial class FrmMain : Form
	{

		#region Declarations

		#region Constants

		public static readonly string ProfilesPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
													 @"\WinFavs\Profiles";

		public static readonly string TasksPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
												  @"\WinFavs\Tasks";

		public static readonly string ConfigPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
												   @"\WinFavs\Config.ini";

		private static readonly string CrashPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
												   @"\WinFavs\.wfv";

		private readonly string LinksPath = Environment.GetEnvironmentVariable("USERPROFILE") + @"\Links";

		#endregion

		public static FrmMain Frm;
		public static FrmSettings C;

		public List<Profile> _loadedProfiles = new List<Profile>();
		public static List<Shortcut> Clipboard;
		private List<Task> _loadedTasks = new List<Task>();
		public static Dictionary<string, Image> Icons = new Dictionary<string, Image>();

		private string _donateLink = "";

		private static int _feedbacks;

		private bool _showMessage = true;
		private bool _canMigrate = true;
		private bool _showWelcomeHint;
		public bool CanShowDialogs;
		public bool ProfileHint { get; set; }
		public bool ShortcutHint { get; set; }
		public bool TaskHint { get; set; }
		public string LastViewed { get; set; }

		public string Ignored
		{
			get { return Updater.Ignored.ToString(); }
			set { Updater.Ignored = new Vers(value); }
		}

		#endregion

		#region GUI

		public FrmMain(IList<string> args)
		{
			Logger.OpenLog();

			if (Environment.OSVersion.Version.Major < 6)
			{
				Alert.Show(
					"WinFavs doesn't support versions of Windows <b>earlier than Vista</b>! Please upgrade your version of Windows.\n" +
					"Plus who still uses " + new ComputerInfo().OSFullName.Replace("Microsoft ", "") +
					" since it's no longer supported. <i>Geez!!!</i>");
				Logger.Log("Version " + Environment.OSVersion.Version + " is not supported", LogType.Warning);
				Process.GetCurrentProcess().Kill();
				return;
			}

			Frm = this;
			C = new FrmSettings();

			if (!Debugger.IsAttached && !File.Exists(CrashPath) && File.Exists(ConfigPath))
			{
				Logger.Log("The app didn't close well", LogType.Warning);
				Alert a = new Alert("The app seems to have crashed in its last session. Would you like to help us investigate the problem?",
					"Crash detected", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Reset settings");

				a.Btn2Text = "&Send feedback";
				a.Btn3Text = "&Continue";
				if (a.Show() == DialogResult.Yes) new FrmFeedback().ShowDialog();
				if (a.CheckBoxState)
					try
					{
						if (File.Exists(ConfigPath)) File.Delete(ConfigPath);
					}
					catch (Exception ex)
					{
						Logger.Log("Failed to reset settings: " + ex.Message, LogType.Error);
						Notifier.Show("Failed to reset settings: " + ex.Message, Resources.Warning);
					}
			}

			int index = Logger.Log("Starting app...", Mode.Start, 0);
			CheckForIllegalCrossThreadCalls = false;

			Hide();
			InitializeComponent();
			Lighter.SetHighlight(BtnApply, Highlight.Green);
			InitReggy(args.Any() && args[0] == "-norestart");
			if (Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\WinFavs\\Presets")) MigratePresets();
			Configure();

			Directory.CreateDirectory(ProfilesPath);
			Directory.CreateDirectory(TasksPath);

			experimentalToolStripMenuItem.Visible = Debugger.IsAttached;
			MspMain.Renderer = CmsNotify.Renderer = CmsSplit.Renderer = new WinRenderer();
			FswProfiles.Path = ProfilesPath;
			FswTasks.Path = TasksPath;

			OpenFiles(args);
			ExtendGlass();
			ScanForProfiles(false);
			Tasks.ScanForTasks(ref _loadedTasks, true, false);

			Application.Idle += (sender, eventArgs) => GC.Collect();
			NetworkChange.NetworkAvailabilityChanged +=
				(sender, eventArgs) => { if (eventArgs.IsAvailable && C.ChbAutoUpdate.Checked) CheckForUpdates(true); };

			if (args.Any() && args[0] == "-s" && C.ChbStartMinimized.Checked) WindowState = FormWindowState.Minimized;
			else Show();

			Logger.Log("Finished loading ({0})", Mode.Stop, index);
		}

		private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_loadedTasks.Count > 0 && C.ChbClosingConfirmation.Checked)
			{
				Alert a = new Alert("If you close the app, <b>tasks won't get triggered</b>! Are you sure you would like to close the app?",
					"Closing confirmation",
					MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, "Never ask again");
				a.Btn2Text = "&Close";
				a.Btn3Text = "C&ancel";
				if (a.Show() != DialogResult.Yes)
				{
					e.Cancel = true;
					if (a.CheckBoxState) C.ChbClosingConfirmation.Checked = false;
					return;
				}
				if (a.CheckBoxState) C.ChbClosingConfirmation.Checked = false;
			}

			if (C.ChbFeedback.Checked)
			{
				Opacity = 0;
				ShowInTaskbar = false;
				new FrmFeedback().ShowDialog();
			}

			SaveConfig(e.CloseReason);

			Logger.WaitForLog();

			Logger.CloseLog();

			Logger.WaitForLog();

			File.Create(CrashPath);
		}

		private void FrmMain_Shown(object sender, EventArgs e)
		{
			BringToFront();
			Activate();
			if (C.ChbHelpImprove.Checked) SendLog();
			if (_showWelcomeHint)
			{
				Alert a = new Alert(
					"Welcome to <b>WinFavs</b>! This app is user-friendly and reliable and it will come in handy if you want to customize the Favorite folders.\n" +
					"You can create a <h1>Profile</h1> which contains shortcuts to your folders.\n" +
					"You can then apply that profile whenever you need the shortcuts inside it and the app will remove old shortcuts and create new ones.\n" +
					"Moreover, you can click the \"Schedule\" button to setup automatic application of a profile at a certain time.\n" +
					"As a first time user, we recommend you tailor the app to your likings by using the settings menu located at the top of the window.\n" +
					"Lastly, if you have any thoughts about the app or ideas on how to improve, please use the \"Send feedback\" menu available in the more section.",
					"Welcome to WinFavs", "Open settings after this dialog", true);

				a.Show();
				if (a.CheckBoxState) settingsToolStripMenuItem_Click(null, null);
			}

			try
			{
				File.Delete(CrashPath);
			}
			catch
			{
			}

			int previousIndex = _loadedProfiles.FindIndex(x => x.Name == LastViewed);
			if (CbxProfiles.Items.Count > 0 && previousIndex >= 0) CbxProfiles.SelectedIndex = previousIndex;

			CanShowDialogs = true;
		}
		
		private void FrmMain_Paint(object sender, PaintEventArgs e)
		{
			e.Graphics.FillRectangle(Brushes.Black, 0, 0, Width, 38);
		}

		private void TmrEgg_Tick(object sender, EventArgs e)
		{
			ToolStripItemDisplayStyle d = ToolStripItemDisplayStyle.ImageAndText;

			switch (MsiWinFavs.DisplayStyle)
			{
				case ToolStripItemDisplayStyle.Image:
					d = ToolStripItemDisplayStyle.Text;
					break;
				case ToolStripItemDisplayStyle.Text:
					d = ToolStripItemDisplayStyle.ImageAndText;
					break;
				case ToolStripItemDisplayStyle.ImageAndText:
					d = ToolStripItemDisplayStyle.Image;
					break;
			}

			MsiWinFavs.DisplayStyle =
				MsiProfile.DisplayStyle = MsiTasks.DisplayStyle = MsiMore.DisplayStyle = d;

			if (d == ToolStripItemDisplayStyle.ImageAndText)
				TmrEgg.Stop();
		}

		private void CbxProfiles_SelectedIndexChanged(object sender, EventArgs e)
		{
			Cursor = Cursors.AppStarting;

			Stopwatch s = new Stopwatch();
			s.Start();

			LsvProfile.BeginUpdate();
			LsvProfile.Items.Clear();
			ImgLarge.Images.Clear();
			ImgSmall.Images.Clear();
			Icons.Clear();

			Profile p = _loadedProfiles[CbxProfiles.SelectedIndex];

			if (!C.ChbConservative.Checked) LoadProfile(p);
			else LoadProfileConservative(p);

			HdrName.Width = -1;
			HdrPath.Width = -2;

			LsvProfile.EndUpdate();
			s.Stop();

			Logger.Log(string.Format("Loaded {0} in {1}ms (Conservative=1)", p.Entries.Count.IsPlural("shortcut"),
				s.ElapsedMilliseconds));

			Cursor = Cursors.Default;

			if (s.ElapsedMilliseconds > 2500 && CanShowDialogs)
				Notifier.Show(
					"\"" + _loadedProfiles[CbxProfiles.SelectedIndex].Name + "\" took about " + s.ElapsedMilliseconds.FormatTime() +
					" to load", "Click here to enable conservative mode",
					() => { C.ChbConservative.Checked = true; });
		}

		private void TmrCheckEnable_Tick(object sender, EventArgs e)
		{
			PbxSearch.Enabled = CbxProfiles.Enabled = TxtSearch.Enabled = BtnTask.Enabled =
				MsiTask.Enabled = CsiFromProfile.Enabled =
					BtnDelete.Enabled = BtnApply.Enabled = BtnEdit.Enabled =
						MsiDelete.Enabled = MsiApply.Enabled = MsiEdit.Enabled = CbxProfiles.SelectedIndex >= 0;
		}

		private void FswProfiles_Change(object sender, EventArgs e)
		{
			ScanForProfiles(false);
		}

		private void BtnSearch_Click(object sender, EventArgs e)
		{
			TxtSearch_KeyDown(null, new KeyEventArgs(Keys.Enter));
		}

		private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					if (TxtSearch.Text.ToLower() == "vbt")
						TmrEgg.Enabled = true;

					int foundIndex =
						_loadedProfiles.FindIndex(x => String.Equals(x.Name, TxtSearch.Text, StringComparison.CurrentCultureIgnoreCase));

					if (foundIndex == -1) return;

					TxtSearch.Text = "";
					CbxProfiles.SelectedIndex = foundIndex;
					break;
				case Keys.Escape:
					TxtSearch.Text = "";
					break;
			}
		}

		private void LsvList_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if (e.IsSelected) LsvProfile.SelectedIndices.Clear();
		}

		private void tmrTasks_Tick(object sender, EventArgs e)
		{
			if (!C.ChbTasks.Checked) return;

			TmrTasks.Interval = (60 - DateTime.Now.Second) * 1000;

			DateTime start = DateTime.Now;

			foreach (Task s in _loadedTasks)
			{
				bool isToday = (s.Mon && start.DayOfWeek == DayOfWeek.Monday) ||
							   (s.Tue && start.DayOfWeek == DayOfWeek.Tuesday) ||
							   (s.Wed && start.DayOfWeek == DayOfWeek.Wednesday) ||
							   (s.Thu && start.DayOfWeek == DayOfWeek.Thursday) ||
							   (s.Fri && start.DayOfWeek == DayOfWeek.Friday) ||
							   (s.Sat && start.DayOfWeek == DayOfWeek.Saturday) ||
							   (s.Sun && start.DayOfWeek == DayOfWeek.Sunday);

				if (!s.Enabled || start.Minute != s.Minute || start.Hour != s.Hour || !isToday) continue;

				Profile p = ReadProfile(ProfilesPath + "\\" + s.Profile + ".wfp");
				if (p.HasFailedLoading) continue;

				if (C.ChbTaskConfirm.Checked)
				{
					Alert a = new Alert("Would you like to apply \"" + p.Name + "\" as scheduled?", "", MessageBoxButtons.YesNo,
						"Never ask again");
					a.Btn2Text = "&Apply";
					a.Btn3Text = "&Cancel";
					if (a.Show() != DialogResult.Yes)
					{
						if (a.CheckBoxState) C.ChbTaskConfirm.Checked = false;
						continue;
					}
					if (a.CheckBoxState) C.ChbTaskConfirm.Checked = false;
				}
				ApplyProfile(p, true);
			}
		}

		private void FswTasks_Change(object sender, EventArgs e)
		{
			Tasks.ScanForTasks(ref _loadedTasks, true, false);
		}

		private void LsvList_KeyDown(object sender, KeyEventArgs e)
		{
			e.Handled = true;
		}

		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		protected override void WndProc(ref Message m)
		{
			switch (m.Msg)
			{
				case 0x0112:
					int command = m.WParam.ToInt32() & 0xfff0;
					if (command == 0xF020 && C.ChbMinimizeToTray.Checked)
					{
						CmiShow.Visible = true;
						Visible = false;
						ShowInTaskbar = false;

						if (!_showMessage) return;

						_showMessage = false;
						Notifier.Show(NtiMain.BalloonTipText, NtiMain.BalloonTipTitle, "Click to restore WinFavs",
							() => showToolStripMenuItem_Click(null, null));
					}
					break;
				case 0x84:
					base.WndProc(ref m);
					if (m.Result.ToInt32() == 1 // ...and it is on the client
					    && IsOnGlass(m.LParam.ToInt32())) // ...and specifically in the glass area
							m.Result = new IntPtr(2); // lie and say they clicked on the title bar
					return;
			}

			base.WndProc(ref m);
		}
		
		private bool IsOnGlass(int lParam)
		{
			// get screen coordinates
			int x = (lParam << 16) >> 16; // lo order word
			int y = lParam >> 16; // hi order word

			// translate screen coordinates to client area
			Point p = PointToClient(new Point(x, y));

			// work out if point clicked is on glass
			return p.Y <38;
		}
		private void LsvProfile_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
		{
			Shortcut shortcut = _loadedProfiles[CbxProfiles.SelectedIndex].Entries[e.ItemIndex];

			int index = 0;
			string temp = shortcut.Path.ToLower();

			if (temp == Paths.GetDefaultPath(KnownFolder.Desktop))
				index = 1;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Downloads))
				index = 2;
			else if ((temp.Length == 2 && temp.EndsWith(":")) || (temp.Length == 3 && temp.EndsWith(":\\")))
				index = 3;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Pictures))
				index = 4;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Music))
				index = 5;
			else if (temp == Environment.GetEnvironmentVariable("USERPROFILE").ToLower())
				index = 6;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Videos))
				index = 7;
			else if (temp == Paths.GetDefaultPath(KnownFolder.Documents))
				index = 8;
			else if (!Directory.Exists(temp))
				index = 9;

			ListViewItem f = new ListViewItem(shortcut.Name, index);
			f.SubItems.Add(shortcut.Path);
			if (!C.ChbConservative.Checked) f.ToolTipText = shortcut.Name + " (" + shortcut.Path + ")";

			e.Item = f;
		}

		#endregion

		#region Profile methods

		/// <summary>
		///     Gets all files in the specified collection and adds them and their icons to the listview
		/// </summary>
		/// <param name="p">The profile to be loaded</param>
		private void LoadProfile(Profile p)
		{
			List<Shortcut> entries = p.Entries.ToList();
			List<ListViewItem> items = new List<ListViewItem>();

			foreach (Shortcut shortcut in entries)
			{
				int index;

				if (Icons.ContainsKey(shortcut.Path) && shortcut.Icon != "")
					index = Icons.Keys.ToList().IndexOf(shortcut.Path);
				else
				{
					ImgLarge.Images.Add(IconManager.GetIcon(shortcut.Path, shortcut.Icon, shortcut.IconIndex, IconSize.ExtraLarge));
					ImgSmall.Images.Add(IconManager.GetIcon(shortcut.Path, shortcut.Icon, shortcut.IconIndex, IconSize.Large));

					index = ImgLarge.Images.Count - 1;
					if (!Icons.ContainsKey(shortcut.Path)) Icons.Add(shortcut.Path, ImgLarge.Images[index]);
				}

				ListViewItem f = new ListViewItem(shortcut.Name, index, LsvProfile.Groups[Directory.Exists(shortcut.Path) ? 0 : 1]);
				f.SubItems.Add(shortcut.Path);
				if (!C.ChbConservative.Checked) f.ToolTipText = shortcut.Name + " (" + shortcut.Path + ")";

				items.Add(f);
			}

			LsvProfile.Items.AddRange(items.ToArray());
		}

		/// <summary>
		///     Gets all files in the specified collection and adds them and their icons to the listview
		/// </summary>
		/// <param name="p">The profile to be loaded</param>
		private void LoadProfileConservative(Profile p)
		{
			List<Bitmap> images = new List<Bitmap>();

			images.Add(IconManager.GetFolderIcon(IconSize.Large, FolderType.Open));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Desktop), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Downloads), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Environment.GetEnvironmentVariable("SystemDrive"), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Pictures), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Music), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Environment.GetEnvironmentVariable("USERPROFILE"), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Videos), "", 0, IconSize.Large));
			images.Add(IconManager.GetIcon(Paths.GetDefaultPath(KnownFolder.Documents), "", 0, IconSize.Large));
			images.Add(Resources.Warning);

			foreach (Bitmap image in images)
			{
				ImgLarge.Images.Add(image);
				ImgSmall.Images.Add(image);
			}

			LsvProfile.VirtualListSize = p.Entries.Count;
		}

		/// <summary>
		///     Scans the profiles folder for all the available profiles
		/// </summary>
		public void ScanForProfiles(bool cancelIfAutoRefreshEnabled)
		{
			if (cancelIfAutoRefreshEnabled && C.ChbRefresh.Checked) return;

			int index = Logger.Log("Scanning for profiles...", Mode.Start, 19);

			string previousName = "";

			if (_loadedProfiles.Count > 0) previousName = _loadedProfiles[CbxProfiles.SelectedIndex].Name;

			_loadedProfiles.Clear();
			CbxProfiles.Items.Clear();
			CmiProfiles.DropDownItems.Clear();
			TxtSearch.AutoCompleteCustomSource.Clear();

			if (!Directory.Exists(ProfilesPath))
			{
				Directory.CreateDirectory(ProfilesPath);
				LsvProfile.EndUpdate();
				return;
			}

			int i = 0;
			foreach (string file in Directory.GetFiles(ProfilesPath))
			{
				if (!file.EndsWith(".wfp")) continue;

				Profile p = ReadProfile(file);

				if (p.HasFailedLoading) continue;

				CbxProfiles.Items.Add(string.Format("{0} ({1})", p.Name,
					(p.Entries.Count != 0 ? p.Entries.Count.IsPlural("shortcut") : "empty")));
				TxtSearch.AutoCompleteCustomSource.Add(p.Name);

				_loadedProfiles.Add(p);

				ToolStripMenuItem tmi = new ToolStripMenuItem(p.Name) { Tag = i, Text = p.Name };
				CmiProfiles.DropDownItems.Add(tmi);
				i++;
				tmi.Click += tmi_Click;
			}

			if (CbxProfiles.Items.Count > 0)
			{
				CbxProfiles.SelectedIndex = previousName == "" ? 0 : _loadedProfiles.FindIndex(x => x.Name == previousName);
				if (CbxProfiles.SelectedIndex == -1) CbxProfiles.SelectedIndex = 0;
			}
			else CmiProfiles.Visible = false;

			Logger.Log(
				string.Format("Found {1} ({0}, {2})", "{0}", CbxProfiles.Items.Count.IsPlural("profile"),
					cancelIfAutoRefreshEnabled.ToYesNo()), Mode.Stop, index);
		}

		/// <summary>
		///     Reads and returns a profile structure from the file, NULL in case of any error
		/// </summary>
		/// <param name="filePath">Path of the file to be read</param>
		/// <returns></returns>
		public static Profile ReadProfile(string filePath)
		{
			Profile p = new Profile();
			try
			{
				XDocument file = XDocument.Load(filePath);
				p.Name = file.Root.Attribute("Name").Value;
				p.FileName = filePath;
				p.Entries = new List<Shortcut>();

				foreach (XElement x in file.Root.Elements("Link"))
				{
					XAttribute linkName = x.Attribute("LinkName");
					XAttribute linkPath = x.Attribute("LinkPath");
					XAttribute iconPath = x.Attribute("IconPath") ?? new XAttribute("IconPath", "");
					XAttribute iconIndex = x.Attribute("IconIndex") ?? new XAttribute("IconPath", "0");

					if (linkName != null && linkPath != null)
						p.Entries.Add(new Shortcut(linkName.Value, linkPath.Value, iconPath.Value, iconIndex.Value.ToInt()));
				}

				return p;
			}
			catch (Exception ex)
			{
				Logger.Log(
					string.Format("Exception while reading profile: {0} ({1}, {2})", ex.Message, filePath, ex.TargetSite.Name),
					LogType.Error);

				p.HasFailedLoading = true;
				return p;
			}
		}

		/// <summary>
		///     Saves the selected profile to the selected path
		/// </summary>
		/// <param name="p">Profile to be saved</param>
		public static void SaveProfile(Profile p)
		{
			int index = Logger.Log("Saving profile...", Mode.Start, 4);

			XWriter x = new XWriter();
			x.Open(p.FileName);

			x.WriteElement("Profile", new Attribute("Name", p.Name), new Attribute("Version", Application.ProductVersion));

			foreach (Shortcut shortcut in p.Entries)
			{
				x.WriteElement("Link", true, new Attribute("LinkName", shortcut.Name), new Attribute("LinkPath", shortcut.Path),
					new Attribute("IconPath", shortcut.Icon), new Attribute("IconIndex", shortcut.IconIndex.ToString()));
			}

			x.Dispose();

			if (Path.GetFileNameWithoutExtension(p.FileName) != p.Name)
				File.Move(p.FileName, ProfilesPath + "\\" + p.Name + ".wfp");

			Logger.Log("Saved profile ({0})", Mode.Stop, index);
		}

		/// <summary>
		///     Deletes a profile
		/// </summary>
		/// <param name="profileIndex">Index of the profile in the combo box</param>
		private void DeleteProfile(int profileIndex)
		{
			Alert a = new Alert(
				"Are you sure you would like to <b>delete this profile</b> (" + _loadedProfiles[profileIndex].Name + ")?"
				, "Delete confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

			a.Btn2Highlight = Highlight.Red;
			a.Btn2Text = "&Delete";
			a.Btn3Text = "&Cancel";
			if (a.Show() != DialogResult.Yes)
			{
				return;
			}

			try
			{
				File.Delete(_loadedProfiles[profileIndex].FileName);
			}
			catch (Exception ex)
			{
				Logger.Log(
					string.Format("Exception while deleting profile: {0} ({1}, {2})", ex.Message, _loadedProfiles[profileIndex].Name,
						ex.TargetSite.Name), LogType.Error);
				Notifier.Show("Failed to delete profile: " + ex.Message, Resources.Warning);
			}

			ScanForProfiles(true);
		}

		/// <summary>
		///     Shows the edit window
		/// </summary>
		/// <param name="p">Profile to be edited</param>
		/// <param name="edit">If true, will only edit the profile</param>
		/// <param name="activateWindow">If true, the app will activate the edit window once shown</param>
		private void ShowEdit(Profile p, bool edit, bool activateWindow = false)
		{
			for (int i = 0; i < Application.OpenForms.Count; i++)
			{
				Form f = Application.OpenForms[i];

				if (f is FrmMain || f is Notiwindow) continue;

				Alert.Show("Please close all windows but the main one before editing!");
				return;
			}

			FrmProfile x = new FrmProfile(p, edit, LsvProfile.View);
			if (activateWindow) Activate();
			x.ShowDialog();

			ScanForProfiles(true);

			if (x.DialogResult == DialogResult.OK) SuggestFeedback("How easy was it to create/edit this profile?");
		}

		public static void SuggestFeedback(string s)
		{
			if (_feedbacks >= 10 || new Random().Next(0, 2) != 1 || !C.ChbHelpImprove.Checked) return;

			_feedbacks++;
			Notifier.Show(s, "Click here to send feedback", () => new FrmFeedback().ShowDialog());
		}

		/// <summary>
		///     Applies the currently selected profile and creates the shortcuts it contains
		/// </summary>
		private void ApplyProfile(Profile targetProfile, bool isTask = false)
		{
			if (C.ChbTooManyToApply.Checked && targetProfile.Entries.Count > 25)
			{
				Alert alert = new Alert("The profile you want to apply has too many shortcuts. Applying them may take a while and cause instabilities in the File Explorer\n" +
										"What do you want to do", MessageBoxButtons.YesNo, "Never ask again");

				alert.Btn2Text = "&Apply";
				alert.Btn3Text = "&Cancel";
				alert.Btn2Highlight = Highlight.Orange;

				if (alert.Show() == DialogResult.No)
				{
					if (alert.CheckBoxState) C.ChbTooManyToApply.Checked = false;
					return;
				}
				if (alert.CheckBoxState) C.ChbTooManyToApply.Checked = false;
			}

			Alert a = new Alert("Your current favorites will be overwritten, what do you want to do?", "Backup confirmation",
						MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, "Next time, don't backup");

			a.Btn1Text = "&Backup";
			a.Btn2Text = "&Apply";
			a.Btn3Text = "&Cancel";
			a.Btn2Highlight = Highlight.Orange;

			if (C.ChbBackup.Checked && !isTask)
				switch (a.Show())
				{
					case DialogResult.Yes:
						Profile p = ProfileFromSetup();
						p.Name = "Backup " + DateTime.Now.ToString("HH-mm-ss");

						if (File.Exists(ProfilesPath + @"\" + p.Name + ".wfp"))
						{
							for (int i = 1; ; i++)
							{
								if (File.Exists(ProfilesPath + @"\" + p.Name + " (" + i + ").wfp")) continue;

								p.Name = "Backup (" + i + ")";
								break;
							}
						}

						p.FileName = ProfilesPath + @"\" + p.Name + ".wfp";
						SaveProfile(p);
						if (a.CheckBoxState) C.ChbBackup.Checked = false;
						break;
					case DialogResult.No:
						if (a.CheckBoxState) C.ChbBackup.Checked = false;
						break;
					case DialogResult.Cancel:
						if (a.CheckBoxState) C.ChbBackup.Checked = false;
						return;
				}

			int index =
				Logger.Log(
					string.Format("Applying {0} ({1})...", isTask ? "task" : "profile",
						targetProfile.Entries.Count.IsPlural("shortcut")),
					Mode.Start, 0);

			foreach (string file in Directory.GetFiles(LinksPath).Where(file => file.EndsWith(".lnk") || file.EndsWith(".url")))
			{
				File.Delete(file);
			}

			bool hasErrorOccured = false;

			foreach (Shortcut shortcut in targetProfile.Entries)
				if (!CreateShortcut(shortcut.Name, shortcut.Path, shortcut.Icon, shortcut.IconIndex)) hasErrorOccured = true;

			if (C.ChbOpenExplorer.Checked) Process.Start("explorer");

			Logger.Log(string.Format("Applied ({0}, scheduled={1})", "{0}", isTask.ToYesNo()), Mode.Stop, index);

			if (!isTask)
				Notifier.Show(string.Format(hasErrorOccured
					? "The profile \"{0}\" was applied{1} but some shortcuts might be missing."
					: "The profile \"{0}\" was successfully applied{1}!", targetProfile.Name, ""),
					"Profile applied", Resources.Profile, "Click here to schedule this profile", () => MsiTask_Click(null, null));
			else
				Notifier.Show(string.Format(hasErrorOccured
					? "The profile \"{0}\" was applied{1} but some shortcuts might be missing."
					: "The profile \"{0}\" was successfully applied{1}!", targetProfile.Name, " as scheduled"),
					"Profile applied", Resources.Profile);
		}

		/// <summary>
		///     Creates a shortcut for the selected path
		/// </summary>
		/// <param name="name">Name of the shortcut</param>
		/// <param name="path">The path that the shortcut will link to</param>
		/// <param name="icon">Path of the custom icon</param>
		/// <param name="index">Index of icon in file</param>
		private bool CreateShortcut(string name, string path, string icon, int index)
		{
			try
			{
				if (!Directory.Exists(path)) return false;

				WshShellClass wsh = new WshShellClass();
				IWshShortcut shortcut = wsh.CreateShortcut(LinksPath + @"\" + name + ".lnk") as IWshShortcut;
				shortcut.TargetPath = path;
				shortcut.Description = "Created using WinFavs";
				if (icon != "")
					shortcut.IconLocation = icon + "," + index;
				shortcut.Save();

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log("Failed to create shortcut: " + ex.Message + " (" + ex.TargetSite.Name + ")", LogType.Error);
				return false;
			}
		}

		/// <summary>
		///     Returns the path that a .lnk is referring to
		/// </summary>
		/// <param name="lnkPath"></param>
		/// <returns></returns>
		public static string GetLnkTarget(string lnkPath)
		{
			var shl = new Shell();
			lnkPath = Path.GetFullPath(lnkPath);
			var dir = shl.NameSpace(Path.GetDirectoryName(lnkPath));
			var itm = dir.Items().Item(Path.GetFileName(lnkPath));
			var lnk = (ShellLinkObject)itm.GetLink;

			return lnk.Target.Path;
		}

		/// <summary>
		///     Create a profile from the current setup
		/// </summary>
		/// <returns></returns>
		private Profile ProfileFromSetup()
		{
			Profile p = new Profile("", "");

			foreach (string file in Directory.GetFiles(LinksPath).Where(file => file.EndsWith(".lnk") || file.EndsWith(".url")))
			{
				string icon = "";
				int index = 0;

				try
				{
					WshShellClass wsh = new WshShellClass();
					IWshShortcut shortcut = wsh.CreateShortcut(file) as IWshShortcut;
					icon = shortcut.IconLocation.Substring(0, shortcut.IconLocation.LastIndexOf(','));
					index = shortcut.IconLocation.Substring(icon.Length + 1).ToInt();
				}
				catch
				{
				}

				// ReSharper disable once AssignNullToNotNullAttribute
				p.Entries.Add(new Shortcut(Path.GetFileNameWithoutExtension(file), GetLnkTarget(file), icon, index));
			}

			return p;
		}

		/// <summary>
		///     Copies all files in the collection to the profiles path
		/// </summary>
		/// <param name="collection">Collection of string containing file paths</param>
		public void OpenFiles(IEnumerable<string> collection)
		{
			IEnumerable<string> e = collection as IList<string> ?? collection.ToList();
			if (!e.Any()) return;

			int i = Logger.Log("Opening file...", Mode.Start, 0);
			int action = 0;

			foreach (string name in e)
			{
				if (!File.Exists(name) || !name.EndsWith(".wfp") || Path.GetDirectoryName(name) == ProfilesPath) continue;

				string newPath = ProfilesPath + "\\" + Path.GetFileName(name);

				if (action == 0 && File.Exists(newPath))
				{
					Alert a = new Alert("A profile with the same name (" + Path.GetFileNameWithoutExtension(name) + ") already exists\n" +
									"What do you want to do?", "",
						MessageBoxButtons.YesNo, "Do this action for all files");

					a.Btn2Text = "&Overwrite";
					a.Btn3Text = "&Skip";

					if (a.Show() != DialogResult.Yes)
					{
						if (a.CheckBoxState) action = -1; //skip all
						continue;
					}
					if (a.CheckBoxState) action = 1; //overwrite all
				}
				if (action == -1 && File.Exists(newPath)) continue;

				if (File.Exists(newPath)) File.Delete(newPath);
				File.Copy(name, newPath, true);
			}
			Logger.Log("Done opening ({0})", Mode.Stop, i);

			if (e.Count() < 2 || !File.Exists(e.ToList()[1])) return;

			if (e.ToList()[0] == "-a") ApplyProfile(ReadProfile(e.ToList()[1]));
			if (e.ToList()[0] == "-e") ShowEdit(ReadProfile(e.ToList()[1]), true, true);
		}

		#endregion

		#region Other methods

		/// <summary>
		///     Loads settings and sets related controls
		/// </summary>
		private void Configure()
		{
			int index = Logger.Log("Configuring...", Mode.Start, 2);

			Config.Add(C.ChbAutoUpdate, "Checked", true, "Main", "CheckForUpdates");
			Config.Add(C.ChbMinimizeToTray, "Checked", true, "Main", "MinimizeToTray");
			Config.Add(C.ChbConservative, "Checked", false, "Main", "ConservativeMode");
			Config.Add(C.ChbStartMinimized, "Checked", false, "Main", "StartMinimized");
			Config.Add(C.ChbFeedback, "Checked", true, "Feedback", "AskForFeedback");
			Config.Add(C.ChbHelpImprove, "Checked", true, "Feedback", "HelpImprove");
			Config.Add(C.ChbOpenExplorer, "Checked", true, "Profile", "OpenExplorer");
			Config.Add(C.ChbBackup, "Checked", true, "Profile", "PromptForBackup");
			Config.Add(C.ChbTooManyToApply, "Checked", true, "Profile", "TooManyShortcutsWarning");
			Config.Add(C.CbxProfileView, "SelectedIndex", 4, 4, 0, "View", "View");
			Config.Add(this, "LastViewed", "", "View", "LastViewed");
			Config.Add(C.ChbRefresh, "Checked", true, "View", "AutoRefresh");
			Config.Add(C.ChbTasks, "Checked", true, "Task", "EnableTasks");
			Config.Add(C.ChbTaskConfirm, "Checked", false, "Task", "ConfirmTask");
			Config.Add(C.ChbNoShortcuts, "Checked", true, "Dialogs", "NoShortcuts");
			Config.Add(C.ChbHints, "Checked", true, "Dialogs", "Hints");
			Config.Add(this, "TaskHint", true, "Hints", "Tasks");
			Config.Add(this, "ShortcutHint", true, "Hints", "Shortcuts");
			Config.Add(this, "ProfileHint", true, "Hints", "Profiles");
			Config.Add(C.ChbTargetNotExist, "Checked", true, "Dialogs", "FolderInexistant");
			Config.Add(this, "Ignored", "0.0.0", "Dialogs", "IgnoredVersion");
			Config.Add(C.ChbPop, "Checked", true, "Notifications", "Sound");

			Config.Read(ConfigPath);

			if (C.ChbAutoUpdate.Checked) CheckForUpdates(true);

			Logger.Log("Finished configuring ({0})", Mode.Stop, index);
		}

		/// <summary>
		/// Saves configuration
		/// </summary>
		private void SaveConfig(CloseReason c)
		{
			int index = Logger.Log("Closing and saving (" + c + ")...", Mode.Start, 1);

			if (_loadedProfiles.Count > 0) LastViewed = _loadedProfiles[CbxProfiles.SelectedIndex].Name;
			Config.Save(ConfigPath);

			NtiMain.Visible = false;

			Logger.Log("Finished saving ({0})", Mode.Stop, index);
		}

		/// <summary>
		///     Checks for updates
		/// </summary>
		/// <param name="isSilent">If true, won't show any error or up to date messages</param>
		public void CheckForUpdates(bool isSilent)
		{
			if (!C.BtnUpdate.Enabled) return;

			Thread t = new Thread(x =>
			{
				C.BtnUpdate.Enabled = false;
				MsiCheckForUpdates.Enabled = false;
				C.BtnUpdate.Text = "Checking for updates...";

				Updater u = new Updater("https://dl.dropboxusercontent.com/u/58717780/WFV/Version.txt",
					"https://dl.dropboxusercontent.com/u/58717780/WFV/Changelog.txt",
					"https://dl.dropboxusercontent.com/u/58717780/WFV/Download.txt");

				u.Update(Application.ProductVersion, isSilent);

				C.BtnUpdate.Enabled = true;
				MsiCheckForUpdates.Enabled = true;
				C.BtnUpdate.Text = "Check for updates";
			}) { IsBackground = true };

			t.Start();
		}

		private void InitReggy(bool noRestart)
		{
			Reggy.Extension = ".wfp";
			Reggy.AppName = "WinFavs";
			Reggy.Description = "WinFavs profile";
			Reggy.KeyName = "wfpfile";
			Reggy.Icon = Application.ExecutablePath + ",1";

			if (File.Exists(ConfigPath)) return;

			if (!Reggy.IsUserAdministrator() && !noRestart) RestartAsAdmin();

			_showWelcomeHint = true;
			Reggy.AddToStart(true);
			Reggy.CreateRegistry(true);
			Reggy.Register("edit", "Edit this profile", "\"" + Application.ExecutablePath + "\"" + " -e \"%1\"");
			Reggy.Register("apply", "Apply this profile", "\"" + Application.ExecutablePath + "\"" + " -a \"%1\"");
		}

		/// <summary>
		///     Changes the listview's view mode
		/// </summary>
		/// <param name="viewIndex">Index of the view</param>
		public void SelectView(int viewIndex)
		{
			C.CbxProfileView.SelectedIndex = viewIndex;

			switch (viewIndex)
			{
				default:
					LsvProfile.View = View.LargeIcon;
					break;
				case 1:
					LsvProfile.View = View.Details;
					break;
				case 2:
					LsvProfile.View = View.SmallIcon;
					break;
				case 3:
					LsvProfile.View = View.List;
					break;
				case 4:
					LsvProfile.View = View.Tile;
					break;
			}

			HdrName.Width = -1;
			HdrPath.Width = -2;
		}

		/// <summary>
		/// Moves old presets to profiles
		/// </summary>
		public void MigratePresets()
		{
			if (!_canMigrate) return;
			_canMigrate = false;
			string presets = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\WinFavs\Presets";

			Notifier.Show(
				"You seem to have some profiles and/or tasks from older WinFavs versions, would you like to migrate those to use them in this version?",
				"Click here to migrate",
				() =>
				{
					try
					{
						if (Directory.Exists(presets))
						{
							foreach (
								string file in
									Directory.GetFiles(presets).Where(file => !File.Exists(ProfilesPath + "\\" + Path.GetFileName(file))))
								File.Move(file, ProfilesPath + "\\" + Path.GetFileName(file));

							Directory.Delete(presets);
						}

						foreach (string s in Directory.GetFiles(TasksPath))
						{
							Task t = Tasks.ReadTask(s, true, true);
							if (t.HasFailedLoading || !t.IsOld) continue;

							string days = "";
							if (t.Mon) days = "Mon";
							if (t.Tue) days += (days == "" ? "" : ",") + "Tue";
							if (t.Wed) days += (days == "" ? "" : ",") + "Wed";
							if (t.Thu) days += (days == "" ? "" : ",") + "Thu";
							if (t.Fri) days += (days == "" ? "" : ",") + "Fri";
							if (t.Sat) days += (days == "" ? "" : ",") + "Sat";
							if (t.Sun) days += (days == "" ? "" : ",") + "Sun";

							Tasks.WriteTask(t.Path, t.Enabled.ToString(), days, t.Hour.ToString(), t.Minute.ToString(), t.Profile);
						}

						Notifier.Show("The migration process succeeded!");
					}
					catch (Exception ex)
					{
						Notifier.Show("The migration process failed.\nThe application will prompt you again in its next startup");
						Logger.Log("Failed to migrate: " + ex.Message, LogType.Error);
					}

					ScanForProfiles(true);
					Tasks.ScanForTasks(ref _loadedTasks, true, true);
				});
		}

		private void RestartAsAdmin()
		{
			Logger.Log("Restarting the app as admin");
			Logger.WaitForLog();
			Process p = new Process();
			p.StartInfo.FileName = Application.ExecutablePath;
			p.StartInfo.Verb = "runas";
			p.StartInfo.Arguments = "-norestart";
			p.Start();
			Process.GetCurrentProcess().Kill();
		}

		private void SendLog()
		{
			Thread t = new Thread(x =>
			{
				try
				{
					if (!File.Exists(Logger._oldLog)) return;

					FrmFeedback.SendFeedback(Debugger.IsAttached ? "devlog" : "", 1, "",
						FrmFeedback.Compress(File.ReadAllText(Logger._oldLog)));

					File.Delete(Logger._oldLog);

					Logger.Log("Sent old log");
				}
				catch (Exception ex)
				{
					Logger.Log("Failed to send old log: " + ex.Message, LogType.Error);
				}
			});
			t.IsBackground = true;
			t.Start();
		}

		private void ExtendGlass()
		{
			bool en;
			MARGINS mg = new MARGINS();
			mg.topHeight = 38;
			Win32.SetCueText(TxtSearch, "Search for profiles");

			if (Environment.OSVersion.Version.Major < 6) return;

			Win32.DwmIsCompositionEnabled(out en);//check if the desktop composition is enabled

			if (en) Win32.DwmExtendFrameIntoClientArea(Handle, ref mg);
		}

		#endregion

		#region MenuStrip

		private void donateToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Thread t = new Thread(x =>
			{
				MsiDonate.Enabled = false;
				try
				{
					WebClient w = new WebClient();

					if (_donateLink == "")
						_donateLink = w.DownloadString("https://dl.dropboxusercontent.com/u/58717780/WFV/Donate.txt");

					if (_donateLink == "http://www.youtube.com/VBTheory")
						BeginInvoke(
							new MethodInvoker(
								() =>
									Notifier.Show("The donate link is not ready yet. In the meantime, please check out my YouTube channel ;).")));

					Process.Start(_donateLink);

					w.Dispose();
				}
				catch
				{
					BeginInvoke(
						new MethodInvoker(
							() =>
								Notifier.Show("Couldn't fetch the donation link, please try again later", Resources.Warning,
									"Click here to try again", () => donateToolStripMenuItem_Click(null, null))));
				}
				finally
				{
					MsiDonate.Enabled = true;
				}
			}) { IsBackground = true };
			t.Start();
		}

		private void MsiRefreshTasks_Click(object sender, EventArgs e)
		{
			Tasks.ScanForTasks(ref _loadedTasks, true, false);
		}

		public void showToolStripMenuItem_Click(object sender, EventArgs e)
		{
			CmiShow.Visible = false;
			Visible = true;
			ShowInTaskbar = true;
			ExtendGlass();
			WindowState = FormWindowState.Normal;
			Activate();
		}

		private void MsiFromSetup_Click(object sender, EventArgs e)
		{
			Logger.Log("Creating new profile from setup...");
			ShowEdit(ProfileFromSetup(), false);
		}

		private void MsiShowProfiles_Click(object sender, EventArgs e)
		{
			Process.Start(ProfilesPath);
		}

		private void MsiExit_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void MsiDelete_Click(object sender, EventArgs e)
		{
			if (TxtSearch.Focused) return;
			DeleteProfile(CbxProfiles.SelectedIndex);
		}

		private void MsiRefresh_Click(object sender, EventArgs e)
		{
			ScanForProfiles(false);
		}

		private void MsiNewProfile_Click(object sender, EventArgs e)
		{
			Logger.Log("Creating new profile...");
			Profile p = new Profile("", "");

			ShowEdit(p, false);
		}

		private void MsiEdit_Click(object sender, EventArgs e)
		{
			Logger.Log("Editing profile...");
			ShowEdit(_loadedProfiles[CbxProfiles.SelectedIndex], true);
		}

		private void MsiApply_Click(object sender, EventArgs e)
		{
			ApplyProfile(_loadedProfiles[CbxProfiles.SelectedIndex]);
		}

		private void MsiFromDefault_Click(object sender, EventArgs e)
		{
			Logger.Log("Creating new profile from default...");
			Profile p = new Profile
			{
				Entries = new List<Shortcut>
				{
					new Shortcut("Desktop", Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "", 0),
					new Shortcut("Downloads", Environment.GetEnvironmentVariable("USERPROFILE") + @"\Downloads", "", 0)
				}
			};

			ShowEdit(p, false);
		}

		private void MsiFromProfile_Click(object sender, EventArgs e)
		{
			Logger.Log("Creating new profile from current...");
			Profile p = _loadedProfiles[CbxProfiles.SelectedIndex];
			p.Name = "";
			ShowEdit(p, false);
		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			new FrmAbout().ShowDialog();
		}

		private void MsiFeedback_Click(object sender, EventArgs e)
		{
			MsiFeedback.Enabled = false;

			FrmFeedback f = new FrmFeedback();
			f.Closed += (o, args) => MsiFeedback.Enabled = true;

			f.Show();
		}

		private void MsiOpen_Click(object sender, EventArgs e)
		{
			OpenFileDialog o = new OpenFileDialog
			{
				Filter = "WinFavs profile file (*.wfp)|*.wfp",
				InitialDirectory = ProfilesPath,
				Title = "Open profile file",
				Multiselect = true
			};

			if (o.ShowDialog() != DialogResult.OK) return;

			OpenFiles(o.FileNames);

			ScanForProfiles(true);
		}

		private void tmi_Click(object sender, EventArgs e)
		{
			ApplyProfile(_loadedProfiles[Convert.ToInt32(((ToolStripMenuItem)sender).Tag)]);
		}

		private void MsiShowTasks_Click(object sender, EventArgs e)
		{
			new FrmTasks().ShowDialog();
		}

		private void MsiShowTasks_Click_1(object sender, EventArgs e)
		{
			Process.Start(TasksPath);
		}

		private void MsiTask_Click(object sender, EventArgs e)
		{
			if (new FrmTask(new Task
			{
				Enabled = true,
				Profile = _loadedProfiles[CbxProfiles.SelectedIndex].Name,
				Hour = DateTime.Now.Hour,
				Minute = DateTime.Now.Minute,
				Mon = true,
				Tue = true,
				Wed = true,
				Thu = true,
				Fri = true,
				Sat = true,
				Sun = true
			}).ShowDialog() != DialogResult.OK)
			{
				return;
			}

			SuggestFeedback("How easy was it to schedule this profile?");

			new FrmTasks().ShowDialog();
		}

		private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			C.ShowDialog();
			SuggestFeedback("How easy was it to tune WinFavs to your likings");
		}

		private void MsiCheckForUpdates_Click(object sender, EventArgs e)
		{
			CheckForUpdates(false);
		}

		private void TtpMain_Draw(object sender, DrawToolTipEventArgs e)
		{
			e.Graphics.Clear(Color.Gray);
			ControlPaint.DrawBorder(e.Graphics, e.Bounds, Color.DimGray, ButtonBorderStyle.Solid);

			TextRenderer.DrawText(e.Graphics, e.ToolTipText, e.Font, e.Bounds, Color.FromArgb(20, 20, 20));
		}

		#endregion

		#region Experimental

		private void openLogReaderToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Process.Start(
				@"C:\Users\VB\Dropbox\Visual Studio 2013\Projects\UnLoggerReader\UnLoggerReader\bin\Debug\UnLoggerReader.exe");
		}

		private void showLoaderToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.ShowLoader("Fuck you");

			Thread t = new Thread(() =>
			{
				Thread.Sleep(10000);
				this.RemoveLoader();
			});
			t.IsBackground = true;
			t.Start();
		}

		private void ownerDrawTooltipToolStripMenuItem_Click(object sender, EventArgs e)
		{
			TtpMain.OwnerDraw = !TtpMain.OwnerDraw;
		}

		private void tryALERTToolStripMenuItem_Click(object sender, EventArgs e)
		{
			while (true)
			{
				Random r = new Random();

				string message = "";
				for (int i = 0; i < r.Next(1, 250); i++)
					message += (r.Next(0, 2) == 0 || message == "" ? "" : "\n") + Path.GetRandomFileName();

				string title = Path.GetRandomFileName();

				MessageBoxIcon m;
				switch (r.Next(0, 4))
				{
					case 0:
						m = MessageBoxIcon.Warning;
						break;
					case 1:
						m = MessageBoxIcon.Error;
						break;
					case 2:
						m = MessageBoxIcon.None;
						break;
					case 3:
						m = MessageBoxIcon.Question;
						break;
					default:
						m = MessageBoxIcon.Information;
						break;
				}

				MessageBoxButtons mbb = (MessageBoxButtons)r.Next(0, 5);
				string checkbox = r.Next(0, 2) == 0 ? "" : Path.GetRandomFileName();
				bool b = r.Next(0, 1) != 0;

				Alert a = new Alert(message, title, mbb, m, checkbox, b);

				if (r.Next(0, 2) != 0) a.Btn1Text = Path.GetRandomFileName();
				if (r.Next(0, 2) != 0) a.Btn2Text = Path.GetRandomFileName();
				if (r.Next(0, 2) != 0) a.Btn3Text = Path.GetRandomFileName();

				if (r.Next(0, 2) != 0) a.Btn1Highlight = (Highlight)r.Next(0, 5);
				if (r.Next(0, 2) != 0) a.Btn2Highlight = (Highlight)r.Next(0, 5);
				if (r.Next(0, 2) != 0) a.Btn3Highlight = (Highlight)r.Next(0, 5);

				a.Show();
			}
			// ReSharper disable once FunctionNeverReturns
		}

		#endregion

	}

}