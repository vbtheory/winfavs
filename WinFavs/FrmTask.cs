﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WinFavs.Classes;

namespace WinFavs
{
	public partial class FrmTask : Form
	{
		private string _path = "";

		public FrmTask(Task s)
		{
			InitializeComponent();

			Lighter.SetHighlight(BtnSave, Highlight.Green);

			ChbMon.Checked = s.Mon;
			ChbTue.Checked = s.Tue;
			ChbWed.Checked = s.Wed;
			ChbThu.Checked = s.Thu;
			ChbFri.Checked = s.Fri;
			ChbSat.Checked = s.Sat;
			ChbSun.Checked = s.Sun;

			ChbEnabled.Checked = s.Enabled;

			DtpMain.Value = new DateTime(DtpMain.Value.Year, DtpMain.Value.Month, DtpMain.Value.Day, s.Hour, s.Minute, 0);

			ScanForProfiles(false);

			int index = CbxProfiles.Items.IndexOf(s.Profile ?? "");
			CbxProfiles.SelectedIndex = index != -1 ? index : (CbxProfiles.Items.Count > 0 ? 0 : -1);

			_path = s.Path ?? "";

			if (Text.EndsWith("*")) Text = Text.Substring(0, Text.Length - 1);
		}

		/// <summary>
		///     Scans the profiles folder for all the available profiles
		/// </summary>
		private void ScanForProfiles(bool doRefresh = true)
		{
			string previousName = CbxProfiles.Text;

			if(doRefresh) FrmMain.Frm.ScanForProfiles(false);

			CbxProfiles.Items.Clear();

			foreach (Profile profile in FrmMain.Frm._loadedProfiles)CbxProfiles.Items.Add(profile.Name);

			if (CbxProfiles.Items.Count <= 0) return;

			CbxProfiles.SelectedIndex = previousName == "" ? 0 : FrmMain.Frm._loadedProfiles.FindIndex(x => x.Name == previousName);
			if (CbxProfiles.SelectedIndex == -1) CbxProfiles.SelectedIndex = 0;
		}

		private void CbxProfiles_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!Text.EndsWith("*")) Text += "*";
			BtnSave.Enabled = CbxProfiles.SelectedIndex != -1;
		}

		private void BtnSave_Click(object sender, EventArgs e)
		{
			int i = Logger.Log("Saving task...", Mode.Start, 0);
			if (_path == "") _path = FrmMain.TasksPath + "\\" + Path.GetRandomFileName() + ".tsk";

			string days = "";
			foreach (CheckBox checkBox in panel1.Controls.OfType<CheckBox>().Where(c => c.Checked))
				days += (days != "" ? "," : "") + checkBox.Text;

			Tasks.WriteTask(_path, ChbEnabled.Checked.ToString(), days, DtpMain.Value.Hour.ToString("D2"),
				DtpMain.Value.Minute.ToString("D2"), CbxProfiles.Text);

			Logger.Log(string.Format("Written task ({0}, {1}, {2})", "{0}", days, CbxProfiles.Text), Mode.Stop, i);
		}

		private void FrmEditTask_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!Text.EndsWith("*") || DialogResult == DialogResult.OK) return;

			Alert a = new Alert("You have some unsaved changes, would you like to <b>save before closing</b>?", "Saving confirmation",
					MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);

			a.Btn1Text = "&Save";
			a.Btn2Text = "&Don't save";
			a.Btn3Text = "&Cancel";

			switch (a.Show())
			{
				case DialogResult.Yes:
					DialogResult = DialogResult.OK;
					BtnSave_Click(null, null);
					break;
				case DialogResult.No:
					DialogResult = DialogResult.Cancel;
					break;
				case DialogResult.Cancel:
					e.Cancel = true;
					break;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			ScanForProfiles();
		}

		private void FrmTask_Shown(object sender, EventArgs e)
		{
			if (!FrmMain.C.ChbHints.Checked || !FrmMain.Frm.TaskHint) return;

			FrmMain.Frm.TaskHint = false;

			Alert.Show(
				"Welcome to the tasks dialog. Here you can create a sort of alarm that will automatically apply a profile at a specified time\n" +
				"There are a number of settings you can use to personalize this task:\n" +
				"<bullet>- Profile: which is the profile that will be applied when this task is triggered.\n" +
				"- Time: this specifies at which time of the day it will be triggered\n" +
				"- Repeat: these are the days in which this task will be enabled\n" +
				"- Enabled: whether or not this task is active.</bullet>",
				"How to use the tasks dialog");
		}

		private void FrmTask_FormClosed(object sender, FormClosedEventArgs e)
		{
			Dispose(true);
		}

	}
}