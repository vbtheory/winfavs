﻿using WinFavs.Controls;

namespace WinFavs
{
	partial class FrmProfile
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Available", System.Windows.Forms.HorizontalAlignment.Left);
			System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Non available", System.Windows.Forms.HorizontalAlignment.Left);
			this.LsvProfile = new System.Windows.Forms.ListView();
			this.HdrName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.HdrPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ImgLarge = new System.Windows.Forms.ImageList(this.components);
			this.ImgSmall = new System.Windows.Forms.ImageList(this.components);
			this.LblName = new System.Windows.Forms.Label();
			this.TmrCheckEnable = new System.Windows.Forms.Timer(this.components);
			this.LblDrop = new System.Windows.Forms.Label();
			this.TtpMain = new System.Windows.Forms.ToolTip(this.components);
			this.BtnSave = new System.Windows.Forms.Button();
			this.BtnCancel = new System.Windows.Forms.Button();
			this.BtnClear = new System.Windows.Forms.Button();
			this.BtnRemove = new System.Windows.Forms.Button();
			this.BtnEdit = new System.Windows.Forms.Button();
			this.BtnAdd = new WinFavs.Controls.SplitButton();
			this.CmsSplit = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.CsiNew = new System.Windows.Forms.ToolStripMenuItem();
			this.CsiFromCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.CmsClipboard = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.CsiCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.CsiCut = new System.Windows.Forms.ToolStripMenuItem();
			this.sep1 = new WinFavs.Controls.Sep();
			this.CsiPaste = new System.Windows.Forms.ToolStripMenuItem();
			this.BtnMoveDown = new System.Windows.Forms.Button();
			this.BtnMoveUp = new System.Windows.Forms.Button();
			this.TxtName = new WinFavs.Controls.RuleBox();
			this.CmsSplit.SuspendLayout();
			this.CmsClipboard.SuspendLayout();
			this.SuspendLayout();
			// 
			// LsvProfile
			// 
			this.LsvProfile.AllowDrop = true;
			this.LsvProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LsvProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.LsvProfile.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.HdrName,
            this.HdrPath});
			this.LsvProfile.ForeColor = System.Drawing.Color.Black;
			this.LsvProfile.FullRowSelect = true;
			this.LsvProfile.GridLines = true;
			listViewGroup1.Header = "Available";
			listViewGroup1.Name = "GrpAvailable";
			listViewGroup2.Header = "Non available";
			listViewGroup2.Name = "GrpNonAvailable";
			this.LsvProfile.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
			this.LsvProfile.HideSelection = false;
			this.LsvProfile.LabelEdit = true;
			this.LsvProfile.LargeImageList = this.ImgLarge;
			this.LsvProfile.Location = new System.Drawing.Point(13, 13);
			this.LsvProfile.Name = "LsvProfile";
			this.LsvProfile.ShowItemToolTips = true;
			this.LsvProfile.Size = new System.Drawing.Size(455, 324);
			this.LsvProfile.SmallImageList = this.ImgSmall;
			this.LsvProfile.TabIndex = 0;
			this.LsvProfile.UseCompatibleStateImageBehavior = false;
			this.LsvProfile.View = System.Windows.Forms.View.Details;
			this.LsvProfile.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.LsvMain_AfterLabelEdit);
			this.LsvProfile.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.LsvProfile_RetrieveVirtualItem);
			this.LsvProfile.DragDrop += new System.Windows.Forms.DragEventHandler(this.LsvMain_DragDrop);
			this.LsvProfile.DragEnter += new System.Windows.Forms.DragEventHandler(this.LsvMain_DragEnter);
			this.LsvProfile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LsvMain_KeyDown);
			this.LsvProfile.MouseUp += new System.Windows.Forms.MouseEventHandler(this.LsvMain_MouseUp);
			// 
			// HdrName
			// 
			this.HdrName.Text = "Name";
			this.HdrName.Width = 25;
			// 
			// HdrPath
			// 
			this.HdrPath.Text = "Path";
			this.HdrPath.Width = 364;
			// 
			// ImgLarge
			// 
			this.ImgLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ImgLarge.ImageSize = new System.Drawing.Size(32, 32);
			this.ImgLarge.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// ImgSmall
			// 
			this.ImgSmall.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ImgSmall.ImageSize = new System.Drawing.Size(24, 24);
			this.ImgSmall.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// LblName
			// 
			this.LblName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.LblName.AutoSize = true;
			this.LblName.Location = new System.Drawing.Point(475, 240);
			this.LblName.Name = "LblName";
			this.LblName.Size = new System.Drawing.Size(38, 13);
			this.LblName.TabIndex = 4;
			this.LblName.Text = "Name:";
			// 
			// TmrCheckEnable
			// 
			this.TmrCheckEnable.Enabled = true;
			this.TmrCheckEnable.Interval = 10;
			this.TmrCheckEnable.Tick += new System.EventHandler(this.TmrCheckSelect_Tick);
			// 
			// LblDrop
			// 
			this.LblDrop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.LblDrop.AutoSize = true;
			this.LblDrop.Location = new System.Drawing.Point(472, 184);
			this.LblDrop.Name = "LblDrop";
			this.LblDrop.Size = new System.Drawing.Size(110, 13);
			this.LblDrop.TabIndex = 9;
			this.LblDrop.Text = " ← Or drop folder here";
			// 
			// TtpMain
			// 
			this.TtpMain.AutoPopDelay = 5000;
			this.TtpMain.InitialDelay = 250;
			this.TtpMain.ReshowDelay = 100;
			// 
			// BtnSave
			// 
			this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.BtnSave.Enabled = false;
			this.BtnSave.Location = new System.Drawing.Point(474, 285);
			this.BtnSave.Name = "BtnSave";
			this.BtnSave.Size = new System.Drawing.Size(130, 23);
			this.BtnSave.TabIndex = 8;
			this.BtnSave.Text = "&Save profile (Alt+S)";
			this.TtpMain.SetToolTip(this.BtnSave, "Save this profile");
			this.BtnSave.UseVisualStyleBackColor = true;
			this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
			// 
			// BtnCancel
			// 
			this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnCancel.Location = new System.Drawing.Point(474, 314);
			this.BtnCancel.Name = "BtnCancel";
			this.BtnCancel.Size = new System.Drawing.Size(130, 23);
			this.BtnCancel.TabIndex = 9;
			this.BtnCancel.Text = "Cancel";
			this.TtpMain.SetToolTip(this.BtnCancel, "Exit without saving changes");
			this.BtnCancel.UseVisualStyleBackColor = true;
			// 
			// BtnClear
			// 
			this.BtnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnClear.Location = new System.Drawing.Point(474, 158);
			this.BtnClear.Name = "BtnClear";
			this.BtnClear.Size = new System.Drawing.Size(130, 23);
			this.BtnClear.TabIndex = 6;
			this.BtnClear.Text = "&Clear (Alt+C)";
			this.TtpMain.SetToolTip(this.BtnClear, "Remove all shortcuts");
			this.BtnClear.UseVisualStyleBackColor = true;
			this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
			// 
			// BtnRemove
			// 
			this.BtnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnRemove.Enabled = false;
			this.BtnRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnRemove.Location = new System.Drawing.Point(474, 129);
			this.BtnRemove.Name = "BtnRemove";
			this.BtnRemove.Size = new System.Drawing.Size(130, 23);
			this.BtnRemove.TabIndex = 5;
			this.BtnRemove.Text = "Remove (Alt+Del)";
			this.TtpMain.SetToolTip(this.BtnRemove, "Delete the selected shortcut(s)");
			this.BtnRemove.UseVisualStyleBackColor = true;
			this.BtnRemove.Click += new System.EventHandler(this.BtnRemove_Click);
			// 
			// BtnEdit
			// 
			this.BtnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnEdit.Enabled = false;
			this.BtnEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnEdit.Location = new System.Drawing.Point(474, 42);
			this.BtnEdit.Name = "BtnEdit";
			this.BtnEdit.Size = new System.Drawing.Size(130, 23);
			this.BtnEdit.TabIndex = 2;
			this.BtnEdit.Text = "&Edit (Alt+E)";
			this.TtpMain.SetToolTip(this.BtnEdit, "Edit the currently selected shortcut(s)");
			this.BtnEdit.UseVisualStyleBackColor = true;
			this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
			// 
			// BtnAdd
			// 
			this.BtnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnAdd.AutoSize = true;
			this.BtnAdd.ContextMenuStrip = this.CmsSplit;
			this.BtnAdd.Location = new System.Drawing.Point(474, 13);
			this.BtnAdd.Name = "BtnAdd";
			this.BtnAdd.Size = new System.Drawing.Size(130, 23);
			this.BtnAdd.SplitMenuStrip = this.CmsSplit;
			this.BtnAdd.TabIndex = 10;
			this.BtnAdd.Text = "&Add (Alt+A)";
			this.TtpMain.SetToolTip(this.BtnAdd, "Adds new shortcut to this profile");
			this.BtnAdd.UseVisualStyleBackColor = true;
			this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
			// 
			// CmsSplit
			// 
			this.CmsSplit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CsiNew,
            this.CsiFromCopy});
			this.CmsSplit.Name = "CmsSplit";
			this.CmsSplit.Size = new System.Drawing.Size(223, 48);
			// 
			// CsiNew
			// 
			this.CsiNew.Name = "CsiNew";
			this.CsiNew.ShortcutKeyDisplayString = "Alt+A";
			this.CsiNew.Size = new System.Drawing.Size(222, 22);
			this.CsiNew.Text = "New shortcut             ";
			this.CsiNew.ToolTipText = "Adds new shortcut to this profile";
			this.CsiNew.Click += new System.EventHandler(this.BtnAdd_Click);
			// 
			// CsiFromCopy
			// 
			this.CsiFromCopy.Name = "CsiFromCopy";
			this.CsiFromCopy.Size = new System.Drawing.Size(222, 22);
			this.CsiFromCopy.Text = "Copy from selected";
			this.CsiFromCopy.ToolTipText = "Creates a shortcut based from the selected ones";
			this.CsiFromCopy.Click += new System.EventHandler(this.CsiFromCopy_Click);
			// 
			// CmsClipboard
			// 
			this.CmsClipboard.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CsiCopy,
            this.CsiCut,
            this.sep1,
            this.CsiPaste});
			this.CmsClipboard.Name = "CmsClipboard";
			this.CmsClipboard.Size = new System.Drawing.Size(145, 76);
			// 
			// CsiCopy
			// 
			this.CsiCopy.Enabled = false;
			this.CsiCopy.Image = global::WinFavs.Properties.Resources.Copy;
			this.CsiCopy.Name = "CsiCopy";
			this.CsiCopy.ShortcutKeyDisplayString = "Ctrl+C";
			this.CsiCopy.Size = new System.Drawing.Size(144, 22);
			this.CsiCopy.Text = "Copy";
			this.CsiCopy.Click += new System.EventHandler(this.CsiCopy_Click);
			// 
			// CsiCut
			// 
			this.CsiCut.Enabled = false;
			this.CsiCut.Image = global::WinFavs.Properties.Resources.Cut;
			this.CsiCut.Name = "CsiCut";
			this.CsiCut.ShortcutKeyDisplayString = "Ctrl+X";
			this.CsiCut.Size = new System.Drawing.Size(144, 22);
			this.CsiCut.Text = "Cut";
			this.CsiCut.Click += new System.EventHandler(this.CsiCut_Click);
			// 
			// sep1
			// 
			this.sep1.Name = "sep1";
			this.sep1.Size = new System.Drawing.Size(141, 6);
			// 
			// CsiPaste
			// 
			this.CsiPaste.Enabled = false;
			this.CsiPaste.Image = global::WinFavs.Properties.Resources.Paste;
			this.CsiPaste.Name = "CsiPaste";
			this.CsiPaste.ShortcutKeyDisplayString = "Ctrl+V";
			this.CsiPaste.Size = new System.Drawing.Size(144, 22);
			this.CsiPaste.Text = "Paste";
			this.CsiPaste.Click += new System.EventHandler(this.CsiPaste_Click);
			// 
			// BtnMoveDown
			// 
			this.BtnMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnMoveDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnMoveDown.Location = new System.Drawing.Point(474, 100);
			this.BtnMoveDown.Name = "BtnMoveDown";
			this.BtnMoveDown.Size = new System.Drawing.Size(130, 23);
			this.BtnMoveDown.TabIndex = 4;
			this.BtnMoveDown.Text = "Move down (Alt+Down)";
			this.BtnMoveDown.UseVisualStyleBackColor = true;
			this.BtnMoveDown.Click += new System.EventHandler(this.BtnMoveDown_Click);
			// 
			// BtnMoveUp
			// 
			this.BtnMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnMoveUp.Enabled = false;
			this.BtnMoveUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnMoveUp.Location = new System.Drawing.Point(474, 71);
			this.BtnMoveUp.Name = "BtnMoveUp";
			this.BtnMoveUp.Size = new System.Drawing.Size(130, 23);
			this.BtnMoveUp.TabIndex = 3;
			this.BtnMoveUp.Text = "Move up (Alt+Up)";
			this.BtnMoveUp.UseVisualStyleBackColor = true;
			this.BtnMoveUp.Click += new System.EventHandler(this.BtnMoveUp_Click);
			// 
			// TxtName
			// 
			this.TxtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.TxtName.BackColor = System.Drawing.SystemColors.Window;
			this.TxtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.TxtName.CueText = "Enter profile name";
			this.TxtName.FixedLength = 0;
			this.TxtName.ForeColor = System.Drawing.Color.Black;
			this.TxtName.Location = new System.Drawing.Point(474, 259);
			this.TxtName.Name = "TxtName";
			this.TxtName.Size = new System.Drawing.Size(130, 20);
			this.TxtName.TabIndex = 7;
			this.TxtName.ValidationMethod = WinFavs.Controls.RuleBox.Validation.FileName;
			this.TxtName.TextChangedDelayed += new System.EventHandler(this.TxtName_TextChangedDelayed);
			this.TxtName.TextChanged += new System.EventHandler(this.TxtName_TextChanged);
			// 
			// FrmProfile
			// 
			this.AcceptButton = this.BtnSave;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.CancelButton = this.BtnCancel;
			this.ClientSize = new System.Drawing.Size(610, 346);
			this.Controls.Add(this.BtnAdd);
			this.Controls.Add(this.LblDrop);
			this.Controls.Add(this.LblName);
			this.Controls.Add(this.TxtName);
			this.Controls.Add(this.BtnCancel);
			this.Controls.Add(this.BtnSave);
			this.Controls.Add(this.BtnMoveDown);
			this.Controls.Add(this.BtnClear);
			this.Controls.Add(this.BtnMoveUp);
			this.Controls.Add(this.BtnRemove);
			this.Controls.Add(this.BtnEdit);
			this.Controls.Add(this.LsvProfile);
			this.ForeColor = System.Drawing.Color.Black;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(430, 297);
			this.Name = "FrmProfile";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Edit profile";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEdit_FormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmProfile_FormClosed);
			this.Shown += new System.EventHandler(this.FrmProfile_Shown);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmProfile_KeyDown);
			this.CmsSplit.ResumeLayout(false);
			this.CmsClipboard.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button BtnEdit;
		private System.Windows.Forms.Button BtnRemove;
		private System.Windows.Forms.Button BtnClear;
		private System.Windows.Forms.Button BtnSave;
		private System.Windows.Forms.Button BtnCancel;
		private RuleBox TxtName;
		private System.Windows.Forms.Label LblName;
		private System.Windows.Forms.ImageList ImgLarge;
		private System.Windows.Forms.Timer TmrCheckEnable;
		private System.Windows.Forms.Label LblDrop;
		private System.Windows.Forms.ToolTip TtpMain;
		private System.Windows.Forms.Button BtnMoveUp;
		private System.Windows.Forms.Button BtnMoveDown;
		private System.Windows.Forms.ImageList ImgSmall;
		private System.Windows.Forms.ColumnHeader HdrName;
		private System.Windows.Forms.ColumnHeader HdrPath;
		private System.Windows.Forms.ListView LsvProfile;
		private System.Windows.Forms.ContextMenuStrip CmsClipboard;
		private System.Windows.Forms.ToolStripMenuItem CsiCopy;
		private System.Windows.Forms.ToolStripMenuItem CsiCut;
		private System.Windows.Forms.ToolStripMenuItem CsiPaste;
		private Sep sep1;
		private SplitButton BtnAdd;
		private System.Windows.Forms.ContextMenuStrip CmsSplit;
		private System.Windows.Forms.ToolStripMenuItem CsiNew;
		private System.Windows.Forms.ToolStripMenuItem CsiFromCopy;
	}
}