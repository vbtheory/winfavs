﻿namespace WinFavs
{
	partial class FrmAbout
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.LblWinFavs = new System.Windows.Forms.Label();
			this.LblQuote = new System.Windows.Forms.Label();
			this.PbxLogo = new System.Windows.Forms.PictureBox();
			this.LblVersion = new System.Windows.Forms.Label();
			this.PbxSnake = new System.Windows.Forms.PictureBox();
			this.LblScore = new System.Windows.Forms.Label();
			this.LblVBTheory = new System.Windows.Forms.Label();
			this.PbxVBTheory = new System.Windows.Forms.PictureBox();
			this.BtnEmail = new System.Windows.Forms.Button();
			this.BtnTwitter = new System.Windows.Forms.Button();
			this.BtnYoutube = new System.Windows.Forms.Button();
			this.BtnFacebook = new System.Windows.Forms.Button();
			this.LblNotice = new System.Windows.Forms.Label();
			this.PnlScroll = new System.Windows.Forms.Panel();
			this.TmrClick = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.PbxLogo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PbxSnake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PbxVBTheory)).BeginInit();
			this.PnlScroll.SuspendLayout();
			this.SuspendLayout();
			// 
			// LblWinFavs
			// 
			this.LblWinFavs.AutoSize = true;
			this.LblWinFavs.BackColor = System.Drawing.Color.Transparent;
			this.LblWinFavs.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblWinFavs.Location = new System.Drawing.Point(67, 9);
			this.LblWinFavs.Name = "LblWinFavs";
			this.LblWinFavs.Size = new System.Drawing.Size(103, 32);
			this.LblWinFavs.TabIndex = 0;
			this.LblWinFavs.Text = "WinFavs";
			// 
			// LblQuote
			// 
			this.LblQuote.AutoSize = true;
			this.LblQuote.BackColor = System.Drawing.Color.Transparent;
			this.LblQuote.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblQuote.Location = new System.Drawing.Point(69, 41);
			this.LblQuote.Name = "LblQuote";
			this.LblQuote.Size = new System.Drawing.Size(187, 13);
			this.LblQuote.TabIndex = 0;
			this.LblQuote.Text = "Easily manage your Windows favorites";
			this.LblQuote.Click += new System.EventHandler(this.LblQuote_Click);
			// 
			// PbxLogo
			// 
			this.PbxLogo.Image = global::WinFavs.Properties.Resources.WinFavs;
			this.PbxLogo.Location = new System.Drawing.Point(13, 9);
			this.PbxLogo.Name = "PbxLogo";
			this.PbxLogo.Size = new System.Drawing.Size(48, 48);
			this.PbxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.PbxLogo.TabIndex = 1;
			this.PbxLogo.TabStop = false;
			this.PbxLogo.Click += new System.EventHandler(this.PbxLogo_Click);
			// 
			// LblVersion
			// 
			this.LblVersion.BackColor = System.Drawing.Color.Transparent;
			this.LblVersion.ForeColor = System.Drawing.Color.OrangeRed;
			this.LblVersion.Location = new System.Drawing.Point(217, 9);
			this.LblVersion.Name = "LblVersion";
			this.LblVersion.Size = new System.Drawing.Size(144, 13);
			this.LblVersion.TabIndex = 2;
			this.LblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// PbxSnake
			// 
			this.PbxSnake.BackColor = System.Drawing.SystemColors.Window;
			this.PbxSnake.Location = new System.Drawing.Point(61, 14);
			this.PbxSnake.Name = "PbxSnake";
			this.PbxSnake.Size = new System.Drawing.Size(250, 250);
			this.PbxSnake.TabIndex = 3;
			this.PbxSnake.TabStop = false;
			this.PbxSnake.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlSnake_Paint);
			// 
			// LblScore
			// 
			this.LblScore.AutoSize = true;
			this.LblScore.Location = new System.Drawing.Point(376, -4);
			this.LblScore.Name = "LblScore";
			this.LblScore.Size = new System.Drawing.Size(47, 13);
			this.LblScore.TabIndex = 4;
			this.LblScore.Text = "Score: 0";
			this.LblScore.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// LblVBTheory
			// 
			this.LblVBTheory.AutoSize = true;
			this.LblVBTheory.Location = new System.Drawing.Point(12, 114);
			this.LblVBTheory.Name = "LblVBTheory";
			this.LblVBTheory.Size = new System.Drawing.Size(325, 13);
			this.LblVBTheory.TabIndex = 6;
			this.LblVBTheory.Text = "Copyright 2011-2014 VBTheory Programs™. All rights reserved.";
			// 
			// PbxVBTheory
			// 
			this.PbxVBTheory.Image = global::WinFavs.Properties.Resources.VBTheory;
			this.PbxVBTheory.Location = new System.Drawing.Point(149, 134);
			this.PbxVBTheory.Name = "PbxVBTheory";
			this.PbxVBTheory.Size = new System.Drawing.Size(50, 50);
			this.PbxVBTheory.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.PbxVBTheory.TabIndex = 5;
			this.PbxVBTheory.TabStop = false;
			// 
			// BtnEmail
			// 
			this.BtnEmail.BackgroundImage = global::WinFavs.Properties.Resources.Email;
			this.BtnEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.BtnEmail.Location = new System.Drawing.Point(264, 72);
			this.BtnEmail.Name = "BtnEmail";
			this.BtnEmail.Size = new System.Drawing.Size(32, 32);
			this.BtnEmail.TabIndex = 4;
			this.BtnEmail.UseVisualStyleBackColor = true;
			this.BtnEmail.Click += new System.EventHandler(this.BtnEmail_Click);
			// 
			// BtnTwitter
			// 
			this.BtnTwitter.BackgroundImage = global::WinFavs.Properties.Resources.Twitter;
			this.BtnTwitter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.BtnTwitter.Location = new System.Drawing.Point(194, 72);
			this.BtnTwitter.Name = "BtnTwitter";
			this.BtnTwitter.Size = new System.Drawing.Size(32, 32);
			this.BtnTwitter.TabIndex = 2;
			this.BtnTwitter.UseVisualStyleBackColor = true;
			this.BtnTwitter.Click += new System.EventHandler(this.BtnTwitter_Click);
			// 
			// BtnYoutube
			// 
			this.BtnYoutube.BackgroundImage = global::WinFavs.Properties.Resources.YouTube;
			this.BtnYoutube.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.BtnYoutube.Location = new System.Drawing.Point(124, 72);
			this.BtnYoutube.Name = "BtnYoutube";
			this.BtnYoutube.Size = new System.Drawing.Size(32, 32);
			this.BtnYoutube.TabIndex = 1;
			this.BtnYoutube.UseVisualStyleBackColor = true;
			this.BtnYoutube.Click += new System.EventHandler(this.BtnYoutube_Click);
			// 
			// BtnFacebook
			// 
			this.BtnFacebook.BackgroundImage = global::WinFavs.Properties.Resources.Facebook;
			this.BtnFacebook.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.BtnFacebook.Location = new System.Drawing.Point(54, 72);
			this.BtnFacebook.Name = "BtnFacebook";
			this.BtnFacebook.Size = new System.Drawing.Size(32, 32);
			this.BtnFacebook.TabIndex = 0;
			this.BtnFacebook.UseVisualStyleBackColor = true;
			this.BtnFacebook.Click += new System.EventHandler(this.BtnFacebook_Click);
			// 
			// LblNotice
			// 
			this.LblNotice.AutoSize = true;
			this.LblNotice.Location = new System.Drawing.Point(51, 0);
			this.LblNotice.Name = "LblNotice";
			this.LblNotice.Size = new System.Drawing.Size(247, 65);
			this.LblNotice.TabIndex = 0;
			this.LblNotice.Text = "Thank you for using WinFavs!\r\n\r\nBy Taha Joudary AKA VBTheory\r\n\r\nSpecial thanks to" +
    ": Richard (dot-net-transitions)";
			this.LblNotice.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// PnlScroll
			// 
			this.PnlScroll.Controls.Add(this.LblVBTheory);
			this.PnlScroll.Controls.Add(this.PbxVBTheory);
			this.PnlScroll.Controls.Add(this.BtnEmail);
			this.PnlScroll.Controls.Add(this.BtnTwitter);
			this.PnlScroll.Controls.Add(this.BtnYoutube);
			this.PnlScroll.Controls.Add(this.BtnFacebook);
			this.PnlScroll.Controls.Add(this.LblNotice);
			this.PnlScroll.Location = new System.Drawing.Point(12, 76);
			this.PnlScroll.Name = "PnlScroll";
			this.PnlScroll.Size = new System.Drawing.Size(348, 191);
			this.PnlScroll.TabIndex = 1;
			// 
			// TmrClick
			// 
			this.TmrClick.Interval = 10000;
			this.TmrClick.Tick += new System.EventHandler(this.TmrClick_Tick);
			// 
			// FrmAbout
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(373, 279);
			this.Controls.Add(this.PnlScroll);
			this.Controls.Add(this.LblVersion);
			this.Controls.Add(this.PbxLogo);
			this.Controls.Add(this.LblQuote);
			this.Controls.Add(this.LblWinFavs);
			this.Controls.Add(this.LblScore);
			this.Controls.Add(this.PbxSnake);
			this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.Black;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FrmAbout";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "About";
			this.Deactivate += new System.EventHandler(this.FrmAbout_Deactivate);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmAbout_FormClosed);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmAbout_KeyDown);
			((System.ComponentModel.ISupportInitialize)(this.PbxLogo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PbxSnake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PbxVBTheory)).EndInit();
			this.PnlScroll.ResumeLayout(false);
			this.PnlScroll.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LblWinFavs;
		private System.Windows.Forms.Label LblQuote;
		private System.Windows.Forms.PictureBox PbxLogo;
		private System.Windows.Forms.Label LblVersion;
		private System.Windows.Forms.PictureBox PbxSnake;
		private System.Windows.Forms.Label LblScore;
		private System.Windows.Forms.Button BtnEmail;
		private System.Windows.Forms.Button BtnTwitter;
		private System.Windows.Forms.Button BtnYoutube;
		private System.Windows.Forms.Button BtnFacebook;
		private System.Windows.Forms.Label LblNotice;
		private System.Windows.Forms.PictureBox PbxVBTheory;
		private System.Windows.Forms.Label LblVBTheory;
		private System.Windows.Forms.Panel PnlScroll;
		private System.Windows.Forms.Timer TmrClick;

	}
}