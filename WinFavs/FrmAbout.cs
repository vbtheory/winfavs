﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Transitions;
using WinFavs.Classes;

namespace WinFavs
{
	public partial class FrmAbout : Form
	{

		private SnakeGame snakeGame;

		public FrmAbout()
		{
			InitializeComponent();

			PbxSnake.Left = 17;
			LblVersion.Text = "V." + new Vers(Application.ProductVersion);
		}

		private void PbxLogo_Click(object sender, EventArgs e)
		{
			Logger.Log("Found about easter egg :)");
			LblScore.Location = new Point(PbxSnake.Left, PbxSnake.Bottom - LblScore.Height);

			Transition t = new Transition(new TransitionType_EaseInEaseOut(500));
			t.add(PbxLogo, "Top", -PbxLogo.Height);
			t.add(LblWinFavs, "Top", -LblWinFavs.Height);
			t.add(LblQuote, "Top", -LblQuote.Height);
			t.add(LblVersion, "Left", Width);
			t.add(PnlScroll, "Top", Bottom);
			t.add(this, "Width", 300);
			t.add(this, "Left", Left + 25);
			t.add(PbxSnake, "BackColor", Color.FromArgb(230, 230, 230));
			t.add(LblScore, "Top", PbxSnake.Bottom);
			t.TransitionCompletedEvent += (o, args) => snakeGame = new SnakeGame(PbxSnake, this, LblScore);
			t.run();
			Notifier.Show("Share this easter egg with your friends :)");
		}

		private void PnlSnake_Paint(object sender, PaintEventArgs e)
		{
			if (snakeGame != null) snakeGame.Paint(e.Graphics);
		}

		private void FrmAbout_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				Close();
				return;
			}

			if(snakeGame==null) return;

			if (e.KeyCode == Keys.Space)
				snakeGame.PauseGame();

			snakeGame.SendKey(e.KeyCode);
		}

		private void BtnFacebook_Click(object sender, EventArgs e)
		{
			Process.Start("http://www.facebook.com/VBTheory");
		}

		private void BtnYoutube_Click(object sender, EventArgs e)
		{
			Process.Start("http://www.youtube.com/VBTheory");
		}

		private void BtnTwitter_Click(object sender, EventArgs e)
		{
			Process.Start("http://www.twitter.com/VBTheory");
		}

		private void BtnEmail_Click(object sender, EventArgs e)
		{
			Process.Start("mailto:ImAVBG@gmail.com");
		}

		private void FrmAbout_Deactivate(object sender, EventArgs e)
		{
			if(snakeGame!=null) snakeGame.PauseGame();
		}

		private void LblQuote_Click(object sender, EventArgs e)
		{
			Alert.Show(!TmrClick.Enabled
				? "Don't click here again, you won't find anything if you do."
				: "YOU. HAVE. FAILED. ME. No cookies for you!");
			
			TmrClick.Enabled = !TmrClick.Enabled;
		}

		private void TmrClick_Tick(object sender, EventArgs e)
		{
			TmrClick.Stop();
			Alert.Show(
				"Congratulations, you have won NOTHING; COMPLETE NOTHING. Your prize will be shipped to you soon, no need to input your address or anything.");
		}

		private void FrmAbout_FormClosed(object sender, FormClosedEventArgs e)
		{
			if(snakeGame != null) snakeGame.Dispose();
			Dispose(true);
		}
		
	}
}