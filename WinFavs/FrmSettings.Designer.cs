﻿namespace WinFavs
{
	partial class FrmSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.TbcMain = new System.Windows.Forms.TabControl();
			this.TpgGeneral = new System.Windows.Forms.TabPage();
			this.BtnReset = new System.Windows.Forms.Button();
			this.ChbAutoUpdate = new System.Windows.Forms.CheckBox();
			this.ChbConservative = new System.Windows.Forms.CheckBox();
			this.BtnUpdate = new System.Windows.Forms.Button();
			this.ChbMinimizeToTray = new System.Windows.Forms.CheckBox();
			this.ChbClosingConfirmation = new System.Windows.Forms.CheckBox();
			this.GpbAssociation = new System.Windows.Forms.GroupBox();
			this.LblStatShell = new System.Windows.Forms.Label();
			this.BtnAssociate = new System.Windows.Forms.Button();
			this.BtnDissociate = new System.Windows.Forms.Button();
			this.GpbStartup = new System.Windows.Forms.GroupBox();
			this.LblStateStart = new System.Windows.Forms.Label();
			this.ChbStartMinimized = new System.Windows.Forms.CheckBox();
			this.BtnRemoveStart = new System.Windows.Forms.Button();
			this.BtnAddStart = new System.Windows.Forms.Button();
			this.TpgView = new System.Windows.Forms.TabPage();
			this.GpbDialogs = new System.Windows.Forms.GroupBox();
			this.ChbTargetNotExist = new System.Windows.Forms.CheckBox();
			this.ChbNoShortcuts = new System.Windows.Forms.CheckBox();
			this.ChbHints = new System.Windows.Forms.CheckBox();
			this.PbxView = new System.Windows.Forms.PictureBox();
			this.CbxProfileView = new System.Windows.Forms.ComboBox();
			this.LblProfileView = new System.Windows.Forms.Label();
			this.TpgProfileTasks = new System.Windows.Forms.TabPage();
			this.GpbTasks = new System.Windows.Forms.GroupBox();
			this.ChbTaskConfirm = new System.Windows.Forms.CheckBox();
			this.ChbTasks = new System.Windows.Forms.CheckBox();
			this.GpbProfiles = new System.Windows.Forms.GroupBox();
			this.ChbOpenExplorer = new System.Windows.Forms.CheckBox();
			this.ChbBackup = new System.Windows.Forms.CheckBox();
			this.ChbTooManyToApply = new System.Windows.Forms.CheckBox();
			this.ChbRefresh = new System.Windows.Forms.CheckBox();
			this.TpgMisc = new System.Windows.Forms.TabPage();
			this.ChbHelpImprove = new System.Windows.Forms.CheckBox();
			this.ChbPop = new System.Windows.Forms.CheckBox();
			this.ChbFeedback = new System.Windows.Forms.CheckBox();
			this.ImgMain = new System.Windows.Forms.ImageList(this.components);
			this.BtnClose = new System.Windows.Forms.Button();
			this.TtpMain = new System.Windows.Forms.ToolTip(this.components);
			this.TbcMain.SuspendLayout();
			this.TpgGeneral.SuspendLayout();
			this.GpbAssociation.SuspendLayout();
			this.GpbStartup.SuspendLayout();
			this.TpgView.SuspendLayout();
			this.GpbDialogs.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PbxView)).BeginInit();
			this.TpgProfileTasks.SuspendLayout();
			this.GpbTasks.SuspendLayout();
			this.GpbProfiles.SuspendLayout();
			this.TpgMisc.SuspendLayout();
			this.SuspendLayout();
			// 
			// TbcMain
			// 
			this.TbcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TbcMain.Controls.Add(this.TpgGeneral);
			this.TbcMain.Controls.Add(this.TpgView);
			this.TbcMain.Controls.Add(this.TpgProfileTasks);
			this.TbcMain.Controls.Add(this.TpgMisc);
			this.TbcMain.HotTrack = true;
			this.TbcMain.ImageList = this.ImgMain;
			this.TbcMain.Location = new System.Drawing.Point(9, 9);
			this.TbcMain.Name = "TbcMain";
			this.TbcMain.SelectedIndex = 0;
			this.TbcMain.Size = new System.Drawing.Size(409, 338);
			this.TbcMain.TabIndex = 0;
			this.TbcMain.SelectedIndexChanged += new System.EventHandler(this.TbcMain_SelectedIndexChanged);
			// 
			// TpgGeneral
			// 
			this.TpgGeneral.Controls.Add(this.BtnReset);
			this.TpgGeneral.Controls.Add(this.ChbAutoUpdate);
			this.TpgGeneral.Controls.Add(this.ChbConservative);
			this.TpgGeneral.Controls.Add(this.BtnUpdate);
			this.TpgGeneral.Controls.Add(this.ChbMinimizeToTray);
			this.TpgGeneral.Controls.Add(this.ChbClosingConfirmation);
			this.TpgGeneral.Controls.Add(this.GpbAssociation);
			this.TpgGeneral.Controls.Add(this.GpbStartup);
			this.TpgGeneral.Location = new System.Drawing.Point(4, 23);
			this.TpgGeneral.Name = "TpgGeneral";
			this.TpgGeneral.Padding = new System.Windows.Forms.Padding(3);
			this.TpgGeneral.Size = new System.Drawing.Size(401, 311);
			this.TpgGeneral.TabIndex = 0;
			this.TpgGeneral.Text = "General   ";
			this.TpgGeneral.UseVisualStyleBackColor = true;
			// 
			// BtnReset
			// 
			this.BtnReset.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnReset.Location = new System.Drawing.Point(197, 278);
			this.BtnReset.Name = "BtnReset";
			this.BtnReset.Size = new System.Drawing.Size(195, 23);
			this.BtnReset.TabIndex = 7;
			this.BtnReset.Text = "Reset ignored updates";
			this.TtpMain.SetToolTip(this.BtnReset, "If you have previously chosen to ignore an update, you can use this button to re-" +
        "enable it");
			this.BtnReset.UseVisualStyleBackColor = true;
			this.BtnReset.Click += new System.EventHandler(this.BtnReset_Click);
			// 
			// ChbAutoUpdate
			// 
			this.ChbAutoUpdate.AutoSize = true;
			this.ChbAutoUpdate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbAutoUpdate.Location = new System.Drawing.Point(7, 253);
			this.ChbAutoUpdate.Name = "ChbAutoUpdate";
			this.ChbAutoUpdate.Size = new System.Drawing.Size(183, 18);
			this.ChbAutoUpdate.TabIndex = 5;
			this.ChbAutoUpdate.Text = "Check for updates automatically";
			this.TtpMain.SetToolTip(this.ChbAutoUpdate, "If this is checked, WinFavs will search automatically for updates");
			this.ChbAutoUpdate.UseVisualStyleBackColor = true;
			// 
			// ChbConservative
			// 
			this.ChbConservative.AutoSize = true;
			this.ChbConservative.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbConservative.Location = new System.Drawing.Point(7, 230);
			this.ChbConservative.Name = "ChbConservative";
			this.ChbConservative.Size = new System.Drawing.Size(123, 18);
			this.ChbConservative.TabIndex = 4;
			this.ChbConservative.Text = "Conservative mode";
			this.TtpMain.SetToolTip(this.ChbConservative, "This mode enables faster profile loading by loading the default folder icon for a" +
        "ll shortcuts");
			this.ChbConservative.UseVisualStyleBackColor = true;
			this.ChbConservative.CheckedChanged += new System.EventHandler(this.ChbConservative_CheckedChanged);
			// 
			// BtnUpdate
			// 
			this.BtnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnUpdate.Location = new System.Drawing.Point(7, 278);
			this.BtnUpdate.Name = "BtnUpdate";
			this.BtnUpdate.Size = new System.Drawing.Size(184, 23);
			this.BtnUpdate.TabIndex = 6;
			this.BtnUpdate.Text = "Check for updates";
			this.TtpMain.SetToolTip(this.BtnUpdate, "Checks if there any updates available for the software");
			this.BtnUpdate.UseVisualStyleBackColor = true;
			this.BtnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
			// 
			// ChbMinimizeToTray
			// 
			this.ChbMinimizeToTray.AutoSize = true;
			this.ChbMinimizeToTray.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbMinimizeToTray.Location = new System.Drawing.Point(7, 207);
			this.ChbMinimizeToTray.Name = "ChbMinimizeToTray";
			this.ChbMinimizeToTray.Size = new System.Drawing.Size(104, 18);
			this.ChbMinimizeToTray.TabIndex = 3;
			this.ChbMinimizeToTray.Text = "Minimize to tray";
			this.TtpMain.SetToolTip(this.ChbMinimizeToTray, "If checked, minimizing the app will hide it to the notification tray");
			this.ChbMinimizeToTray.UseVisualStyleBackColor = true;
			// 
			// ChbClosingConfirmation
			// 
			this.ChbClosingConfirmation.AutoSize = true;
			this.ChbClosingConfirmation.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbClosingConfirmation.Location = new System.Drawing.Point(7, 184);
			this.ChbClosingConfirmation.Name = "ChbClosingConfirmation";
			this.ChbClosingConfirmation.Size = new System.Drawing.Size(155, 18);
			this.ChbClosingConfirmation.TabIndex = 2;
			this.ChbClosingConfirmation.Text = "Show closing confirmation";
			this.TtpMain.SetToolTip(this.ChbClosingConfirmation, "If checked and you have any enabled tasks, the app will ask for confirmation befo" +
        "re exiting");
			this.ChbClosingConfirmation.UseVisualStyleBackColor = true;
			// 
			// GpbAssociation
			// 
			this.GpbAssociation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GpbAssociation.Controls.Add(this.LblStatShell);
			this.GpbAssociation.Controls.Add(this.BtnAssociate);
			this.GpbAssociation.Controls.Add(this.BtnDissociate);
			this.GpbAssociation.Location = new System.Drawing.Point(7, 107);
			this.GpbAssociation.Name = "GpbAssociation";
			this.GpbAssociation.Size = new System.Drawing.Size(388, 68);
			this.GpbAssociation.TabIndex = 1;
			this.GpbAssociation.TabStop = false;
			this.GpbAssociation.Text = "File association";
			// 
			// LblStatShell
			// 
			this.LblStatShell.AutoSize = true;
			this.LblStatShell.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblStatShell.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.LblStatShell.Location = new System.Drawing.Point(6, 18);
			this.LblStatShell.Name = "LblStatShell";
			this.LblStatShell.Size = new System.Drawing.Size(35, 13);
			this.LblStatShell.TabIndex = 2;
			this.LblStatShell.Text = "State:";
			this.TtpMain.SetToolTip(this.LblStatShell, "State:");
			// 
			// BtnAssociate
			// 
			this.BtnAssociate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnAssociate.Location = new System.Drawing.Point(6, 39);
			this.BtnAssociate.Name = "BtnAssociate";
			this.BtnAssociate.Size = new System.Drawing.Size(184, 23);
			this.BtnAssociate.TabIndex = 0;
			this.BtnAssociate.Text = "Associate with WinFavs profiles";
			this.TtpMain.SetToolTip(this.BtnAssociate, "Use this button if you want to add an icon to WinFavs profiles and add context me" +
        "nu actions to them");
			this.BtnAssociate.UseVisualStyleBackColor = true;
			this.BtnAssociate.Click += new System.EventHandler(this.BtnAssociate_Click);
			// 
			// BtnDissociate
			// 
			this.BtnDissociate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnDissociate.Location = new System.Drawing.Point(196, 39);
			this.BtnDissociate.Name = "BtnDissociate";
			this.BtnDissociate.Size = new System.Drawing.Size(143, 23);
			this.BtnDissociate.TabIndex = 1;
			this.BtnDissociate.Text = "Remove association";
			this.TtpMain.SetToolTip(this.BtnDissociate, "Use this if you don\'t want WinFavs associated with profiles");
			this.BtnDissociate.UseVisualStyleBackColor = true;
			this.BtnDissociate.Click += new System.EventHandler(this.BtnDissociate_Click);
			// 
			// GpbStartup
			// 
			this.GpbStartup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GpbStartup.Controls.Add(this.LblStateStart);
			this.GpbStartup.Controls.Add(this.ChbStartMinimized);
			this.GpbStartup.Controls.Add(this.BtnRemoveStart);
			this.GpbStartup.Controls.Add(this.BtnAddStart);
			this.GpbStartup.Location = new System.Drawing.Point(7, 7);
			this.GpbStartup.Name = "GpbStartup";
			this.GpbStartup.Size = new System.Drawing.Size(388, 91);
			this.GpbStartup.TabIndex = 0;
			this.GpbStartup.TabStop = false;
			this.GpbStartup.Text = "Windows startup";
			// 
			// LblStateStart
			// 
			this.LblStateStart.AutoSize = true;
			this.LblStateStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblStateStart.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.LblStateStart.Location = new System.Drawing.Point(6, 19);
			this.LblStateStart.Name = "LblStateStart";
			this.LblStateStart.Size = new System.Drawing.Size(38, 13);
			this.LblStateStart.TabIndex = 2;
			this.LblStateStart.Text = "State: ";
			this.TtpMain.SetToolTip(this.LblStateStart, "State:");
			// 
			// ChbStartMinimized
			// 
			this.ChbStartMinimized.AutoSize = true;
			this.ChbStartMinimized.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbStartMinimized.Location = new System.Drawing.Point(6, 69);
			this.ChbStartMinimized.Name = "ChbStartMinimized";
			this.ChbStartMinimized.Size = new System.Drawing.Size(102, 18);
			this.ChbStartMinimized.TabIndex = 2;
			this.ChbStartMinimized.Text = "Start minimized";
			this.TtpMain.SetToolTip(this.ChbStartMinimized, "If checked, WinFavs will be minimized when it starts with Windows");
			this.ChbStartMinimized.UseVisualStyleBackColor = true;
			// 
			// BtnRemoveStart
			// 
			this.BtnRemoveStart.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnRemoveStart.Location = new System.Drawing.Point(196, 38);
			this.BtnRemoveStart.Name = "BtnRemoveStart";
			this.BtnRemoveStart.Size = new System.Drawing.Size(143, 23);
			this.BtnRemoveStart.TabIndex = 1;
			this.BtnRemoveStart.Text = "Remove from start";
			this.TtpMain.SetToolTip(this.BtnRemoveStart, "Use this button if you don\'t want WinFavs to start with Windows anymore");
			this.BtnRemoveStart.UseVisualStyleBackColor = true;
			this.BtnRemoveStart.Click += new System.EventHandler(this.BtnRemoveStart_Click);
			// 
			// BtnAddStart
			// 
			this.BtnAddStart.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnAddStart.Location = new System.Drawing.Point(6, 38);
			this.BtnAddStart.Name = "BtnAddStart";
			this.BtnAddStart.Size = new System.Drawing.Size(184, 23);
			this.BtnAddStart.TabIndex = 0;
			this.BtnAddStart.Text = "Add to start";
			this.TtpMain.SetToolTip(this.BtnAddStart, "If you use this button, WinFavs will start automatically when Windows starts up");
			this.BtnAddStart.UseVisualStyleBackColor = true;
			this.BtnAddStart.Click += new System.EventHandler(this.BtnAddStart_Click);
			// 
			// TpgView
			// 
			this.TpgView.Controls.Add(this.GpbDialogs);
			this.TpgView.Controls.Add(this.PbxView);
			this.TpgView.Controls.Add(this.CbxProfileView);
			this.TpgView.Controls.Add(this.LblProfileView);
			this.TpgView.Location = new System.Drawing.Point(4, 23);
			this.TpgView.Name = "TpgView";
			this.TpgView.Size = new System.Drawing.Size(401, 311);
			this.TpgView.TabIndex = 1;
			this.TpgView.Text = "View   ";
			this.TpgView.UseVisualStyleBackColor = true;
			// 
			// GpbDialogs
			// 
			this.GpbDialogs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GpbDialogs.Controls.Add(this.ChbTargetNotExist);
			this.GpbDialogs.Controls.Add(this.ChbNoShortcuts);
			this.GpbDialogs.Controls.Add(this.ChbHints);
			this.GpbDialogs.Location = new System.Drawing.Point(7, 7);
			this.GpbDialogs.Name = "GpbDialogs";
			this.GpbDialogs.Size = new System.Drawing.Size(388, 89);
			this.GpbDialogs.TabIndex = 0;
			this.GpbDialogs.TabStop = false;
			this.GpbDialogs.Text = "Dialogs";
			// 
			// ChbTargetNotExist
			// 
			this.ChbTargetNotExist.AutoSize = true;
			this.ChbTargetNotExist.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbTargetNotExist.Location = new System.Drawing.Point(7, 66);
			this.ChbTargetNotExist.Name = "ChbTargetNotExist";
			this.ChbTargetNotExist.Size = new System.Drawing.Size(240, 18);
			this.ChbTargetNotExist.TabIndex = 2;
			this.ChbTargetNotExist.Text = "Warn me if the shortcut\'s target doesn\'t exist";
			this.TtpMain.SetToolTip(this.ChbTargetNotExist, "If checked, the app will warn you if the target of the shortcut you chose doesn\'t" +
        " exist");
			this.ChbTargetNotExist.UseVisualStyleBackColor = true;
			// 
			// ChbNoShortcuts
			// 
			this.ChbNoShortcuts.AutoSize = true;
			this.ChbNoShortcuts.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbNoShortcuts.Location = new System.Drawing.Point(7, 43);
			this.ChbNoShortcuts.Name = "ChbNoShortcuts";
			this.ChbNoShortcuts.Size = new System.Drawing.Size(262, 18);
			this.ChbNoShortcuts.TabIndex = 1;
			this.ChbNoShortcuts.Text = "Warn me if the profile doesn\'t have any shortcuts";
			this.TtpMain.SetToolTip(this.ChbNoShortcuts, "If checked, the app will warn you if the profile you are editing/creating doesn\'t" +
        " contain any shortcuts");
			this.ChbNoShortcuts.UseVisualStyleBackColor = true;
			// 
			// ChbHints
			// 
			this.ChbHints.AutoSize = true;
			this.ChbHints.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbHints.Location = new System.Drawing.Point(7, 20);
			this.ChbHints.Name = "ChbHints";
			this.ChbHints.Size = new System.Drawing.Size(90, 18);
			this.ChbHints.TabIndex = 0;
			this.ChbHints.Text = "Enable hints";
			this.TtpMain.SetToolTip(this.ChbHints, "If checked, WinFavs will show occasional tips to guide you through the app");
			this.ChbHints.UseVisualStyleBackColor = true;
			// 
			// PbxView
			// 
			this.PbxView.Location = new System.Drawing.Point(323, 101);
			this.PbxView.Name = "PbxView";
			this.PbxView.Size = new System.Drawing.Size(25, 25);
			this.PbxView.TabIndex = 2;
			this.PbxView.TabStop = false;
			// 
			// CbxProfileView
			// 
			this.CbxProfileView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbxProfileView.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.CbxProfileView.FormattingEnabled = true;
			this.CbxProfileView.Items.AddRange(new object[] {
            "Large icons",
            "Details",
            "Small icons",
            "List",
            "Tile"});
			this.CbxProfileView.Location = new System.Drawing.Point(103, 103);
			this.CbxProfileView.Name = "CbxProfileView";
			this.CbxProfileView.Size = new System.Drawing.Size(214, 21);
			this.CbxProfileView.TabIndex = 1;
			this.TtpMain.SetToolTip(this.CbxProfileView, "This alters the arrangement of profiles in the app");
			this.CbxProfileView.SelectedIndexChanged += new System.EventHandler(this.CbxProfileView_SelectedIndexChanged);
			// 
			// LblProfileView
			// 
			this.LblProfileView.AutoSize = true;
			this.LblProfileView.Location = new System.Drawing.Point(4, 106);
			this.LblProfileView.Name = "LblProfileView";
			this.LblProfileView.Size = new System.Drawing.Size(93, 13);
			this.LblProfileView.TabIndex = 0;
			this.LblProfileView.Text = "Profile view mode:";
			// 
			// TpgProfileTasks
			// 
			this.TpgProfileTasks.Controls.Add(this.GpbTasks);
			this.TpgProfileTasks.Controls.Add(this.GpbProfiles);
			this.TpgProfileTasks.Controls.Add(this.ChbRefresh);
			this.TpgProfileTasks.Location = new System.Drawing.Point(4, 23);
			this.TpgProfileTasks.Name = "TpgProfileTasks";
			this.TpgProfileTasks.Size = new System.Drawing.Size(401, 311);
			this.TpgProfileTasks.TabIndex = 2;
			this.TpgProfileTasks.Text = "Profiles & tasks   ";
			this.TpgProfileTasks.UseVisualStyleBackColor = true;
			// 
			// GpbTasks
			// 
			this.GpbTasks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GpbTasks.Controls.Add(this.ChbTaskConfirm);
			this.GpbTasks.Controls.Add(this.ChbTasks);
			this.GpbTasks.Location = new System.Drawing.Point(7, 107);
			this.GpbTasks.Name = "GpbTasks";
			this.GpbTasks.Size = new System.Drawing.Size(388, 70);
			this.GpbTasks.TabIndex = 1;
			this.GpbTasks.TabStop = false;
			this.GpbTasks.Text = "Tasks";
			// 
			// ChbTaskConfirm
			// 
			this.ChbTaskConfirm.AutoSize = true;
			this.ChbTaskConfirm.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbTaskConfirm.Location = new System.Drawing.Point(7, 43);
			this.ChbTaskConfirm.Name = "ChbTaskConfirm";
			this.ChbTaskConfirm.Size = new System.Drawing.Size(230, 18);
			this.ChbTaskConfirm.TabIndex = 1;
			this.ChbTaskConfirm.Text = "Ask for confirmation before executing task";
			this.TtpMain.SetToolTip(this.ChbTaskConfirm, "If checked, the app will ask for confirmation before executing a task");
			this.ChbTaskConfirm.UseVisualStyleBackColor = true;
			// 
			// ChbTasks
			// 
			this.ChbTasks.AutoSize = true;
			this.ChbTasks.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbTasks.Location = new System.Drawing.Point(7, 20);
			this.ChbTasks.Name = "ChbTasks";
			this.ChbTasks.Size = new System.Drawing.Size(93, 18);
			this.ChbTasks.TabIndex = 0;
			this.ChbTasks.Text = "Enable tasks";
			this.TtpMain.SetToolTip(this.ChbTasks, "If enabled, tasks will be executed");
			this.ChbTasks.UseVisualStyleBackColor = true;
			// 
			// GpbProfiles
			// 
			this.GpbProfiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GpbProfiles.Controls.Add(this.ChbOpenExplorer);
			this.GpbProfiles.Controls.Add(this.ChbBackup);
			this.GpbProfiles.Controls.Add(this.ChbTooManyToApply);
			this.GpbProfiles.Location = new System.Drawing.Point(7, 7);
			this.GpbProfiles.Name = "GpbProfiles";
			this.GpbProfiles.Size = new System.Drawing.Size(388, 89);
			this.GpbProfiles.TabIndex = 0;
			this.GpbProfiles.TabStop = false;
			this.GpbProfiles.Text = "Profiles";
			// 
			// ChbOpenExplorer
			// 
			this.ChbOpenExplorer.AutoSize = true;
			this.ChbOpenExplorer.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbOpenExplorer.Location = new System.Drawing.Point(7, 66);
			this.ChbOpenExplorer.Name = "ChbOpenExplorer";
			this.ChbOpenExplorer.Size = new System.Drawing.Size(180, 18);
			this.ChbOpenExplorer.TabIndex = 1;
			this.ChbOpenExplorer.Text = "Open file explorer after applying";
			this.TtpMain.SetToolTip(this.ChbOpenExplorer, "If checked, the app will open the file explorer after it applies a profile");
			this.ChbOpenExplorer.UseVisualStyleBackColor = true;
			// 
			// ChbBackup
			// 
			this.ChbBackup.AutoSize = true;
			this.ChbBackup.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbBackup.Location = new System.Drawing.Point(7, 20);
			this.ChbBackup.Name = "ChbBackup";
			this.ChbBackup.Size = new System.Drawing.Size(314, 18);
			this.ChbBackup.TabIndex = 0;
			this.ChbBackup.Text = "Prompt me to backup current favorites when applying profile";
			this.TtpMain.SetToolTip(this.ChbBackup, "If enabled, the app will ask you to backup your current favorites before applying" +
        " the selected profile");
			this.ChbBackup.UseVisualStyleBackColor = true;
			// 
			// ChbTooManyToApply
			// 
			this.ChbTooManyToApply.AutoSize = true;
			this.ChbTooManyToApply.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbTooManyToApply.Location = new System.Drawing.Point(7, 43);
			this.ChbTooManyToApply.Name = "ChbTooManyToApply";
			this.ChbTooManyToApply.Size = new System.Drawing.Size(286, 18);
			this.ChbTooManyToApply.TabIndex = 1;
			this.ChbTooManyToApply.Text = "Warn me before applying a profile with many shortcuts";
			this.TtpMain.SetToolTip(this.ChbTooManyToApply, "If enabled, the app will warn you if you try to apply a profile with more than 25" +
        " shortcuts");
			this.ChbTooManyToApply.UseVisualStyleBackColor = true;
			// 
			// ChbRefresh
			// 
			this.ChbRefresh.AutoSize = true;
			this.ChbRefresh.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbRefresh.Location = new System.Drawing.Point(7, 185);
			this.ChbRefresh.Name = "ChbRefresh";
			this.ChbRefresh.Size = new System.Drawing.Size(228, 18);
			this.ChbRefresh.TabIndex = 2;
			this.ChbRefresh.Text = "Refresh profile and task lists automatically";
			this.TtpMain.SetToolTip(this.ChbRefresh, "If this is checked, the app will refresh profiles & tasks list when it detects ch" +
        "ange in their folders");
			this.ChbRefresh.UseMnemonic = false;
			this.ChbRefresh.UseVisualStyleBackColor = true;
			this.ChbRefresh.CheckedChanged += new System.EventHandler(this.ChbRefresh_CheckedChanged);
			// 
			// TpgMisc
			// 
			this.TpgMisc.Controls.Add(this.ChbHelpImprove);
			this.TpgMisc.Controls.Add(this.ChbPop);
			this.TpgMisc.Controls.Add(this.ChbFeedback);
			this.TpgMisc.Location = new System.Drawing.Point(4, 23);
			this.TpgMisc.Name = "TpgMisc";
			this.TpgMisc.Size = new System.Drawing.Size(401, 311);
			this.TpgMisc.TabIndex = 3;
			this.TpgMisc.Text = "Misc.   ";
			this.TpgMisc.UseVisualStyleBackColor = true;
			// 
			// ChbHelpImprove
			// 
			this.ChbHelpImprove.AutoSize = true;
			this.ChbHelpImprove.Location = new System.Drawing.Point(7, 56);
			this.ChbHelpImprove.Name = "ChbHelpImprove";
			this.ChbHelpImprove.Size = new System.Drawing.Size(232, 17);
			this.ChbHelpImprove.TabIndex = 2;
			this.ChbHelpImprove.Text = "Join User Experience Improvement Program";
			this.TtpMain.SetToolTip(this.ChbHelpImprove, "Enabling this option will help greatly in the development of WinFavs by sending a" +
        "nonymous information to the developer and asking you periodically to send your f" +
        "eedback");
			this.ChbHelpImprove.UseVisualStyleBackColor = true;
			// 
			// ChbPop
			// 
			this.ChbPop.AutoSize = true;
			this.ChbPop.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbPop.Location = new System.Drawing.Point(7, 31);
			this.ChbPop.Name = "ChbPop";
			this.ChbPop.Size = new System.Drawing.Size(177, 18);
			this.ChbPop.TabIndex = 1;
			this.ChbPop.Text = "Enable notifications pop sound";
			this.TtpMain.SetToolTip(this.ChbPop, "If enabled, notifications will play a pop sound when shown");
			this.ChbPop.UseVisualStyleBackColor = true;
			// 
			// ChbFeedback
			// 
			this.ChbFeedback.AutoSize = true;
			this.ChbFeedback.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbFeedback.Location = new System.Drawing.Point(7, 8);
			this.ChbFeedback.Name = "ChbFeedback";
			this.ChbFeedback.Size = new System.Drawing.Size(179, 18);
			this.ChbFeedback.TabIndex = 0;
			this.ChbFeedback.Text = "Ask for feedback before exiting";
			this.TtpMain.SetToolTip(this.ChbFeedback, "If checked, the app will open the feedback window before exiting");
			this.ChbFeedback.UseVisualStyleBackColor = true;
			// 
			// ImgMain
			// 
			this.ImgMain.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ImgMain.ImageSize = new System.Drawing.Size(16, 16);
			this.ImgMain.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// BtnClose
			// 
			this.BtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnClose.Location = new System.Drawing.Point(338, 353);
			this.BtnClose.Name = "BtnClose";
			this.BtnClose.Size = new System.Drawing.Size(80, 23);
			this.BtnClose.TabIndex = 1;
			this.BtnClose.Text = "Close";
			this.BtnClose.UseVisualStyleBackColor = true;
			this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
			// 
			// TtpMain
			// 
			this.TtpMain.AutoPopDelay = 10000;
			this.TtpMain.InitialDelay = 250;
			this.TtpMain.ReshowDelay = 100;
			// 
			// FrmSettings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.CancelButton = this.BtnClose;
			this.ClientSize = new System.Drawing.Size(427, 384);
			this.Controls.Add(this.BtnClose);
			this.Controls.Add(this.TbcMain);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FrmSettings";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Settings";
			this.VisibleChanged += new System.EventHandler(this.FrmSettings_VisibleChanged);
			this.TbcMain.ResumeLayout(false);
			this.TpgGeneral.ResumeLayout(false);
			this.TpgGeneral.PerformLayout();
			this.GpbAssociation.ResumeLayout(false);
			this.GpbAssociation.PerformLayout();
			this.GpbStartup.ResumeLayout(false);
			this.GpbStartup.PerformLayout();
			this.TpgView.ResumeLayout(false);
			this.TpgView.PerformLayout();
			this.GpbDialogs.ResumeLayout(false);
			this.GpbDialogs.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.PbxView)).EndInit();
			this.TpgProfileTasks.ResumeLayout(false);
			this.TpgProfileTasks.PerformLayout();
			this.GpbTasks.ResumeLayout(false);
			this.GpbTasks.PerformLayout();
			this.GpbProfiles.ResumeLayout(false);
			this.GpbProfiles.PerformLayout();
			this.TpgMisc.ResumeLayout(false);
			this.TpgMisc.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl TbcMain;
		private System.Windows.Forms.TabPage TpgGeneral;
		private System.Windows.Forms.TabPage TpgView;
		private System.Windows.Forms.TabPage TpgProfileTasks;
		private System.Windows.Forms.TabPage TpgMisc;
		private System.Windows.Forms.Button BtnClose;
		private System.Windows.Forms.GroupBox GpbStartup;
		private System.Windows.Forms.Button BtnRemoveStart;
		private System.Windows.Forms.Button BtnAddStart;
		public System.Windows.Forms.CheckBox ChbAutoUpdate;
		private System.Windows.Forms.GroupBox GpbAssociation;
		private System.Windows.Forms.Button BtnAssociate;
		private System.Windows.Forms.Button BtnDissociate;
		private System.Windows.Forms.GroupBox GpbDialogs;
		private System.Windows.Forms.PictureBox PbxView;
		private System.Windows.Forms.Label LblProfileView;
		private System.Windows.Forms.GroupBox GpbTasks;
		private System.Windows.Forms.GroupBox GpbProfiles;
		public System.Windows.Forms.CheckBox ChbTaskConfirm;
		public System.Windows.Forms.CheckBox ChbTasks;
		public System.Windows.Forms.CheckBox ChbOpenExplorer;
		public System.Windows.Forms.CheckBox ChbBackup;
		public System.Windows.Forms.CheckBox ChbRefresh;
		public System.Windows.Forms.CheckBox ChbStartMinimized;
		public System.Windows.Forms.CheckBox ChbConservative;
		public System.Windows.Forms.CheckBox ChbMinimizeToTray;
		public System.Windows.Forms.CheckBox ChbClosingConfirmation;
		public System.Windows.Forms.CheckBox ChbTargetNotExist;
		public System.Windows.Forms.CheckBox ChbNoShortcuts;
		public System.Windows.Forms.CheckBox ChbHints;
		public System.Windows.Forms.CheckBox ChbPop;
		public System.Windows.Forms.CheckBox ChbFeedback;
		public System.Windows.Forms.Button BtnUpdate;
		public System.Windows.Forms.ComboBox CbxProfileView;
		private System.Windows.Forms.ToolTip TtpMain;
		private System.Windows.Forms.ImageList ImgMain;
		public System.Windows.Forms.CheckBox ChbHelpImprove;
		private System.Windows.Forms.Label LblStatShell;
		private System.Windows.Forms.Label LblStateStart;
		public System.Windows.Forms.Button BtnReset;
		public System.Windows.Forms.CheckBox ChbTooManyToApply;
	}
}