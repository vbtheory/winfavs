﻿namespace WinFavs
{
	partial class FrmTasks
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.BtnNew = new System.Windows.Forms.Button();
			this.BtnEdit = new System.Windows.Forms.Button();
			this.BtnRemove = new System.Windows.Forms.Button();
			this.BtnClear = new System.Windows.Forms.Button();
			this.TmrCheckEnable = new System.Windows.Forms.Timer(this.components);
			this.BtnRefresh = new System.Windows.Forms.Button();
			this.BtnClose = new System.Windows.Forms.Button();
			this.TtpMain = new System.Windows.Forms.ToolTip(this.components);
			this.LsvMain = new System.Windows.Forms.ListView();
			this.HdrProfile = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.HdrTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.HdrDays = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.HdrEnabled = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.FswMain = new WinFavs.Controls.DelayedWatcher();
			((System.ComponentModel.ISupportInitialize)(this.FswMain)).BeginInit();
			this.SuspendLayout();
			// 
			// BtnNew
			// 
			this.BtnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnNew.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnNew.Location = new System.Drawing.Point(421, 13);
			this.BtnNew.Name = "BtnNew";
			this.BtnNew.Size = new System.Drawing.Size(124, 23);
			this.BtnNew.TabIndex = 1;
			this.BtnNew.Text = "&New (Alt+ N)";
			this.TtpMain.SetToolTip(this.BtnNew, "Create new task");
			this.BtnNew.UseVisualStyleBackColor = true;
			this.BtnNew.Click += new System.EventHandler(this.BtnNew_Click);
			// 
			// BtnEdit
			// 
			this.BtnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnEdit.Enabled = false;
			this.BtnEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnEdit.Location = new System.Drawing.Point(421, 42);
			this.BtnEdit.Name = "BtnEdit";
			this.BtnEdit.Size = new System.Drawing.Size(124, 23);
			this.BtnEdit.TabIndex = 2;
			this.BtnEdit.Text = "&Edit (Alt+E)";
			this.TtpMain.SetToolTip(this.BtnEdit, "Edit the currently selected task(s)");
			this.BtnEdit.UseVisualStyleBackColor = true;
			this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
			// 
			// BtnRemove
			// 
			this.BtnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnRemove.Enabled = false;
			this.BtnRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnRemove.Location = new System.Drawing.Point(421, 71);
			this.BtnRemove.Name = "BtnRemove";
			this.BtnRemove.Size = new System.Drawing.Size(124, 23);
			this.BtnRemove.TabIndex = 3;
			this.BtnRemove.Text = "Remove (Del)";
			this.TtpMain.SetToolTip(this.BtnRemove, "Delete the currently selected task(s)");
			this.BtnRemove.UseVisualStyleBackColor = true;
			this.BtnRemove.Click += new System.EventHandler(this.BtnRemove_Click);
			// 
			// BtnClear
			// 
			this.BtnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnClear.Enabled = false;
			this.BtnClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnClear.Location = new System.Drawing.Point(421, 100);
			this.BtnClear.Name = "BtnClear";
			this.BtnClear.Size = new System.Drawing.Size(124, 23);
			this.BtnClear.TabIndex = 4;
			this.BtnClear.Text = "&Clear (Alt+C)";
			this.TtpMain.SetToolTip(this.BtnClear, "Delete all tasks");
			this.BtnClear.UseVisualStyleBackColor = true;
			this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
			// 
			// TmrCheckEnable
			// 
			this.TmrCheckEnable.Enabled = true;
			this.TmrCheckEnable.Interval = 1;
			this.TmrCheckEnable.Tick += new System.EventHandler(this.TmrCheckEnable_Tick);
			// 
			// BtnRefresh
			// 
			this.BtnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnRefresh.Location = new System.Drawing.Point(421, 129);
			this.BtnRefresh.Name = "BtnRefresh";
			this.BtnRefresh.Size = new System.Drawing.Size(124, 23);
			this.BtnRefresh.TabIndex = 5;
			this.BtnRefresh.Text = "Refresh (F5)";
			this.TtpMain.SetToolTip(this.BtnRefresh, "Refresh tasks");
			this.BtnRefresh.UseVisualStyleBackColor = true;
			this.BtnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
			// 
			// BtnClose
			// 
			this.BtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnClose.Location = new System.Drawing.Point(421, 280);
			this.BtnClose.Name = "BtnClose";
			this.BtnClose.Size = new System.Drawing.Size(124, 23);
			this.BtnClose.TabIndex = 6;
			this.BtnClose.Text = "Close";
			this.TtpMain.SetToolTip(this.BtnClose, "Close window");
			this.BtnClose.UseVisualStyleBackColor = true;
			this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
			// 
			// TtpMain
			// 
			this.TtpMain.AutoPopDelay = 5000;
			this.TtpMain.InitialDelay = 250;
			this.TtpMain.ReshowDelay = 100;
			// 
			// LsvMain
			// 
			this.LsvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LsvMain.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.HdrProfile,
            this.HdrTime,
            this.HdrDays,
            this.HdrEnabled});
			this.LsvMain.FullRowSelect = true;
			this.LsvMain.GridLines = true;
			this.LsvMain.Location = new System.Drawing.Point(13, 13);
			this.LsvMain.Name = "LsvMain";
			this.LsvMain.Size = new System.Drawing.Size(402, 288);
			this.LsvMain.TabIndex = 0;
			this.LsvMain.UseCompatibleStateImageBehavior = false;
			this.LsvMain.View = System.Windows.Forms.View.Details;
			// 
			// HdrProfile
			// 
			this.HdrProfile.Text = "Profile";
			this.HdrProfile.Width = 42;
			// 
			// HdrTime
			// 
			this.HdrTime.Text = "Time";
			this.HdrTime.Width = 35;
			// 
			// HdrDays
			// 
			this.HdrDays.Text = "Days";
			this.HdrDays.Width = 36;
			// 
			// HdrEnabled
			// 
			this.HdrEnabled.Text = "Enabled";
			this.HdrEnabled.Width = 285;
			// 
			// FswMain
			// 
			this.FswMain.EnableRaisingEvents = true;
			this.FswMain.Interval = 500;
			this.FswMain.NotifyFilter = ((System.IO.NotifyFilters)((((((System.IO.NotifyFilters.FileName | System.IO.NotifyFilters.Attributes) 
            | System.IO.NotifyFilters.Size) 
            | System.IO.NotifyFilters.LastWrite) 
            | System.IO.NotifyFilters.LastAccess) 
            | System.IO.NotifyFilters.CreationTime)));
			this.FswMain.SynchronizingObject = this;
			this.FswMain.ChangeDetected += new System.EventHandler(this.FswMain_ChangeDetected);
			// 
			// FrmTasks
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.CancelButton = this.BtnClose;
			this.ClientSize = new System.Drawing.Size(558, 321);
			this.Controls.Add(this.LsvMain);
			this.Controls.Add(this.BtnClose);
			this.Controls.Add(this.BtnRefresh);
			this.Controls.Add(this.BtnClear);
			this.Controls.Add(this.BtnRemove);
			this.Controls.Add(this.BtnEdit);
			this.Controls.Add(this.BtnNew);
			this.ForeColor = System.Drawing.Color.Black;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(566, 352);
			this.Name = "FrmTasks";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Tasks";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTasks_FormClosed);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmTasks_KeyDown);
			((System.ComponentModel.ISupportInitialize)(this.FswMain)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button BtnNew;
		private System.Windows.Forms.Button BtnEdit;
		private System.Windows.Forms.Button BtnRemove;
		private System.Windows.Forms.Button BtnClear;
		private System.Windows.Forms.Timer TmrCheckEnable;
		private System.Windows.Forms.Button BtnRefresh;
		private System.Windows.Forms.Button BtnClose;
		private System.Windows.Forms.ToolTip TtpMain;
		private System.Windows.Forms.ListView LsvMain;
		private System.Windows.Forms.ColumnHeader HdrProfile;
		private System.Windows.Forms.ColumnHeader HdrTime;
		private System.Windows.Forms.ColumnHeader HdrDays;
		private System.Windows.Forms.ColumnHeader HdrEnabled;
		private Controls.DelayedWatcher FswMain;
	}
}