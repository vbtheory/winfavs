﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Transitions;
using WinFavs.Classes;

namespace WinFavs
{
	public partial class FrmFeedback : Form
	{
		private bool _canExit = true;

		public FrmFeedback()
		{
			InitializeComponent();
			Lighter.SetHighlight(BtnUp, Highlight.Blue);
			Lighter.SetHighlight(BtnOk, Highlight.Green);
		}

		private void BtnOk_Click(object sender, EventArgs e)
		{
			BtnOk.Enabled = false;

			if (!TxtSummary.IsValid)
			{
				BtnOk.Enabled = true;
				Transition t = new Transition(new TransitionType_Flash(3, 250));
				t.add(BtnOk, "BackColor", Color.Red);
				t.TransitionCompletedEvent += (o, args) => BtnOk.BackColor = Color.FromKnownColor(KnownColor.Control);
				t.run();
				return;
			}

			Cursor = Cursors.AppStarting;

			string init = SendFeedback(TxtSummary.Text.Trim(',', '\'', '"', '*', '/', '>', '<', '=', '~', '(', ')'),
					Lighter.GetHighlight(BtnUp) == Highlight.Blue ? 1 : 0,
					TxtEmail.IsValid ? TxtEmail.Text : "", File.Exists(Logger._logPath)
					? Compress(new StreamReader(Logger._logPath).ReadToEnd()) : "");

			while (init != "")
			{
				Logger.Log("Failed to send feedback: " + init);
				if (Alert.Show("Sending feedback failed: <b>" + init + "</b>", "Error", MessageBoxButtons.RetryCancel) !=
				    DialogResult.Retry)
				{
					BtnOk.Enabled = true;
					Cursor = Cursors.Default;
					return;
				}

				init = SendFeedback(TxtSummary.Text.Trim(',', '\'', '"', '*', '/', '>', '<', '=', '~', '(', ')'),
					Lighter.GetHighlight(BtnUp) == Highlight.Blue ? 1 : 0,
					TxtEmail.IsValid ? TxtEmail.Text : "", File.Exists(Logger._logPath)
					? Compress(new StreamReader(Logger._logPath).ReadToEnd()) : "");
			}

			Cursor = Cursors.Default;
			AnimateExit();
		}

		private void BtnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void FrmFeedback_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!_canExit)
			{
				_canExit = true;
				e.Cancel = true;
			}
		}

		private void AnimateExit()
		{
			Transition t2 = new Transition(new TransitionType_Acceleration(350));
			t2.TransitionCompletedEvent += (sender, args) =>
			{
				_canExit = true;
				Close();
				Alert.Show(
					"Thank you <b>very much</b> for helping us shape the future of the app. If you provided your email, you'll receive a message from the developer in the next few days :)");
			};
			t2.add(this, "Top", -Height);
			t2.add(this, "Opacity", 0.01);
			t2.run();
		}

		public static string SendFeedback(string summary, int rating, string mail, string log)
		{
			MySqlConnection c =
				new MySqlConnection(ConfigurationManager.ConnectionStrings["WinFavs"].ConnectionString);

			string temp = OpenConnection(c);

			if (temp != "") return temp;

			temp = Insert(c, summary, rating, mail, log);

			return temp != "" ? temp : CloseConnection(c);
		}

		public static string OpenConnection(MySqlConnection c)
		{
			try
			{
				c.Open();
				return "";
			}
			catch (MySqlException ex)
			{
				CloseConnection(c);
				return ex.Message;
			}
		}

		public static string CloseConnection(MySqlConnection c)
		{
			try
			{
				c.Close();
				return "";
			}
			catch (MySqlException ex)
			{
				return ex.Message;
			}
		}

		public static string Insert(MySqlConnection c, string summary, int rating, string mail, string log)
		{
			try
			{
				Logger.WaitForLog();
				
				string query = string.Format("INSERT INTO Feedback(Version, Summary, Rating, Date, Log, Email) " +
				                             "VALUES ('{0}', '{1}', '{2}', {3}, '{4}', '{5}');",
											 (Debugger.IsAttached?"dev-":"") + Application.ProductVersion,
					summary,
					rating,
					"CURRENT_TIMESTAMP",
					log,
					mail);

				MySqlCommand cmd = new MySqlCommand(query, c) {CommandTimeout = 10000};
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				CloseConnection(c);
				return ex.Message;
			}

			return "";
		}

		public static string Compress(string text)
		{
			byte[] buffer = Encoding.UTF8.GetBytes(text);
			MemoryStream ms = new MemoryStream();
			using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
			{
				zip.Write(buffer, 0, buffer.Length);
			}

			ms.Position = 0;

			byte[] compressed = new byte[ms.Length];
			ms.Read(compressed, 0, compressed.Length);

			byte[] gzBuffer = new byte[compressed.Length + 4];
			Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
			Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
			return Convert.ToBase64String(gzBuffer);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Lighter.SetHighlight(BtnUp, Highlight.Blue);
			Lighter.SetHighlight(BtnDown, Highlight.None);
		}

		private void button2_Click(object sender, EventArgs e)
		{
			Lighter.SetHighlight(BtnDown, Highlight.Blue);
			Lighter.SetHighlight(BtnUp, Highlight.None);
		}

		private void FrmFeedback_FormClosed(object sender, FormClosedEventArgs e)
		{
			Dispose(true);
		}

		private void TxtEmail_TextChanged(object sender, EventArgs e)
		{
			BtnOk.Enabled = false;
		}

		private void TxtEmail_TextChangedDelayed(object sender, EventArgs e)
		{
			BtnOk.Enabled = (TxtEmail.IsValid || TxtEmail.Text == "");
		}
	}
}