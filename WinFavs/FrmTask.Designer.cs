﻿namespace WinFavs
{
	partial class FrmTask
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.LblRepeat = new System.Windows.Forms.Label();
			this.ChbMon = new System.Windows.Forms.CheckBox();
			this.ChbTue = new System.Windows.Forms.CheckBox();
			this.ChbWed = new System.Windows.Forms.CheckBox();
			this.ChbThu = new System.Windows.Forms.CheckBox();
			this.ChbFri = new System.Windows.Forms.CheckBox();
			this.ChbSat = new System.Windows.Forms.CheckBox();
			this.ChbSun = new System.Windows.Forms.CheckBox();
			this.BtnCancel = new System.Windows.Forms.Button();
			this.BtnSave = new System.Windows.Forms.Button();
			this.LblTime = new System.Windows.Forms.Label();
			this.LblProfile = new System.Windows.Forms.Label();
			this.CbxProfiles = new System.Windows.Forms.ComboBox();
			this.ChbEnabled = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.DtpMain = new System.Windows.Forms.DateTimePicker();
			this.BtnRefresh = new System.Windows.Forms.Button();
			this.TtpMain = new System.Windows.Forms.ToolTip(this.components);
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// LblRepeat
			// 
			this.LblRepeat.AutoSize = true;
			this.LblRepeat.Location = new System.Drawing.Point(12, 88);
			this.LblRepeat.Name = "LblRepeat";
			this.LblRepeat.Size = new System.Drawing.Size(45, 13);
			this.LblRepeat.TabIndex = 0;
			this.LblRepeat.Text = "Repeat:";
			// 
			// ChbMon
			// 
			this.ChbMon.AutoSize = true;
			this.ChbMon.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbMon.Location = new System.Drawing.Point(8, 4);
			this.ChbMon.Name = "ChbMon";
			this.ChbMon.Size = new System.Drawing.Size(53, 18);
			this.ChbMon.TabIndex = 0;
			this.ChbMon.Text = "Mon";
			this.ChbMon.UseVisualStyleBackColor = true;
			this.ChbMon.CheckedChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// ChbTue
			// 
			this.ChbTue.AutoSize = true;
			this.ChbTue.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbTue.Location = new System.Drawing.Point(61, 4);
			this.ChbTue.Name = "ChbTue";
			this.ChbTue.Size = new System.Drawing.Size(51, 18);
			this.ChbTue.TabIndex = 1;
			this.ChbTue.Text = "Tue";
			this.ChbTue.UseVisualStyleBackColor = true;
			this.ChbTue.CheckedChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// ChbWed
			// 
			this.ChbWed.AutoSize = true;
			this.ChbWed.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbWed.Location = new System.Drawing.Point(112, 4);
			this.ChbWed.Name = "ChbWed";
			this.ChbWed.Size = new System.Drawing.Size(55, 18);
			this.ChbWed.TabIndex = 2;
			this.ChbWed.Text = "Wed";
			this.ChbWed.UseVisualStyleBackColor = true;
			this.ChbWed.CheckedChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// ChbThu
			// 
			this.ChbThu.AutoSize = true;
			this.ChbThu.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbThu.Location = new System.Drawing.Point(167, 4);
			this.ChbThu.Name = "ChbThu";
			this.ChbThu.Size = new System.Drawing.Size(51, 18);
			this.ChbThu.TabIndex = 3;
			this.ChbThu.Text = "Thu";
			this.ChbThu.UseVisualStyleBackColor = true;
			this.ChbThu.CheckedChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// ChbFri
			// 
			this.ChbFri.AutoSize = true;
			this.ChbFri.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbFri.Location = new System.Drawing.Point(218, 4);
			this.ChbFri.Name = "ChbFri";
			this.ChbFri.Size = new System.Drawing.Size(43, 18);
			this.ChbFri.TabIndex = 4;
			this.ChbFri.Text = "Fri";
			this.ChbFri.UseVisualStyleBackColor = true;
			this.ChbFri.CheckedChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// ChbSat
			// 
			this.ChbSat.AutoSize = true;
			this.ChbSat.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbSat.Location = new System.Drawing.Point(261, 4);
			this.ChbSat.Name = "ChbSat";
			this.ChbSat.Size = new System.Drawing.Size(48, 18);
			this.ChbSat.TabIndex = 5;
			this.ChbSat.Text = "Sat";
			this.ChbSat.UseVisualStyleBackColor = true;
			this.ChbSat.CheckedChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// ChbSun
			// 
			this.ChbSun.AutoSize = true;
			this.ChbSun.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbSun.Location = new System.Drawing.Point(309, 4);
			this.ChbSun.Name = "ChbSun";
			this.ChbSun.Size = new System.Drawing.Size(51, 18);
			this.ChbSun.TabIndex = 6;
			this.ChbSun.Text = "Sun";
			this.ChbSun.UseVisualStyleBackColor = true;
			this.ChbSun.CheckedChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// BtnCancel
			// 
			this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnCancel.Location = new System.Drawing.Point(289, 139);
			this.BtnCancel.Name = "BtnCancel";
			this.BtnCancel.Size = new System.Drawing.Size(75, 23);
			this.BtnCancel.TabIndex = 6;
			this.BtnCancel.Text = "Cancel";
			this.TtpMain.SetToolTip(this.BtnCancel, "Exit without saving changes");
			this.BtnCancel.UseVisualStyleBackColor = true;
			// 
			// BtnSave
			// 
			this.BtnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.BtnSave.Enabled = false;
			this.BtnSave.Location = new System.Drawing.Point(156, 139);
			this.BtnSave.Name = "BtnSave";
			this.BtnSave.Size = new System.Drawing.Size(127, 23);
			this.BtnSave.TabIndex = 5;
			this.BtnSave.Text = "Save task";
			this.TtpMain.SetToolTip(this.BtnSave, "Save changes");
			this.BtnSave.UseVisualStyleBackColor = true;
			this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
			// 
			// LblTime
			// 
			this.LblTime.AutoSize = true;
			this.LblTime.Location = new System.Drawing.Point(12, 49);
			this.LblTime.Name = "LblTime";
			this.LblTime.Size = new System.Drawing.Size(33, 13);
			this.LblTime.TabIndex = 0;
			this.LblTime.Text = "Time:";
			// 
			// LblProfile
			// 
			this.LblProfile.AutoSize = true;
			this.LblProfile.Location = new System.Drawing.Point(12, 9);
			this.LblProfile.Name = "LblProfile";
			this.LblProfile.Size = new System.Drawing.Size(39, 13);
			this.LblProfile.TabIndex = 4;
			this.LblProfile.Text = "Profile:";
			// 
			// CbxProfiles
			// 
			this.CbxProfiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbxProfiles.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.CbxProfiles.FormattingEnabled = true;
			this.CbxProfiles.Location = new System.Drawing.Point(15, 25);
			this.CbxProfiles.Name = "CbxProfiles";
			this.CbxProfiles.Size = new System.Drawing.Size(320, 21);
			this.CbxProfiles.TabIndex = 0;
			this.CbxProfiles.SelectedIndexChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// ChbEnabled
			// 
			this.ChbEnabled.AutoSize = true;
			this.ChbEnabled.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.ChbEnabled.Location = new System.Drawing.Point(15, 142);
			this.ChbEnabled.Name = "ChbEnabled";
			this.ChbEnabled.Size = new System.Drawing.Size(71, 18);
			this.ChbEnabled.TabIndex = 4;
			this.ChbEnabled.Text = "Enabled";
			this.TtpMain.SetToolTip(this.ChbEnabled, "If this is checked, the app will trigger this task");
			this.ChbEnabled.UseVisualStyleBackColor = true;
			this.ChbEnabled.CheckedChanged += new System.EventHandler(this.CbxProfiles_SelectedIndexChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.ChbMon);
			this.panel1.Controls.Add(this.ChbTue);
			this.panel1.Controls.Add(this.ChbWed);
			this.panel1.Controls.Add(this.ChbThu);
			this.panel1.Controls.Add(this.ChbFri);
			this.panel1.Controls.Add(this.ChbSun);
			this.panel1.Controls.Add(this.ChbSat);
			this.panel1.Location = new System.Drawing.Point(15, 104);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(349, 24);
			this.panel1.TabIndex = 3;
			// 
			// DtpMain
			// 
			this.DtpMain.CustomFormat = "HH:mm";
			this.DtpMain.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.DtpMain.Location = new System.Drawing.Point(15, 65);
			this.DtpMain.Name = "DtpMain";
			this.DtpMain.ShowUpDown = true;
			this.DtpMain.Size = new System.Drawing.Size(69, 20);
			this.DtpMain.TabIndex = 2;
			// 
			// BtnRefresh
			// 
			this.BtnRefresh.BackgroundImage = global::WinFavs.Properties.Resources.Refresh;
			this.BtnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.BtnRefresh.Location = new System.Drawing.Point(341, 24);
			this.BtnRefresh.Name = "BtnRefresh";
			this.BtnRefresh.Size = new System.Drawing.Size(23, 23);
			this.BtnRefresh.TabIndex = 1;
			this.TtpMain.SetToolTip(this.BtnRefresh, "Refresh profiles");
			this.BtnRefresh.UseVisualStyleBackColor = true;
			this.BtnRefresh.Click += new System.EventHandler(this.button1_Click);
			// 
			// TtpMain
			// 
			this.TtpMain.AutoPopDelay = 5000;
			this.TtpMain.InitialDelay = 250;
			this.TtpMain.ReshowDelay = 100;
			// 
			// FrmTask
			// 
			this.AcceptButton = this.BtnSave;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.CancelButton = this.BtnCancel;
			this.ClientSize = new System.Drawing.Size(373, 170);
			this.Controls.Add(this.BtnRefresh);
			this.Controls.Add(this.DtpMain);
			this.Controls.Add(this.CbxProfiles);
			this.Controls.Add(this.LblProfile);
			this.Controls.Add(this.BtnSave);
			this.Controls.Add(this.BtnCancel);
			this.Controls.Add(this.ChbEnabled);
			this.Controls.Add(this.LblTime);
			this.Controls.Add(this.LblRepeat);
			this.Controls.Add(this.panel1);
			this.ForeColor = System.Drawing.Color.Black;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FrmTask";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Edit task";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmEditTask_FormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTask_FormClosed);
			this.Shown += new System.EventHandler(this.FrmTask_Shown);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LblRepeat;
		private System.Windows.Forms.CheckBox ChbMon;
		private System.Windows.Forms.CheckBox ChbTue;
		private System.Windows.Forms.CheckBox ChbWed;
		private System.Windows.Forms.CheckBox ChbThu;
		private System.Windows.Forms.CheckBox ChbFri;
		private System.Windows.Forms.CheckBox ChbSat;
		private System.Windows.Forms.CheckBox ChbSun;
		private System.Windows.Forms.Button BtnCancel;
		private System.Windows.Forms.Button BtnSave;
		private System.Windows.Forms.Label LblTime;
		private System.Windows.Forms.Label LblProfile;
		private System.Windows.Forms.ComboBox CbxProfiles;
		private System.Windows.Forms.CheckBox ChbEnabled;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DateTimePicker DtpMain;
		private System.Windows.Forms.Button BtnRefresh;
		private System.Windows.Forms.ToolTip TtpMain;
	}
}