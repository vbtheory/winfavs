﻿using WinFavs.Controls;

namespace WinFavs
{
	partial class FrmFeedback
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFeedback));
			this.LblRating = new System.Windows.Forms.Label();
			this.LblSummary = new System.Windows.Forms.Label();
			this.LblEmail = new System.Windows.Forms.Label();
			this.BtnOk = new System.Windows.Forms.Button();
			this.BtnCancel = new System.Windows.Forms.Button();
			this.BtnDown = new System.Windows.Forms.Button();
			this.BtnUp = new System.Windows.Forms.Button();
			this.TxtEmail = new WinFavs.Controls.RuleBox();
			this.TxtSummary = new WinFavs.Controls.RuleBox();
			this.SuspendLayout();
			// 
			// LblRating
			// 
			this.LblRating.AutoSize = true;
			this.LblRating.Location = new System.Drawing.Point(11, 54);
			this.LblRating.Name = "LblRating";
			this.LblRating.Size = new System.Drawing.Size(108, 13);
			this.LblRating.TabIndex = 0;
			this.LblRating.Text = "Please rate WinFavs:";
			// 
			// LblSummary
			// 
			this.LblSummary.Location = new System.Drawing.Point(11, 122);
			this.LblSummary.Name = "LblSummary";
			this.LblSummary.Size = new System.Drawing.Size(346, 26);
			this.LblSummary.TabIndex = 0;
			this.LblSummary.Text = "Please describe your experience with WinFavs (Best/Worst features, possible impro" +
    "vements, request features, problems faced...):";
			// 
			// LblEmail
			// 
			this.LblEmail.AutoSize = true;
			this.LblEmail.Location = new System.Drawing.Point(11, 11);
			this.LblEmail.Name = "LblEmail";
			this.LblEmail.Size = new System.Drawing.Size(219, 13);
			this.LblEmail.TabIndex = 0;
			this.LblEmail.Text = "Please enter your email (optional for contact):";
			// 
			// BtnOk
			// 
			this.BtnOk.Location = new System.Drawing.Point(13, 274);
			this.BtnOk.Name = "BtnOk";
			this.BtnOk.Size = new System.Drawing.Size(263, 23);
			this.BtnOk.TabIndex = 4;
			this.BtnOk.Text = "Send feedback";
			this.BtnOk.UseVisualStyleBackColor = true;
			this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
			// 
			// BtnCancel
			// 
			this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnCancel.Location = new System.Drawing.Point(282, 274);
			this.BtnCancel.Name = "BtnCancel";
			this.BtnCancel.Size = new System.Drawing.Size(75, 23);
			this.BtnCancel.TabIndex = 5;
			this.BtnCancel.Text = "Cancel";
			this.BtnCancel.UseVisualStyleBackColor = true;
			this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
			// 
			// BtnDown
			// 
			this.BtnDown.Image = global::WinFavs.Properties.Resources.Down;
			this.BtnDown.Location = new System.Drawing.Point(187, 70);
			this.BtnDown.Name = "BtnDown";
			this.BtnDown.Size = new System.Drawing.Size(48, 48);
			this.BtnDown.TabIndex = 2;
			this.BtnDown.TabStop = false;
			this.BtnDown.UseVisualStyleBackColor = true;
			this.BtnDown.Click += new System.EventHandler(this.button2_Click);
			// 
			// BtnUp
			// 
			this.BtnUp.Image = global::WinFavs.Properties.Resources.Up;
			this.BtnUp.Location = new System.Drawing.Point(133, 70);
			this.BtnUp.Name = "BtnUp";
			this.BtnUp.Size = new System.Drawing.Size(48, 48);
			this.BtnUp.TabIndex = 1;
			this.BtnUp.UseVisualStyleBackColor = true;
			this.BtnUp.Click += new System.EventHandler(this.button1_Click);
			// 
			// TxtEmail
			// 
			this.TxtEmail.BackColor = System.Drawing.SystemColors.Window;
			this.TxtEmail.CueText = null;
			this.TxtEmail.FixedLength = 0;
			this.TxtEmail.Location = new System.Drawing.Point(13, 28);
			this.TxtEmail.Name = "TxtEmail";
			this.TxtEmail.Size = new System.Drawing.Size(344, 20);
			this.TxtEmail.TabIndex = 0;
			this.TxtEmail.ValidationMethod = WinFavs.Controls.RuleBox.Validation.Email;
			this.TxtEmail.TextChangedDelayed += new System.EventHandler(this.TxtEmail_TextChangedDelayed);
			this.TxtEmail.TextChanged += new System.EventHandler(this.TxtEmail_TextChanged);
			// 
			// TxtSummary
			// 
			this.TxtSummary.BackColor = System.Drawing.SystemColors.Window;
			this.TxtSummary.CueText = null;
			this.TxtSummary.FixedLength = 0;
			this.TxtSummary.ForeColor = System.Drawing.Color.Black;
			this.TxtSummary.Location = new System.Drawing.Point(13, 154);
			this.TxtSummary.Multiline = true;
			this.TxtSummary.Name = "TxtSummary";
			this.TxtSummary.Size = new System.Drawing.Size(344, 114);
			this.TxtSummary.TabIndex = 3;
			this.TxtSummary.ValidationMethod = WinFavs.Controls.RuleBox.Validation.None;
			// 
			// FrmFeedback
			// 
			this.AcceptButton = this.BtnOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.CancelButton = this.BtnCancel;
			this.ClientSize = new System.Drawing.Size(368, 307);
			this.Controls.Add(this.BtnDown);
			this.Controls.Add(this.BtnUp);
			this.Controls.Add(this.TxtEmail);
			this.Controls.Add(this.BtnCancel);
			this.Controls.Add(this.BtnOk);
			this.Controls.Add(this.LblSummary);
			this.Controls.Add(this.LblEmail);
			this.Controls.Add(this.LblRating);
			this.Controls.Add(this.TxtSummary);
			this.ForeColor = System.Drawing.SystemColors.MenuText;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FrmFeedback";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Send feedback";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmFeedback_FormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmFeedback_FormClosed);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LblRating;
		private System.Windows.Forms.Label LblSummary;
		private RuleBox TxtSummary;
		private System.Windows.Forms.Button BtnOk;
		private System.Windows.Forms.Button BtnCancel;
		private System.Windows.Forms.Label LblEmail;
		private RuleBox TxtEmail;
		private System.Windows.Forms.Button BtnUp;
		private System.Windows.Forms.Button BtnDown;
	}
}